
// init our global var.
var graph = {width: 300, height: 25, fill: 'red', total: 10, value: 7, barWidth: 40, id: ''};
var aniCount = {total: 200, steps: 100, id: ''};
var time = 2000;

(function ($) {


Drupal.behaviors.citconGraph = {};

Drupal.behaviors.citconGraph.attach = function (context, settings) {
  graph.width = settings.citconGraph.width;
  graph.height = settings.citconGraph.height;
  graph.fill = settings.citconGraph.barcolor;
  graph.total = settings.citconGraph.total;
  graph.value = settings.citconGraph.value;
  graph.barWidth = (graph.value / graph.total) * graph.width;
  graph.id = '#citcon-graph-'+settings.citconGraph.hash;

  $(graph.id).svg({onLoad: drawGraph});

  aniCount.id = '#citcon-ani-count-'+settings.citconGraph.hash;
  aniCount.total = settings.citconGraph.value;
  aniCount.steps = time / 100;

  var counter = new drawAniCounter();
  counter.startCounter();
};

}(jQuery));

/**
 * Draw the bar
 */
function drawGraph(svg) {
  var bar = svg.rect(0,0,0,graph.height,
    {fill: '#444', stroke: 'none'});
  var outline = svg.rect(0,0,graph.width,graph.height,
    {fill: 'none', stroke: 'black', strokeWidth: 2});

  jQuery(bar).animate({svgWidth:graph.barWidth, svgFill:graph.fill}, time);
}

/**
 * Animate the counter
 */
function drawAniCounter(){
  var timer;
  var chunk = aniCount.total / aniCount.steps;
  var i = 1;

  // Find the next number
  function citconGraphNextNum(){
    var num = Math.round(chunk * i);

    jQuery(aniCount.id).text(addCommas(num));

    i++;
    if (i >= aniCount.steps){
      stopCounter();
    }
  }

  // Kill the timer
  function stopCounter(){
    jQuery(aniCount.id).text(addCommas(aniCount.total));
    clearInterval(timer);
  }

  // Start the timer
  this.startCounter = function(){
    timer = setInterval(citconGraphNextNum,100);
  }
}

/**
 * Like it says... it adds commas to numbers
 */
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
