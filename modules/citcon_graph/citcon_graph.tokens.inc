<?php

/**
 * @file
 * Citizens Connected tokens for graphing.
 */

/**
 * Implements hook_token_info().
 */
function citcon_graph_token_info() {

  // Thermometer helper tokens
  $info['types']['cc-g-t'] = array(
    'name' => t('CC G'),
    'description' => t('CitCon Thermometer Tokens.'),
    'needs-data' => 'cc-c',
  );
  $info['tokens']['cc-g-t']['width-?'] = array(
    'name' => 'width-?',
    'description' => t("The width of the thermometer. Integer, in pixles."),
  );
  $info['tokens']['cc-g-t']['height-?'] = array(
    'name' => 'height-?',
    'description' => t("The height of the thermometer. Integer, in pixles."),
  );
  //$info['tokens']['cc-g-t']['style-?'] = array(
  //  'name' => 'style-?',
  //  'description' => t("The style of the bar. Possible values are: round, sharp, soft, square."),
  //);
  $info['tokens']['cc-g-t']['barcolor-?'] = array(
    'name' => 'barcolor-?',
    'description' => t("A six digit hex color for the bar, with the hash prefix. Like this: #339911"),
  );

  // Graph tokens
  $info['types']['cc-g'] = array(
    'name' => t('CC G'),
    'description' => t('CitCon Graph Tokens.'),
    'needs-data' => 'cc-c',
  );
  $info['tokens']['cc-g']['therm'] = array(
    'name' => t('Thermometer'),
    'description' => t("A thermometer style graph. This token will output the default values.<br />
      Expend this token to see a list of chainable tokens that you can use to customize a thermometer.<br />
      Also, check the help for more info."),
    'type' => 'cc-g-t',
  );
  $info['tokens']['cc-g']['count'] = array(
    'name' => t('Animated Counter'),
    'description' => t("An animated counter that will count the number of supporters."),
      //'type' => 'cc-g-t',
  );

  return $info;
}

/**
 * Implements hook_tokens()
 */
function citcon_graph_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if (strpos($type, 'cc-g') === 0) {
    // if we don't have a proper node... do nothing.
    if (!isset($data['cc-c']->nid)) {
      return;
    }
    $campaign_values = citcon_get_campaign_info($data['cc-c']);
    foreach ($tokens as $name => $original) {
      $ops = explode(':', $name);

      switch ($ops[0]) {
        case 'count' :
          // Get property tokens
          $props = $ops;
          // Set them up in our graph array
          $graph = array();
          $graph['type'] = array_shift($props);

          // Get the graph values: Supporter count and total
          $nid = $data['cc-c']->nid;
          $tnid = isset($data['cc-c']->tnid) ? $data['cc-c']->tnid : 0;
          $values[] = citcon_get_campaign_supporters_count($nid, $tnid) + $campaign_values['citcon_goal_offset'][0]['value'];
                    $values[] = $campaign_values['citcon_goal'][0]['value'];

          // Set value to graph array and take away the "total" value from the values array
          $graph['total'] = array_pop($values);

          // Render array and replace tokens
          $replacements[$original] = citcon_graph_render($graph, $values);
          break;
        case 'therm':
          // Get property tokens
          $props = $ops;

          // Set them up in our graph array
          $graph = array();
          $graph['type'] = array_shift($props);
          foreach ($props as $cc_token) {
            $prop = explode('-', $cc_token);
            $graph[$prop[0]] = $prop[1];
          }

          // Get defaults for any missing values
          $graph = citcon_graph_set_defaults($graph);

          // Get the graph values: Supporter count and total
          $nid = $data['cc-c']->nid;
          $tnid = isset($data['cc-c']->tnid) ? $data['cc-c']->tnid : 0;
          $values[] = citcon_get_campaign_supporters_count($nid, $tnid) + $campaign_values['citcon_goal_offset'][0]['value'];
                    $values[] = $campaign_values['citcon_goal'][0]['value'];

          // Set value to graph array and take away the "total" value from the values array
          $graph['total'] = array_pop($values);

          // Render array and replace tokens
          $replacements[$original] = citcon_graph_render($graph, $values);
          break;
      }
    }
  }

  return $replacements;
}
