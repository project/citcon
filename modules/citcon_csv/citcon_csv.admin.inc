<?php

/**
 * @file
 * Admin pages for CSV output.
 */

/**
 * Returns HTML for the CSV download form.
 * Need it to do the exact same thing as the other function.
 *
 * @todo Look into a better way to do this. I think I can
 * manually define the function in the theme hook...?
 */
function theme_citcon_csv_top_form($variables) {
  return theme_citcon_csv_form($variables);
}

/**
 * Returns HTML for the CSV download form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_citcon_csv_form($variables) {
  $form = $variables['form'];

  // Add the "Field Options" table with tabledrag.
  drupal_add_tabledrag('citcon-csv-fields-table', 'order', 'sibling', 'field-weight');
  $header = array(t('CSV Column Label'), t('Download'), t('Weight'));
  $rows = array();
  foreach (element_children($form['csv_field_opts_fielset']['csv_field_opts']) as $inst_id) {

    $row = array();
    $row[] = drupal_render($form['csv_field_opts_fielset']['csv_field_opts'][$inst_id]['label']);
    $row[] = drupal_render($form['csv_field_opts_fielset']['csv_field_opts'][$inst_id]['selected']);
    $row[] = drupal_render($form['csv_field_opts_fielset']['csv_field_opts'][$inst_id]['weight']);

    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }
  $form['csv_field_opts_fielset']['csv_field_opts']['csv_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'citcon-csv-fields-table'),
  );

  // if this is a top supporters form... return here
  if (!empty($form['#csv_top_supporters'])) {
    return drupal_render_children($form);
  }

  // Add the "Filters" table.
  if (isset($form['csv_filters']['csv_field_filters'])) {
    $header = array(t('Enable Filter'), t('Filter'));
    $rows = array();
    foreach (element_children($form['csv_filters']['csv_field_filters']) as $inst_id) {

      $row = array();
      $row[] = drupal_render($form['csv_filters']['csv_field_filters'][$inst_id]['filter_enabled']);
      $row[] = drupal_render($form['csv_filters']['csv_field_filters'][$inst_id]['filter']);

      $rows[] = $row;
    }
    $form['csv_filters']['csv_field_filters']['csv_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }

  return drupal_render_children($form);
}

/**
 * Returns CSV formatted string.
 *
 * @param $variables
 *   An associative array containing:
 *   - fields: Array of fields to return, with the label key for each field.
 *   - supporters: Array of supporter objects to pull the data from.
 *
 * @ingroup themeable
 */
function theme_citcon_csv_csv($variables) {
  $fields = array_filter($variables['fields']);
  $supporters = $variables['supporters'];
  $options = $variables['options'];
  $rows = array();
  // Start the file with the labels
  foreach($fields as $field) {
    // If no label, move on.
    if (empty($field['label'])) {
      continue;
    }

    $row[] = $field['label'];
  }
  $rows[] = '"' . implode('","', $row) . '"';

  // For each supporter, lets get field values
  foreach ($supporters as $supporter) {
    // Reset rows
    $row = array();
    // And values
    $values = array();

    // if it's a 'top supporters' list... we're expecting a different array
    // Which means a different "source" for our data.
    if (!empty($options['top_sups'])) {
      $values = citcon_get_supporter_info($supporter['email']);
    }

    foreach($fields as $field) {
      // If no label, move on.
      if (empty($field['label'])) {
        continue;
      }

      // Our "created" field is a special case... we'll handle it here.
      if ($field['id'] == 'created') {
        $row[] = date('c', $supporter->created);
        continue;
      }

      // if it's not a 'top supporters' list... we need to load the data per field
      if (empty($options['top_sups'])) {
        $supporter->sid = NULL;
        $values[$field['id']] = field_get_items('citcon_supporter', $supporter, $field['id']);
      }

      $row[] = isset($values[$field['id']]) ? str_replace('"', '""', $values[$field['id']][0][$field['column']]) : '';
    }
    $rows[] = '"' . implode('","', $row) . '"';
  }

  return implode("\r\n", $rows);
}

/**
 * Callback page to create CSV file.
 *
 * @param $hash
 *   String - The hash string used for the tmp variable.
 *
 * @return If the hash is real and user has permissions, this function
 * sets the headers for a CSV file and dumps the data. Otherwise,
 * we return a page not found.
 */
function citcon_csv_menu_callback($hash) {
  $cid = 'citcon_csv-' . $hash;

  // We simply want to return a "Page not found"
  // instead of "Access denied".
  // Just feel like an "Access denied" message almost sounds like a challenge.
  // so with a "page not found" people would be more like "Oh, I guess this
  // isn't the page I'm looking for."... jedi styles ;)
  if (!user_access('admin citcon campaigns') || !($data = cache_get($cid))) {
    return MENU_NOT_FOUND;
  }

  // Set a 10 min time limit.
  drupal_set_time_limit(600);

  // Get the options data from the cache data
  $options = $data->data;

  // Get the csv text
  $return = citcon_csv_generate_csv($options);

  // Set the filename and replace global tokens
  $filename = isset($options['csv_options']['filename']) ? $options['csv_options']['filename'] : 'CitCon-Supporters-[current-date:custom:M-j-y-hia].csv';
  $filename = token_replace($filename);

  // Set the headers and dump the text
  header("Content-type: application/csv");
  header("Content-Disposition: attachment; filename=" . $filename);
  print $return;

  //cache_clear_all($cid, 'cache');
}
