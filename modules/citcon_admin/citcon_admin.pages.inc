<?php

/**
 * @file
 * Pages used by the admin module.
 */

/**
 * Page callback for the main admin page
 *
 * @todo Once you can select from different nodetypes to create
 * a campaign, we should use ctools jump list to create a slick
 * interface to create different types of campaigns.
 */
function citcon_admin_page() {

  $output = array();

  // Add Campaign
  // TODO: Make this a ctools jump list.
  $output['add_campaign'] = array(
    '#markup' => l(t('Create a new campaign'), 'node/add/citcon-campaign'),
  );

  // Get campaigns
  $campaigns = array();

  $campaigns['active'] = citcon_get_all_active_campaigns();
  sort($campaigns['active']);

  $campaigns['inactive'] = citcon_get_all_inactive_campaigns();
  sort($campaigns['inactive']);

  $campaigns['test'] = citcon_get_all_test_campaigns();
  sort($campaigns['test']);

  $translations = citcon_get_all_campaign_translations();
  // Remove campaigns which are a translation of another.
  if (module_exists('citcon_locale')) {
    foreach ($translations as $tnid => $nids) {
      // Holy
      foreach ($nids as $nid) {
        // Nesting
        foreach ($campaigns as $type => $camp_nids) {
          // Batman
          $key = array_search($nid, $camp_nids);
          if ($key) {
            unset($campaigns[$type][$key]);
          }
        }
      }
    }
  }

  // Group the campaign list by status
  if (!empty($campaigns['active'])) {
    $output['active_list'] = array(
      '#theme' => 'citcon_admin_campaign_list',
      '#nids' => $campaigns['active'],
      '#tnids' => $translations,
      '#title' => t('Active Campaigns (@num)', array('@num' => count($campaigns['active']))),
    );
  }
  // If there's nothing in the active campaign section... at least say so.
  else {
    $output['nothing_active'] = array(
      '#markup' => t("<h3><em>There are currently no active campaigns.</em></h3>"),
    );
  }
  if (!empty($campaigns['test'])) {
    $output['test_list'] = array(
      '#theme' => 'citcon_admin_campaign_list',
      '#nids' => $campaigns['test'],
      '#tnids' => $translations,
      '#title' => t('Test Campaigns (@num)', array('@num' => count($campaigns['test']))),
    );
  }
  if (!empty($campaigns['inactive'])) {
    $output['inactive_list'] = array(
      '#theme' => 'citcon_admin_campaign_list',
      '#nids' => $campaigns['inactive'],
      '#tnids' => $translations,
      '#title' => t('Inactive Campaigns (@num)', array('@num' => count($campaigns['inactive']))),
    );
  }

  return $output;
}

/**
 * Page callback for particular campaign restuls
 *
 * @param $node
 *   Object - Campaign node object for which to gather results
 *
 * @return The results page.
 */
function citcon_admin_campaign_results_page($node) {

  $supporters = array();
  // Not used: $supporters['all'] = citcon_get_all_supporters($node->nid);
  $supporters['total'] = citcon_get_total_supporters('all', $node->nid);
  $supporters['unique'] = citcon_get_total_supporters('unique', $node->nid);
  $supporters['repeat-unique'] = citcon_get_total_supporters('repeat-unique', $node->nid);

  // Figure out how many top supporters to show... if any
  if ($supporters['repeat-unique'] > 3) {
    $num_of_top_sups = 3;
    if ($supporters['repeat-unique'] > 100) {
      $num_of_top_sups = 25;
    }
    elseif ($supporters['repeat-unique'] > 50) {
      $num_of_top_sups = 15;
    }
    elseif ($supporters['repeat-unique'] > 30) {
      $num_of_top_sups = 10;
    }
    elseif ($supporters['repeat-unique'] > 20) {
      $num_of_top_sups = 8;
    }
    elseif ($supporters['repeat-unique'] > 10) {
      $num_of_top_sups = 5;
    }
    $supporters['top'] = citcon_get_top_supporters($num_of_top_sups, $node->nid);
  }

  // Show the totals
  $output = array();
  $output['totals'] = array(
    '#theme' => 'table',
    '#header' => array(),
    '#rows' => array(
      array(t('Number of Entries'), $supporters['total']),
      array(t('Number of Unique Entries'), $supporters['unique']),
      array(t('Number of Repeat Supporters'), $supporters['repeat-unique']),
    ),
  );

  // Display the top supporters and their info.
  if (isset($supporters['top']) && !empty($supporters['top'])) {
    $handle = t('These are your top @num supporters', array('@num' => count($supporters['top'])));
    foreach ($supporters['top'] as $i => $top) {
      $content['top_supporters']['top_supporter_' . $i] = array(
        '#theme' => 'table',
        '#header' => array(
          array(
            'data' => t('#@num - @display_name', array('@num' => $i + 1, '@display_name' => $top['display_name'])),
            'colspan' => 2,
          )
        ),
        '#rows' => array(
          array(t('Number of Times Signed'), count($top['data'])),
          array(t('First Time Signed'), date("D, M jS 'y - g:ia", $top['first']->created)),
          array(t('Last Time Signed'), date("D, M jS 'y - g:ia", $top['last']->created)),
          array(t('Number of Campaigns Supported'), count($top['campaigns'])),
        ),
      );
    }
    $output['top_supporters'] = array(
      '#prefix' => '<div class="citcon-form-group">',
      '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => drupal_render($content), 'collapsed' => TRUE)),
      '#suffix' => '</div>',
    );

    // If citcon_csv enabled, add the Top Supporters CSV form in a fieldset.
    if (module_exists('citcon_csv')) {
      $handle = t('Download Top Supporters CSV Results');
      $csv_top_form = drupal_get_form('citcon_csv_top_form', $node);
      $content = drupal_render($csv_top_form);
      $output['csv_top_form_group'] = array(
        '#prefix' => '<div class="citcon-form-group">',
        '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, 'collapsed' => TRUE)),
        '#suffix' => '</div>',
      );
    }
  }

  // If citcon_csv enabled, add the All CSV form in a fieldset.
  if (module_exists('citcon_csv')) {
    $handle = t('Download All CSV Results');
    $csv_form = drupal_get_form('citcon_csv_form', $node);
    $content = drupal_render($csv_form);
    $output['csv_all_form_group'] = array(
      '#prefix' => '<div class="citcon-form-group">',
      '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, 'collapsed' => TRUE)),
      '#suffix' => '</div>',
    );
  }
  return $output;
}

/**
 * Page callback for the all results tab
 *
 */
function citcon_admin_all_results_page() {
  $campaigns = array();
  $campaigns['all'] = citcon_get_all_campaigns();
  $supporters = array();
  // Not used: $supporters['all'] = citcon_get_all_supporters();
  $supporters['total'] = citcon_get_total_supporters('all');
  $supporters['unique'] = citcon_get_total_supporters('unique');
  $supporters['repeat-unique'] = citcon_get_total_supporters('repeat-unique');

  // Figure out how many top supporters to show... if any
  if ($supporters['repeat-unique'] > 3) {
    $num_of_top_sups = 3;
    if ($supporters['repeat-unique'] > 500) {
      $num_of_top_sups = 25;
    }
    elseif ($supporters['repeat-unique'] > 350) {
      $num_of_top_sups = 15;
    }
    elseif ($supporters['repeat-unique'] > 200) {
      $num_of_top_sups = 10;
    }
    elseif ($supporters['repeat-unique'] > 100) {
      $num_of_top_sups = 8;
    }
    elseif ($supporters['repeat-unique'] > 50) {
      $num_of_top_sups = 5;
    }
    $supporters['top'] = citcon_get_top_supporters($num_of_top_sups);
  }

  // Show the totals
  $output = array();
  $output['totals'] = array(
    '#theme' => 'table',
    '#header' => array(),
    '#rows' => array(
      array(t('Number of Campaigns'), count($campaigns['all'])),
      array(t('Number of Entries'), $supporters['total']),
      array(t('Number of Unique Entries'), $supporters['unique']),
      array(t('Number of Repeat Supporters'), $supporters['repeat-unique']),
    ),
  );

  // Display the top supporters and their info.
  if (isset($supporters['top']) && !empty($supporters['top'])) {
    $handle = t('These are your top @num supporters', array('@num' => count($supporters['top'])));
    foreach ($supporters['top'] as $i => $top) {
      $content['top_supporters']['top_supporter_' . $i] = array(
        '#theme' => 'table',
        '#header' => array(
          array(
            'data' => t('#@num - @display_name', array('@num' => $i + 1, '@display_name' => $top['display_name'])),
            'colspan' => 2,
          )
        ),
        '#rows' => array(
          array(t('Number of Times Signed'), count($top['data'])),
          array(t('First Time Signed'), date("D, M jS 'y - g:ia", $top['first']->created)),
          array(t('Last Time Signed'), date("D, M jS 'y - g:ia", $top['last']->created)),
          array(t('Number of Campaigns Supported'), count($top['campaigns'])),
        ),
      );
    }
    $output['top_supporters'] = array(
      '#prefix' => '<div class="citcon-form-group">',
      '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => drupal_render($content), 'collapsed' => TRUE)),
      '#suffix' => '</div>',
    );

    // If citcon_csv enabled, add the Top Supporters CSV form in a fieldset.
    if (module_exists('citcon_csv')) {
      $handle = t('Download Top Supporters CSV Results');
      $csv_top_form = drupal_get_form('citcon_csv_top_form');
      $content = drupal_render($csv_top_form);
      $output['csv_top_form_group'] = array(
        '#prefix' => '<div class="citcon-form-group">',
        '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, 'collapsed' => TRUE)),
        '#suffix' => '</div>',
      );
    }
  }

  // If citcon_csv enabled, add the All CSV form in a fieldset.
  if (module_exists('citcon_csv')) {
    $handle = t('Download All CSV Results');
    $csv_form = drupal_get_form('citcon_csv_form');
    $content = drupal_render($csv_form);
    $output['csv_all_form_group'] = array(
      '#prefix' => '<div class="citcon-form-group">',
      '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, 'collapsed' => TRUE)),
      '#suffix' => '</div>',
    );
  }
  return $output;
}

/**
 * Config form
 */
function citcon_admin_config_form($form, &$form_state) {
  $form = array();
  $form_state['plugins_enabled'] = array();
  $form_state['plugins_settings'] = array();

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['plugins'] = array(
    '#weight' => -10,
    '#type' => 'fieldset',
    '#title' => t('Enable/Disable Plugins'),
    '#group' => 'tabs',
  );

  // Add a checkbox for every plugin
  if ($plugins = citcon_get_all_plugins()) {
    foreach ($plugins as $plugin) {
      // List each as checkbox
      $form['plugins'][$plugin['type']][$plugin['name']] = array(
        '#type' => 'checkbox',
        '#title' => t($plugin['title']),
        '#description' => t($plugin['description']),
        '#default_value' => variable_get('citcon_' . $plugin['name'] . '_is_enabled', TRUE),
      );

      // Add each to form_state array for easy retrieval during submit.
      $form_state['plugins_enabled'][] = $plugin['name'];
    }
  }

  // Set data_process fieldset
  if (isset($form['plugins']['data_process'])) {
    $form['plugins']['data_process'] += array(
      '#type' => 'fieldset',
      '#title' => t('Data Processing Plugins'),
      '#description' => t('CRM Integration, Export to other DBs, and other things'),
    );
  }
  // Set target fieldset
  if (isset($form['plugins']['target'])) {
    $form['plugins']['target'] += array(
      '#type' => 'fieldset',
      '#title' => t('Auto Target Plugins'),
      '#description' => t('Lookup email targets plugins'),
    );
  }
  // Set sharing fieldset
  if (isset($form['plugins']['sharing'])) {
    $form['plugins']['sharing'] += array(
      '#weight' => 10,
      '#type' => 'fieldset',
      '#title' => t('Sharing Plugins'),
      '#description' => t('Social Media and Sharing'),
    );
  }
  // Set settings fieldset
  if (isset($form['plugins']['settings'])) {
    $form['plugins']['settings'] += array(
      '#weight' => 10,
      '#type' => 'fieldset',
      '#title' => t('Settings Plugins'),
      '#description' => t('General Settings Plugins'),
    );
  }
  // Set petition fieldset
  if (isset($form['plugins']['petition'])) {
    $form['plugins']['petition'] += array(
      '#weight' => 10,
      '#type' => 'fieldset',
      '#title' => t('Petition Plugins'),
      '#description' => t('Plugins for Petitions'),
    );
  }

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );

  /**
   * plugin_point - citcon_config settings
   */
  // Add plugin options to "Settings" page
  // Get settings forms
  foreach ($plugins as $plugin) {
    if (variable_get('citcon_' . $plugin['name'] . '_is_enabled', TRUE) && $function = ctools_plugin_get_function($plugin, 'citcon_config settings')) {
      // Create the fieldset, and attach form elements
      $form['citcon_config_' . $plugin['name'] . '_settings'] = array(
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#group' => 'tabs',
        '#title' => t($plugin['title']),
        '#description' => t($plugin['description']),
          ) + $function($form, $form_state, variable_get('citcon_config_' . $plugin['name'] . '_settings', array()));

      // Add each to form_state array for easy retrieval during submit.
      $form_state['plugins_settings'][] = $plugin['name'];
    }
  }

  return $form;
}

/**
 * Config form
 */
function citcon_admin_config_form_submit($form, &$form_state) {
  // Save any Enable/Disable data
  foreach ($form_state['plugins_enabled'] as $plugin_name) {
    variable_set('citcon_' . $plugin_name . '_is_enabled', $form_state['values'][$plugin_name]);
  }

  // Save any settings for plugins
  foreach ($form_state['plugins_settings'] as $plugin_name) {
    variable_set('citcon_config_' . $plugin_name . '_settings', $form_state['values']['citcon_config_' . $plugin_name . '_settings']);
  }
}

/**
 * Menu callback
 */
function citcon_admin_update_fields_page() {
  // Load the install include file...
  module_load_include('inc', 'citcon', 'includes/install');

  $campaign_types = citcon_get_all_campaign_types();

  foreach ($campaign_types as $type) {
    $instances = array_keys(field_info_instances('node', $type->type));

    // Create all the fields we are adding to our content type.
    foreach (_citcon_installed_campaign_fields() as $field) {
      if (!in_array($field['field_name'], $instances)) {
        $field['entity_types'] = array('node');
        $field['locked'] = TRUE;
        field_create_field($field);
      }
    }

    // Create all the instances for our fields.
    foreach (_citcon_installed_campaign_instances() as $instance) {
      if (!in_array($instance['field_name'], $instances)) {
        $instance['entity_type'] = 'node';
        $instance['bundle'] = $type->type;
        if (!isset($instance['display'])) {
          $instance['display'] = array(
            'default' => array(
              'label' => 'hidden',
              'type' => 'hidden',
            ),
          );
        }
        field_create_instance($instance);
      }
    }
  }

  // TODO: Come up with some better error reporting.
  return 'Done... any errors? I hope not.';
}
