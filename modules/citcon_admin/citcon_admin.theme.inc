<?php

/**
 * @file
 * Theme functions for the admin module.
 */

/**
 * Returns HTML for an admin list of campaigns.
 *
 * @param $variables
 *   An associative array containing:
 *   - nids: The nids to list.
 *   - title: The title of the list.
 *
 * @return Returns HTML output.
 */
function theme_citcon_admin_campaign_list($variables) {
  // Get and init variables
  $nids = $variables['nids'];
  $tnids = $variables['tnids'];
  $handle = $variables['title'];

  // Create a table that we can have data removed from via AJAX.
  $header = array(t('Name'), t('Operations'));

  // Add language columns if we need to
  if (module_exists('citcon_locale')) {
    $header[] = t('Language');
    $header[] = t('Translations');
  }

  $rows = array();
  foreach ($nids as $nid) {
    $row = array();
    $node = node_load($nid);

    // Add the title, as a link.
    $row[] = l($node->title, 'node/' . $nid);

    // Add Operations to row.
    $operations = array(
      l(t('view'), 'node/' . $nid),
      l(t('edit'), 'node/' . $nid . '/edit'),
      l(t('results'), 'node/' . $nid . '/results'),
      l(t('clone'), 'node/add/' . str_replace('_', '-', $node->type), array('query' => array('clone' => $node->nid))),
    );
    $row[] = implode('&nbsp;&nbsp;&nbsp;', $operations);

    // Add language columns if we need to
    if (module_exists('citcon_locale')) {
      // Add Campaign language info
      $row[] = $node->language;

      // Add translations info
      $translations = array();
      if (isset($tnids[$nid])) {
        // if the node has translations... show their link!
        foreach ($tnids[$nid] as $tnid) {
          $t_node = node_load($tnid);
          $translations[] = l($t_node->language, 'node/' . $t_node->nid);
        }
      }
      $row[] = implode('&nbsp;&nbsp;&nbsp;', $translations);
    }

    // Create a new row
    $rows[] = $row;
  }

  // Render the table
  $content = theme('table', array('header' => $header, 'rows' => $rows));

  // Attach it to a ctools_collapsible div, and return it!
  return theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, 'collapsed' => FALSE));
}

/**
 * Returns HTML for the CSV download form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_citcon_admin_find_supporters_form($variables) {
  $form = $variables['form'];

  // Add the "Filters" table.
  $header = array(t('Enable Filter'), t('Filter'));
  $rows = array();
  foreach (element_children($form['filters']['field_filters']) as $inst_id) {

    $row = array();
    $row[] = drupal_render($form['filters']['field_filters'][$inst_id]['filter_enabled']);
    $row[] = drupal_render($form['filters']['field_filters'][$inst_id]['filter']);

    $rows[] = $row;
  }
  $form['filters']['field_filters']['csv_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return drupal_render_children($form);
}
