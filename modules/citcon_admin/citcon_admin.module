<?php

/**
 * @file
 * Citizens Connected admin module. UI for admin-ing CitCon.
 */

/**
 * Implements hook_menu()
 */
function citcon_admin_menu() {
  $items = array();

  // CitCon Campaign config pages
  $items['admin/config/system/citcon'] = array(
    'title' => t('Citizens Connected'),
    'description' => 'Change site-wide Citizens Connected settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('citcon_admin_config_form'),
    'access arguments' => array('admin citcon config'),
    'file' => 'citcon_admin.pages.inc',
  );
  $items['admin/config/system/citcon/config'] = array(
    'title' => t('Citizens Connected'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/config/system/citcon/update-fields'] = array(
    'title' => t('Update Fields'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'citcon_admin_update_fields_page',
    'access arguments' => array('admin citcon config'),
    'file' => 'citcon_admin.pages.inc',
    'weight' => 0,
  );


  // CitCon Campaign admin pages
  $items['admin/citcon'] = array(
    'title' => t('Citizens Connected Dashboard'),
    'page callback' => 'citcon_admin_page',
    'access arguments' => array('view campaign results'),
    'file' => 'citcon_admin.pages.inc',
  );
  $items['admin/citcon/list'] = array(
    'title' => t('List Campaigns'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'page callback' => 'citcon_admin_page',
    'access arguments' => array('view campaign results'),
    'file' => 'citcon_admin.pages.inc',
    'weight' => -10,
  );
  $items['admin/citcon/results'] = array(
    'title' => t('Results'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'citcon_admin_all_results_page',
    'access arguments' => array('view campaign results'),
    'file' => 'citcon_admin.pages.inc',
    'weight' => 0,
  );
  $items['node/%node/results'] = array(
    'title' => t('Results'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'citcon_admin_campaign_results_page',
    'page arguments' => array(1),
    'access arguments' => array('view campaign results'),
    'file' => 'citcon_admin.pages.inc',
    'weight' => 0,
  );
  $items['node/%node/results/view'] = array(
    'title' => t('View Results'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['node/%node/results/find'] = array(
    'title' => t('Find Supporters'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('citcon_admin_find_supporters_form', 1),
    'access arguments' => array('view supporters'),
    'file' => 'citcon_admin.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function citcon_admin_theme() {
  return array(
    'citcon_admin_campaign_list' => array(
      'variables' => array('nids' => array(), 'tnids' => array(), 'title' => ''),
      'file' => 'citcon_admin.theme.inc',
    ),
    'citcon_admin_find_supporters_form' => array(
      'render element' => 'form',
      'file' => 'citcon_admin.theme.inc',
    ),
  );
}

/**
 * Supporter search filters form
 *
 * @param $form
 *   Array - The form array.
 *
 * @param $form_state
 *   Array - The form_state array.
 *
 * @param $node
 *   Object - Optional - Campagin node to create filters from... unless
 *   this is a global supporter search.
 *
 * @return Form Array, ready to be used
 *
 * @todo Look into a better way to get field values, and meta info like columns.
 */
function citcon_admin_find_supporters_form($form, &$form_state, $node = NULL) {
  global $language;

  // Define parents early so the field_default_form will stop complaining
  $form['#parents'] = array();

  // Get all the supporter types and their fields
  $supporter_types = field_info_instances('citcon_supporter');

  // Get/Set vars for particular campaign.
  if (!empty($node) && isset($node->nid)) {
    $campaign_values = citcon_get_campaign_info($node);
    $supporter_types = array(
      $campaign_values['citcon_fields_supporter_type'][0]['value'] => $supporter_types[$campaign_values['citcon_fields_supporter_type'][0]['value']],
    );

    // Set form campaign nid
    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $node->nid,
    );

    $form['csv_filters']['supporter_type'] = array(
      '#type' => 'value',
      '#value' => $campaign_values['citcon_fields_supporter_type'][0]['value'],
    );

    // Start a blank options array
    $supporter_types_options = array();
  }
  else {
    // Start the options array with the "All" option.
    $supporter_types_options = array(
      NULL => t('All'),
    );
  }

  $inst_options = array();
  foreach ($supporter_types as $supporter_type => $insts) {
    $query = new EntityFieldQuery();
    // Find all supporters
    $query->entityCondition('entity_type', 'citcon_supporter');
    // Of type
    $query->propertyCondition('type', $supporter_type);
    // And just count them
    $count = $query->count()->execute();

    $type_info = citcon_supporter_type_load($supporter_type);
    $supporter_types_options[$supporter_type] = $type_info->label . ' (' . $count . ')';

    // Now let's get one big array of ALL the fields
    foreach ($insts as $inst_id => $inst) {
      // Remove requirements and description.
      $inst['required'] = FALSE;
      $inst['description'] = '';
      // Then get the form element
      $inst_options += field_default_form('citcon_supporter', $node, field_info_field($inst_id), $inst, $language->language, NULL, $form, $form_state, 0);
    }
  }

  // Add the field options
  $weight_delta = round(count($inst_options) / 2);
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Supporter Search Filters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['filters']['date_from'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date From'),
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'm/d/Y - h:i a',
    '#date_increment' => 30,
    '#date_year_range' => '-3:+3',
    '#weight' => -30,
  );
  $form['filters']['date_to'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date To'),
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'm/d/Y - h:i a',
    '#date_increment' => 30,
    '#date_year_range' => '-3:+3',
    '#weight' => -25,
  );

  $form['filters']['field_filters'] = array(
    '#tree' => TRUE,
  );

  // For each of the field instances, let's add it's form element.
  foreach ($inst_options as $inst_id => $inst_form) {
    $form['filters']['field_filters'][$inst_id]['filter'] = $inst_form;
    $form['filters']['field_filters'][$inst_id]['filter_enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => t('Enable Filter'),
      '#title_display' => 'invisible',
    );
  }

  // Records per page, for pager.
  //$form['num_per_page'] = array(
  //  '#type' => 'select',
  //  '#title' => t('Number of results per page'),
  //  '#options' => array(
  //    2 => 2,
  //    5 => 5,
  //    10 => 10,
  //    25 => 25,
  //    50 => 50,
  //    100 => 100,
  //  ),
  //  '#default_value' => isset($form_state['input']['num_per_page']) ? $form_state['input']['num_per_page'] : 10,
  //  '#weight' => 35,
  //);
  // Add our actions
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['find'] = array(
    '#type' => 'button',
    '#value' => t('Find'),
    '#weight' => 40,
  );


  // ********************
  // Form ends here... since we always want to display results... just do it here.
  // ********************
  // Build options array for citcon_get_supporters()
  $options = array(
    'nid' => isset($node->nid) ? $node->nid : NULL,
  );

  // if we need to... let's keep buliding our options.
  if (isset($form_state['values']['field_filters'])) {
    foreach ($form_state['values']['field_filters'] as $inst_id => $value) {
      // if we didn't select it... don't care about it.
      if (!$value['filter_enabled']) {
        unset($form_state['values']['field_filters'][$inst_id]);
        continue;
      }

      // This feels like a hack way of getting the column value.
      // TODO: Find a better way to get the col val.
      $field_info = field_info_field($inst_id);
      $key = array_keys($field_info['columns']);
      // Let's add the field data to our options
      $options['field_filters'][$inst_id]['column'] = $key[0];
      $options['field_filters'][$inst_id]['value'] = $value['filter'][$language->language][0][$key[0]];
    }
  }

  // Get our list of supporters
  $supporters = citcon_get_supporters($options, 20);
  $views = citcon_supporter_view_multiple($supporters);

  // Display them
  $results = '';
  $results .= drupal_render($views);
  $results .= theme('pager');
  $form['results'] = array(
    '#markup' => $results,
    '#weight' => 999,
  );

  // Set rebuild to true so that we never "submit" and lose the filter data.
  $form_state['rebuild'] = TRUE;

  return $form;
}

/**
 * Implements hook_node_prepare().
 */
function citcon_admin_node_prepare($node) {
  // We're checking to see if we're trying to clone a campaign
  if (citcon_is_campaign_type($node->type) &&
      // And it's a new node.
      empty($node->nid) &&
      // And the $_GET variables are set properly.
      isset($_GET['clone']) &&
      is_numeric($_GET['clone'])) {

    // Get the source
    $source_node = node_load($_GET['clone']);
    if (empty($source_node) || !node_access('view', $source_node)) {
      // Source node not found or no access to view.
      return;
    }

    // Attach our source_node fields to the clone
    $node->title = $source_node->title;
    $instances = field_info_instances('node', $source_node->type);
    foreach ($instances as $instance) {
      $field_name = $instance['field_name'];
      $node->{$field_name} = $source_node->{$field_name};
    }
  }
}
