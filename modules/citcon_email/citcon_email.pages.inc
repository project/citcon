<?php

/**
 * @file
 * Pages used by the email module.
 */

/**
 * Display email addresses.
 *
 * @ingroup themeable
 */
function theme_citcon_email_display_addresses($variables) {

  $element = array(
    '#type' => 'item',
    '#title' => t($variables['address_type']),
    '#description' => implode('<br />', str_replace(array('<', '>'), array('&lt;', '&gt'), $variables['addresses'])),
  );

  return drupal_render($element);
}
