<?php

/**
 * @file
 * Citizens Connected email a target functionality. This is a major feature
 * of CitCon.
 */
/**
 * Email body field is locked, and uneditable by supporters.
 */
define('CITCON_EMAIL_BODY_LOCKED', 0);

/**
 * A portion of the email body field is locked, and another editable
 * textarea will be made available to them.
 */
define('CITCON_EMAIL_BODY_APPEND', 1);

/**
 * Email body field is not locked and editable by the supporter.
 */
define('CITCON_EMAIL_BODY_EDIT', 2);

/**
 * Implements hook_menu()
 */
function citcon_email_menu() {
  $items = array();

  // Common settings array for campaign admin pages
  $admin_base = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('citcon_campaign_form', 1, 3),
    'access arguments' => array('admin citcon campaigns'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/admin.inc',
    'file path' => drupal_get_path('module', 'citcon'),
  );

  // Campaign email settings page
  $items['node/%node/edit/email'] = array(
    'title' => 'Email',
    'weight' => -7,
      ) + $admin_base;

  return $items;
}

/**
 * Implements hook_form_admin_paths()
 */
function citcon_email_admin_paths() {
  $paths = array(
    'node/*/edit/email' => TRUE,
  );
  return $paths;
}

/**
 * Implements hook_form_alter()
 */
function citcon_email_form_alter(&$form, &$form_state, $form_id) {
  // Check for citcon node forms
  if (isset($form['#citcon_node_edit_form']) && $form['#citcon_node_edit_form']) {

    // Add the email plug in settings
    if (isset($form['#citcon_node_edit_group']) && $form['#citcon_node_edit_group'] == 'email'
        && $plugins = citcon_email_get_target_plugins()) {

      // Add our own submit handler, so we can save the plugin data
      $form['#validate'][] = 'citcon_email_campaign_form_validate';

      // Get plugin defaults... maybe from the ajaxy form_state
      if (isset($form_state['values']['citcon_plugin_targets'])) {
        $target_defaults = $form_state['values']['citcon_plugin_targets'];
      }
      else {
        $target_defaults = citcon_email_get_plugin_data('citcon_email_plugin_target', $form['#node']);
      }

      // Create the plugin tab
      $form['ccfs_autotarget'] = array(
        '#type' => 'fieldset',
        '#title' => t('Auto Target'),
        '#description' => t('Select and configure one of the "Auto Target" plug ins below. Your supporters will be able to find an email target that\s right for them.'),
        '#group' => 'tabs',
        '#weight' => 0,
      );
      // Set array tree so we can easily serialize all the plugin settings
      $form['ccfs_autotarget']['citcon_plugin_targets'] = array(
        '#prefix' => '<div id="citcon-email-target-plugin">',
        '#suffix' => '</div>',
        '#tree' => TRUE,
      );


      // Create the radio buttons for the plugins
      $plugin_options = array(NULL => 'None');
      foreach ($plugins as $plugin_key => $plugin_info) {
        $plugin_options[$plugin_key] = '<strong>' . $plugin_info['title'] . '</strong> - ' . $plugin_info['description'];
      }
      $form['ccfs_autotarget']['citcon_plugin_targets']['citcon_autotarget_plugin'] = array(
        '#type' => 'radios',
        '#title' => t('Auto Target Plugin Selection'),
        '#options' => $plugin_options,
        '#default_value' => isset($target_defaults['citcon_autotarget_plugin']) ? $target_defaults['citcon_autotarget_plugin'] : NULL,
        '#ajax' => array(
          'callback' => 'citcon_email_target_plugin_ajax_callback',
          'wrapper' => 'citcon-email-target-plugin',
          'method' => 'replace',
          'effect' => 'fade',
        ),
      );

      // Get target defaults data
      foreach ($plugins as $plugin) {
        if ($function = ctools_plugin_get_function($plugin, 'settings form')) {

          // Create the fieldset, and attach form elements
          $form['ccfs_autotarget']['citcon_plugin_targets'][$plugin['name']] = array(
            '#type' => 'fieldset',
            '#title' => t($plugin['title']),
              ) + $function($form, $form_state, $target_defaults[$plugin['name']]);
        }

        // If it's not selected, hide it.
        if ($plugin['name'] != $target_defaults['citcon_autotarget_plugin']) {
          $form['ccfs_autotarget']['citcon_plugin_targets'][$plugin['name']]['#prefix'] = '<div class="citcon-css-hide">';
          $form['ccfs_autotarget']['citcon_plugin_targets'][$plugin['name']]['#suffix'] = '</div>';
        }
      }

      // If we've picked a plugin, we can set a "result" message using tokens.
      if (isset($target_defaults['citcon_autotarget_plugin'])) {
        // Get the title and content of token tree drop down.
        $handle = t('Tokens Supported') . ' <span class="citcon-email-lookup-tokens citcon-form-element-flag" title="This field supports Lookup Tokens">L-T</span>';
        $tree = array(
          '#theme' => 'token_tree',
          '#token_types' => array('cce-lookup-' . $target_defaults['citcon_autotarget_plugin']),
          '#global_types' => FALSE,
        );
        // Then render it.
        $drop_down = '<div class="citcon-token-trees">';
        $drop_down .= theme('ctools_collapsible', array('handle' => $handle, 'content' => drupal_render($tree), 'collapsed' => TRUE));
        $drop_down .= '</div>';

        // Add the "Result message" option
        $form['ccfs_autotarget']['citcon_plugin_targets']['result_msg'] = array(
          '#type' => 'textarea',
          '#rows' => 2,
          '#title' => t('Result Message'),
          '#description' => t('Change the resulting message. Should not contain HTML and be something like: We found @target, they will also receive this email.</em>') . $drop_down,
          '#weight' => 10,
          '#default_value' => isset($target_defaults['result_msg']) ? $target_defaults['result_msg'] : t('Your target is @target.'),
        );
      }
    }
  }
//dsm($form);
}

/**
 * Ajax callback for target plugins... plugins should use citcon_ajax_callback()
 * instead.
 */
function citcon_email_target_plugin_ajax_callback($form, $form_state) {
  return $form['ccfs_autotarget']['citcon_plugin_targets'];
}

/**
 * Helper function to load the plugin data
 *
 * @param $type
 *   String - The plugin type to load.
 *
 * @param $node
 *   Object - The campaign node to get the data from.
 *
 * @param $id
 *   String - Optional - The plugin ID of the particular plugin you'd
 *   like to load... or all the plugins if that's what you're after.
 *
 * @return An array containing the unserialized data.
 */
function citcon_email_get_plugin_data($type, $node, $id = NULL) {
  $value = citcon_get_campaign_info($node, $type);
  $data = unserialize($value[0]['value']);
  if (empty($id)) {
    return $data;
  }
  else {
    return $data[$id];
  }
}

/**
 * Fetch metadata for a particular plugin.
 *
 * @param $type
 *   String - The plug in type.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular target plugin.
 */
function citcon_email_get_plugin($type, $id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon_email', $type, $id);
}

/**
 * Fetch metadata for a particular target plugin.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular target plugin.
 */
function citcon_email_get_target_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon_email', 'target', $id);
}

/**
 * Fetch metadata for all target plugins.
 *
 * @return
 *   An array of arrays with information about all available auto target plugins.
 */
function citcon_email_get_target_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('citcon_email', 'target');
}

/**
 * Helper function for a repetative task
 *
 * @param $values
 *   Array - The email addresses. Usually from a
 *   campaign values array.
 *
 * @param $value_key
 *   String - The key to find the values in the array.
 *
 * @param $address_type
 *   String - The label to attach to the field.
 *
 * @return Returns renderable a array.
 */
function _citcon_email_add_addresses($values, $value_key, $address_type) {
  $addresses = array();

  // Get all addresses.
  foreach ($values as $value) {
    $addresses[] = $value[$value_key];
  }

  // Return themed array
  return array(
    '#theme' => 'citcon_email_display_addresses',
    '#address_type' => $address_type,
    '#addresses' => $addresses,
  );
}

/**
 * Implements hook_theme()
 */
function citcon_email_theme() {
  return array(
    'citcon_email_display_addresses' => array(
      'variables' => array('address_type' => 'To', 'addresses' => array()),
      'file' => 'citcon_email.pages.inc',
    ),
  );
}

/**
 * Validate handler for the campaign form, so we can save our
 * plugin data.
 */
function citcon_email_campaign_form_validate($form, &$form_state) {
  if (isset($form_state['values']['citcon_plugin_targets']) && $form['#citcon_node_edit_group'] == 'email') {
    // Get the field from the form so we can find the langcode
    $field = citcon_move_field($form, 'citcon_email_plugin_target');
    $langcode = $field['citcon_email_plugin_target']['#language'];
    unset($form_state['values']['citcon_email_plugin_target'][$langcode]);
    // Then serialize the data and set it to the field.
    $form_state['values']['citcon_email_plugin_target'][$langcode][0]['value'] = serialize($form_state['values']['citcon_plugin_targets']);
  }
}

/**
 * Submit handler for email functionality on
 * supporter form
 */
function citcon_email_form_citcon_supporter_form_submit($form, &$form_state) {
  global $language;

  // Get To, Cc, and Bcc info
  $campaign_node = node_load($form['nid']['#value']);
  $campaign_values = citcon_get_campaign_info($campaign_node);
  citcon_email_prepare_form_values($campaign_values, $campaign_node, $form_state);

  if (isset($form_state['citcon_email']['to'])) {
    $to = implode(', ', $form_state['citcon_email']['to']);
  }
  if (isset($form_state['citcon_email']['cc'])) {
    $params['cc'] = implode(', ', $form_state['citcon_email']['cc']);
  }
  if (isset($form_state['citcon_email']['bcc'])) {
    $params['bcc'] = implode(', ', $form_state['citcon_email']['bcc']);
  }

  // Now get Subject
  switch ($form_state['citcon_email']['subject_opt']) {
    case CITCON_CHECKBOX_OFF :
      $params['subject'] = $form_state['citcon_email']['subject'][0]['value'];
      break;
    case CITCON_CHECKBOX_ON :
      $params['subject'] = $form_state['values']['citcon_subject'];
      break;
  }

  // And the body
  switch ($form_state['citcon_email']['body_opt']) {
    case CITCON_EMAIL_BODY_LOCKED :
      $params['body'] = $form_state['citcon_email']['body'][0]['value'];
      break;
    case CITCON_EMAIL_BODY_APPEND :
      $params['body'] = $form_state['citcon_email']['body'][0]['value'];
      $params['body'] .= "\r\n\r\n";
      $params['body'] .= $form_state['values']['citcon_body_append'];
      break;
    case CITCON_EMAIL_BODY_EDIT :
      $params['body'] = $form_state['values']['citcon_body'];
      break;
  }

  // And the signature
  if (!empty($form_state['citcon_email']['signature'])) {
    // Add our token replaced text.
    $params['body'] .= "\r\n\r\n";
    $params['body'] .= $form_state['citcon_email']['signature'];
  }

  // We'll need the campaign node.
  // Campaign status, plugins, tokens, and such.
  $campaign_node = node_load($form_state['values']['nid']);

  // Get "Previous Supporter" data if we have one.
  if (isset($form_state['citcon_supporter_lookup']) && !empty($form_state['citcon_supporter_lookup'])) {
    $previous_supporter = $form_state['citcon_supporter_lookup'];
    foreach (array_keys($form_state['field']) as $field_name) {
      $form_state['values'][$field_name] = $previous_supporter['combined_values']->{$field_name};
    }
  }

  // Create a supporter object for the token data
  $supporter = entity_create('citcon_supporter', $form_state['values']);
  // And get the campaign values
  $campaign_values = citcon_get_campaign_info($campaign_node);
  // Get the email field
  $email_field = citcon_get_supporter_email_field($campaign_node);

  // Get the supporters email address and make it the "from" email addy.
  $from = $supporter->{$email_field}['und'][0]['email'];

  // Replace tokens
  $params['subject'] = token_replace($params['subject'], array('cc-s' => $supporter, 'cc-c' => $campaign_node));
  $params['body'] = token_replace($params['body'], array('cc-s' => $supporter, 'cc-c' => $campaign_node));

  // Now we let the plugins alter the form
  $target_defaults = citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node);
  if (isset($target_defaults['citcon_autotarget_plugin']) && !empty($target_defaults['citcon_autotarget_plugin'])) {
    $plugin_id = $target_defaults['citcon_autotarget_plugin'];
    $plugin = citcon_email_get_target_plugin($plugin_id);

    if ($function = ctools_plugin_get_function($plugin, 'supporter form submit')) {
      $function($form, $form_state, $message);
    }
  }

  // Then we send out the email
  switch ($campaign_values['citcon_campaign_status'][0]['value']) {
    case CITCON_CAMPAIGN_ACTIVE:
      $message = drupal_mail('citcon', 'general', $to, $language, $params, $from);
      break;
    case CITCON_CAMPAIGN_TEST:
      // Get the "Test To" email addresses.
      $test_to = FALSE;
      if (isset($campaign_values['citcon_test_emails'])
          && !empty($campaign_values['citcon_test_emails'])) {
        $test_to = citcon_get_test_email_addresses($campaign_values);
      }

      // Find what test mode we're in.
      $test_mode = citcon_get_campaign_info($campaign_node, 'citcon_test_opt', $from);
      $test_mode = $test_mode[0]['value'];

      // If we're going to need debug text... get it.
      if ($test_mode == CITCON_TEST_DISPLAY
          || $test_mode == CITCON_TEST_EMAIL_DEBUG) {
        $debug_info = "\n\r";
        $debug_info .= '=========================';
        $debug_info .= "\n\r";
        $debug_info .= '"Campaign Action" Email Debug Info';
        $debug_info .= "\n\r";
        $debug_info .= '=========================';
        $debug_info .= "\n\r";
        $debug_info .= citcon_get_email_debug_info($to, $params, $from);
      }
      switch ($test_mode) {
        case CITCON_TEST_DISPLAY:
          drupal_set_message(nl2br($debug_info));
          break;
        case CITCON_TEST_EMAIL_DEBUG:
          $params['body'] .= $debug_info;
        case CITCON_TEST_EMAIL:
          if ($test_to) {
            $to = $test_to;
            unset($params['cc']);
            unset($params['bcc']);
            $email = drupal_mail('citcon', 'general', $to, $language, $params, $from);
          }
          break;
      }

      break;
    // If we're submitting a form and the campaign is inactive... strange.
    // Just redirect to campaign page.
    case CITCON_CAMPAIGN_INACTIVE:
      drupal_set_message(t('You tried to fill out a form for a campaign that is explired.'));
      drupal_goto('node/' . $campaign_node->nid);
      break;
  }
}

/**
 * Helper function that will prep the email values.
 *
 * @todo We've hard coded language UND here... not good!!!
 */
function citcon_email_prepare_form_values($campaign_values, $campaign_node, &$form_state) {

  // If we haven't yet... initialize the array.
  if (!isset($form_state['citcon_email'])) {
    $form_state['citcon_email'] = array(
      'to' => array(),
      'cc' => array(),
      'bcc' => array(),
    );
  }
  // To addresses
  if (!empty($campaign_values['citcon_email_to'])) {
    foreach ($campaign_values['citcon_email_to'] as $value) {
      if (!in_array($value['value'], $form_state['citcon_email']['to'])) {
        $form_state['citcon_email']['to'][] = $value['value'];
      }
    }
  }
  // Cc addresses
  if (!empty($campaign_values['citcon_email_cc'])) {
    foreach ($campaign_values['citcon_email_cc'] as $value) {
      if (!in_array($value['value'], $form_state['citcon_email']['cc'])) {
        $form_state['citcon_email']['cc'][] = $value['value'];
      }
    }
  }
  // Bcc addresses
  if (!empty($campaign_values['citcon_email_bcc'])) {
    foreach ($campaign_values['citcon_email_bcc'] as $value) {
      if (!in_array($value['value'], $form_state['citcon_email']['bcc'])) {
        $form_state['citcon_email']['bcc'][] = $value['value'];
      }
    }
  }

  // "Send Myself a Copy of This Email" functionality
  $bcc_me_field = citcon_get_supporter_field('bcc_me', $campaign_node);
  $email_field = citcon_get_supporter_email_field($campaign_node);
  if (!empty($bcc_me_field) && !empty($email_field)
      // if we have fields, and they're not empty
      // TODO: Hard coding UND language... bad!
      && isset($form_state['input'][$bcc_me_field]['und']) && !empty($form_state['input'][$bcc_me_field]['und'])
      && isset($form_state['input'][$email_field]['und'][0]) && !empty($form_state['input'][$email_field]['und'][0])) {

    // Add the user's email to the bcc.
    $form_state['citcon_email']['bcc'][] = $form_state['input'][$email_field]['und'][0]['email'];
  }

  // Subject Option
  $form_state['citcon_email']['subject_opt'] = $campaign_values['citcon_email_subject_opt'][0]['value'];
  // Subject
  if (!isset($form_state['citcon_email']['subject']) || empty($form_state['citcon_email']['subject'])) {
    $form_state['citcon_email']['subject'] = $campaign_values['citcon_email_subject'];
  }

  // Body Option
  $form_state['citcon_email']['body_opt'] = $campaign_values['citcon_email_body_opt'][0]['value'];
  // Body
  if (!isset($form_state['citcon_email']['body']) || empty($form_state['citcon_email']['body'])) {
    $form_state['citcon_email']['body'] = $campaign_values['citcon_email_body'];
  }

  // Signature
  $form_state['citcon_email']['signature'] = $campaign_values['citcon_email_signature'][0]['value'];
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function citcon_email_form_citcon_supporter_form_alter(&$form, &$form_state, $form_id) {
  // If this is not a new supporter... don't do any funny stuff.
  $supporter = $form['#supporter'];
  if (!(isset($supporter->is_new) && $supporter->is_new)) {
    return;
  }

  $campaign_node = node_load($form['nid']['#value']);
  $campaign_values = citcon_get_campaign_info($campaign_node);

  // If it's not an email campaign... don't do anything.
  if ($campaign_values['citcon_email_status'][0]['value'] == CITCON_CHECKBOX_OFF) {
    return;
  }

  citcon_email_prepare_form_values($campaign_values, $campaign_node, $form_state);

  // Add our submit handler
  $form['#submit'][] = 'citcon_email_form_citcon_supporter_form_submit';

  // Set the weight of the 'Additional Info' fieldset.
  $form['citcon_additional_info']['#weight'] = 10;
  // Move appropriate fields over to the 'Additional Info' fieldset
  foreach (array_keys($form_state['field']) as $field_name) {
    if (isset($form[$field_name])) {
      $form['citcon_additional_info'][$field_name] = $form[$field_name];
      unset($form[$field_name]);
    }
  }

  // Add our email fieldset
  $form['citcon_email_info'] = array(
    '#type' => 'fieldset',
    '#title' => t($campaign_values['citcon_email_ef_title'][0]['value']),
    '#weight' => 0,
    '#description' => !empty($campaign_values['citcon_email_ef_desc'][0]['value']) ? t($campaign_values['citcon_email_ef_desc'][0]['value']) : '',
    '#prefix' => '<div id="citcon-email-info">',
    '#suffix' => '</div>',
  );

  // Add "To" fields... if any.
  if (!empty($form_state['citcon_email']['to'])) {
    $form['citcon_email_info']['display_to'] = array(
      '#theme' => 'citcon_email_display_addresses',
      '#address_type' => 'To:',
      '#addresses' => $form_state['citcon_email']['to'],
    );
  }
  // Add "Cc" fields... if any.
  if (!empty($form_state['citcon_email']['cc'])) {
    $form['citcon_email_info']['display_cc'] = array(
      '#theme' => 'citcon_email_display_addresses',
      '#address_type' => 'Cc:',
      '#addresses' => $form_state['citcon_email']['cc'],
    );
  }

  // Add "Bcc" fields... if any.
  // And if we want to show them...
  if (!empty($form_state['citcon_email']['bcc']) && !empty($campaign_values['citcon_email_bcc_opt'][0]['value'])) {
    $form['citcon_email_info']['display_bcc'] = array(
      '#theme' => 'citcon_email_display_addresses',
      '#address_type' => 'Bcc:',
      '#addresses' => $form_state['citcon_email']['bcc'],
    );
  }

  // Add "Subject" field
  if (!empty($campaign_values['citcon_email_subject'])
      || $form_state['citcon_email']['subject_opt'] == CITCON_CHECKBOX_ON
      || !empty($form_state['citcon_email']['subject'][0]['value'])) {
    switch ($form_state['citcon_email']['subject_opt']) {
      case CITCON_CHECKBOX_ON :
        $form['citcon_email_info']['citcon_subject'] = array(
          '#type' => 'textfield',
          '#title' => t('Subject'),
          '#default_value' => $form_state['citcon_email']['subject'][0]['value'],
          '#required' => TRUE,
        );
        // If we're here, but not submitting... we're probabbly here for ajaxy reasons and should
        if (isset($form_state['citcon_email_target']['rebuild_display'])) {
          //dsm('changing');
          $form['citcon_email_info']['citcon_subject']['#value'] = $form_state['citcon_email']['subject'][0]['value'];
        }
        break;
      case CITCON_CHECKBOX_OFF :
        $form['citcon_email_info']['display_subject'] = array(
          '#type' => 'item',
          '#title' => t('Subject'),
          '#description' => $form_state['citcon_email']['subject'][0]['value'],
        );
        break;
    }
  }

  // Add "Body" field
  if (!empty($campaign_values['citcon_email_body'])
      || $form_state['citcon_email']['body_opt'] != CITCON_EMAIL_BODY_LOCKED
      || !empty($form_state['citcon_email']['body'][0]['value'])) {
    switch ($form_state['citcon_email']['body_opt']) {
      case CITCON_EMAIL_BODY_LOCKED :
        $form['citcon_email_info']['display_body'] = array(
          '#type' => 'item',
          '#title' => t('Body'),
          '#description' => check_markup($form_state['citcon_email']['body'][0]['value']),
        );
        break;
      case CITCON_EMAIL_BODY_APPEND :
        $form['citcon_email_info']['display_body'] = array(
          '#type' => 'item',
          '#title' => t('Body'),
          '#description' => check_markup($form_state['citcon_email']['body'][0]['value']),
        );
        $form['citcon_email_info']['citcon_body_append'] = array(
          '#type' => 'textarea',
          '#title' => t('Add Your Message'),
          '#default_value' => '',
        );
        break;
      case CITCON_EMAIL_BODY_EDIT :
        $form['citcon_email_info']['citcon_body'] = array(
          '#type' => 'textarea',
          '#title' => t('Body'),
          '#default_value' => $form_state['citcon_email']['body'][0]['value'],
          '#rows' => 10,
          '#required' => TRUE,
        );
        // If we're here, but not submitting... we're probabbly here for ajaxy reasons and should
        if (isset($form_state['citcon_email_target']['rebuild_display'])) {
          $form['citcon_email_info']['citcon_body']['#value'] = $form_state['citcon_email']['body'][0]['value'];
        }
        break;
    }
  }

  // Add prefix and suffix for AJAX to find, locate and replace parts of the form.
  $form['citcon_additional_info']['#prefix'] = '<div id="citcon-additional-info">';
  $form['citcon_additional_info']['#suffix'] = '</div>';

  // Now we let the plugins alter the form
  $target_defaults = citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node);

  // Go through all the saved plugin values
  if (isset($target_defaults['citcon_autotarget_plugin']) && !empty($target_defaults['citcon_autotarget_plugin'])) {

    // Get the plugin that was chosen.
    $plugin = citcon_email_get_target_plugin($target_defaults['citcon_autotarget_plugin']);

    // Assume requirements are not met.
    $has_req = FALSE;
    if ($function = ctools_plugin_get_function($plugin, 'requirements met')) {
      // Check if they are.
      $has_req = $function($target_defaults[$target_defaults['citcon_autotarget_plugin']]);
    }

    // If the plugin requirements haven't been met, display an error message.
    if (empty($has_req)) {
      drupal_set_message('Plugin not configured properly... please check the settings for the plugin.', 'error');
    }
    // Then let the plugin alter the form
    else if ($function = ctools_plugin_get_function($plugin, 'alter supporter form callback')) {
      // Create a fieldset, and move the Postal Code field there.
      $form['citcon_email_target'] = array(
        '#type' => 'fieldset',
        '#title' => t($campaign_values['citcon_email_atf_title'][0]['value']),
        '#description' => t($campaign_values['citcon_email_atf_desc'][0]['value']),
        '#weight' => -20,
        '#prefix' => '<div id="citcon-email-target">',
        '#suffix' => '</div>',
      );

      $function($form, $form_state, $target_defaults[$target_defaults['citcon_autotarget_plugin']]);
    }
  }
  //dsm($form);

  if (isset($form_state['citcon_email_target']['rebuild_display'])) {
    unset($form_state['citcon_email_target']['rebuild_display']);
  }
}

/**
 * Ajax callback for all plugins
 */
function citcon_email_ajax_callback($form, $form_state) {
  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = (empty($button) && isset($form_state['triggering_element']['#ajax'])) ? $form_state['triggering_element']['#ajax'] : $button;
  // If the plugin is requesting to be loaded... load it.
  if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_email_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);
    if ($function = ctools_plugin_get_function($plugin, 'ajax email callback')) {
      return $function($form, $form_state);
    }
  }
}

/**
 * Ajax submit for all plugins
 */
function citcon_email_ajax_validate($form, &$form_state) {
  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = isset($form_state['triggering_element']['#ajax']) ? $form_state['triggering_element']['#ajax'] : NULL;

  // If the plugin is requesting to be loaded via button... load it.
  if (isset($button['#citcon_plugin_load']) && $button['#citcon_plugin_load']) {
    $plugin = citcon_email_get_plugin($button['#citcon_plugin_type'], $button['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax email validate')) {
      $function($form, $form_state);
    }
  }
  // If the plugin is requesting to be loaded via a triggering element, like a select list... load it.
  else if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_email_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax email validate')) {
      $function($form, $form_state);
    }
  }
}

/**
 * Ajax submit for all plugins
 */
function citcon_email_ajax_submit($form, &$form_state) {

  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = isset($form_state['triggering_element']['#ajax']) ? $form_state['triggering_element']['#ajax'] : NULL;

  // If the plugin is requesting to be loaded via a button... load it.
  if (isset($button['#citcon_plugin_load']) && $button['#citcon_plugin_load']) {
    $plugin = citcon_email_get_plugin($button['#citcon_plugin_type'], $button['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax email submit')) {
      $function($form, $form_state);
    }
  }
  // If the plugin is requesting to be loaded via a select list... load it.
  else if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_email_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax email submit')) {
      $function($form, $form_state);
    }
  }
}

/* * ******************************
 * CTools hook implementations
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function citcon_email_ctools_plugin_directory($module, $plugin) {
  if ($module == 'citcon_email' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_ctools_plugin_type().
 *
 * ...is this seriously undocumented in the guilde to create plugins?
 *
 */
function citcon_email_ctools_plugin_type() {
  return array(
    'target' => array(
    ),
  );
}

/**
 *
 *****************************************/
