<?php

/**
 * @file
 * Email install modifies the campaign content type to include email settings.
 */

/**
 * Implements hook_install().
 */
function citcon_email_install() {
  // During installation, the t() function is unavailable, so we use get_t()
  // to store the name of the translation function.
  $t = get_t();

  // Create all the fields we are adding to our content type.
  // http://api.drupal.org/api/function/field_create_field/7
  foreach (_citcon_email_installed_fields() as $field) {
    $field['entity_types'] = array('node');
    $field['locked'] = TRUE;
    field_create_field($field);
  }

  // Create all the instances for our fields.
  // http://api.drupal.org/api/function/field_create_instance/7
  foreach (_citcon_email_installed_instances() as $instance) {
    $instance['entity_type'] = 'node';
    $instance['bundle'] = 'citcon_campaign';
    if (!isset($instance['display'])) {
      $instance['display'] = array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'hidden',
        ),
      );
    }
    field_create_instance($instance);
  }
}

/**
 * Implements hook_uninstall().
 */
function citcon_email_uninstall() {
  // Loop over each of the fields defined by this module and delete
  // all instances of the field, their data, and the field itself.
  // http://api.drupal.org/api/function/field_delete_field/7
  foreach (array_keys(_citcon_email_installed_fields()) as $field) {
    field_delete_field($field);
  }

  // Loop over any remaining field instances attached to the node_example
  // content type (such as the body field) and delete them individually.
  // http://api.drupal.org/api/function/field_delete_field/7
  $instances = field_info_instances('node', 'citcon_campaign');
  foreach ($instances as $instance_name => $instance) {
    if (strpos($instance_name, 'citcon_email') === 0) {
      field_delete_instance($instance);
    }
  }

  // Purge all field infromation
  // http://api.drupal.org/api/function/field_purge_batch/7
  field_purge_batch(1000);
}

/**
 * Returns a structured array defining the fields created by the default content type.
 *
 * This is factored into this function so it can be used in both
 * citcon_email_install() and citcon_email_uninstall().
 *
 * @return
 *  An associative array specifying the fields we wish to add to our
 *  new node type.
 */
function _citcon_email_installed_fields() {
  // During installation, the t() function is unavailable, so we use get_t()
  // to store the name of the translation function.
  $t = get_t();

  // Initialize empty fields array
  $fields = array();

  //***********************
  // "Email" page fields
  //***********************
  // "Email Status" fields
  $fields['citcon_email_status'] = array(
    'field_name' => 'citcon_email_status',
    'cardinality' => 1,
    'type' => 'list_integer',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => '',
      'allowed_values' => array(
        CITCON_CHECKBOX_OFF => $t('Do not make this an email campaign'),
        CITCON_CHECKBOX_ON => $t('Make this an email campaign'),
      ),
    ),
  );

  // "Target" fields
  $fields['citcon_email_to'] = array(
    'field_name' => 'citcon_email_to',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Target][To',
    ),
  );
  $fields['citcon_email_cc'] = array(
    'field_name' => 'citcon_email_cc',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Target][Cc',
    ),
  );
  $fields['citcon_email_bcc'] = array(
    'field_name' => 'citcon_email_bcc',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Target][Bcc',
    ),
  );
  $fields['citcon_email_bcc_opt'] = array(
    'field_name' => 'citcon_email_bcc_opt',
    'cardinality' => 1,
    'type' => 'list_integer',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Target][Bcc',
      'allowed_values' => array(
        CITCON_CHECKBOX_OFF => $t('Hide the BCC field on the supporter form'),
        CITCON_CHECKBOX_ON => $t('Show the BCC field on the supporter form'),
      ),
    ),
  );

  // "Content" fields
  $fields['citcon_email_subject_opt'] = array(
    'field_name' => 'citcon_email_subject_opt',
    'cardinality' => 1,
    'type' => 'list_integer',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Content][Subject',
      'allowed_values' => array(
        CITCON_CHECKBOX_OFF => $t('Do not allow the user to edit the Subject field'),
        CITCON_CHECKBOX_ON => $t('Allow the user to edit the Subject field'),
      ),
    ),
  );
  $fields['citcon_email_subject'] = array(
    'field_name' => 'citcon_email_subject',
    'cardinality' => 1,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Content][Subject',
      'citcon supporter tokens' => TRUE,
      'citcon token support' => array(
        '#citcon_supporter_token_support',
        '#citcon_campaign_token_support',
        '#citcon_lookup_token_support',
        '#citcon_no_html_token_support',
      ),
    ),
  );
  $fields['citcon_email_body_opt'] = array(
    'field_name' => 'citcon_email_body_opt',
    'cardinality' => 1,
    'type' => 'list_integer',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Content][Body',
      'allowed_values' => array(
        CITCON_EMAIL_BODY_LOCKED => $t('Do not allow the user to edit the Body field'),
        CITCON_EMAIL_BODY_APPEND => $t('Allow the user to append an optional message to the Body field'),
        CITCON_EMAIL_BODY_EDIT => $t('Allow the user to fully edit the Body field'),
      ),
    ),
  );
  $fields['citcon_email_body'] = array(
    'field_name' => 'citcon_email_body',
    'cardinality' => 1,
    'type' => 'text_long',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Content][Body',
      'citcon supporter tokens' => TRUE,
      'citcon token support' => array(
        '#citcon_supporter_token_support',
        '#citcon_campaign_token_support',
        '#citcon_lookup_token_support',
        '#citcon_no_html_token_support',
      ),
    ),
  );
  $fields['citcon_email_signature'] = array(
    'field_name' => 'citcon_email_signature',
    'cardinality' => 1,
    'type' => 'text_long',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Content][Signature',
      'citcon supporter tokens' => TRUE,
      'citcon token support' => array(
        '#citcon_supporter_token_support',
        '#citcon_campaign_token_support',
        '#citcon_lookup_token_support',
        '#citcon_no_html_token_support',
      ),
    ),
  );

  // "Options" fields
  $fields['citcon_email_bcc_me_field'] = array(
    'field_name' => 'citcon_email_bcc_me_field',
    'cardinality' => 1,
    'type' => 'list_text',
    'settings' => array(
      'citcon shared setting' => TRUE,
      'citcon admin group' => 'fields',
      'citcon fieldsets' => 'Field Mapping',
      'allowed_values_function' => 'citcon_get_checkbox_field_options',
    ),
  );
  $fields['citcon_email_limiter'] = array(
    'field_name' => 'citcon_email_limiter',
    'cardinality' => 1,
    'type' => 'number_integer',
    'settings' => array(
      'citcon shared setting' => TRUE,
      'citcon admin group' => 'email',
      'citcon fieldsets' => 'Options',
    ),
  );

  //***********************
  // "Fields" page fields
  //***********************
  // Fieldset options
  $fields['citcon_email_atf_title'] = array(
    'field_name' => 'citcon_email_atf_title',
    'cardinality' => 1,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'fields',
      'citcon fieldsets' => 'Form Groups][Auto Target Fieldset',
    ),
  );
  $fields['citcon_email_atf_desc'] = array(
    'field_name' => 'citcon_email_atf_desc',
    'cardinality' => 1,
    'type' => 'text_long',
    'settings' => array(
      'citcon admin group' => 'fields',
      'citcon fieldsets' => 'Form Groups][Auto Target Fieldset',
      'citcon token support' => array(
        '#citcon_campaign_token_support',
      ),
    ),
  );
  $fields['citcon_email_ef_title'] = array(
    'field_name' => 'citcon_email_ef_title',
    'cardinality' => 1,
    'type' => 'text',
    'settings' => array(
      'citcon admin group' => 'fields',
      'citcon fieldsets' => 'Form Groups][Email Fieldset',
    ),
  );
  $fields['citcon_email_ef_desc'] = array(
    'field_name' => 'citcon_email_ef_desc',
    'cardinality' => 1,
    'type' => 'text_long',
    'settings' => array(
      'citcon admin group' => 'fields',
      'citcon fieldsets' => 'Form Groups][Email Fieldset',
      'citcon token support' => array(
        '#citcon_campaign_token_support',
      ),
    ),
  );

  // "Plugin" fields... these will always be hidden.
  $fields['citcon_email_plugin_target'] = array(
    'field_name' => 'citcon_email_plugin_target',
    'cardinality' => 1,
    'type' => 'text_long',
    'settings' => array(
      'citcon admin group' => 'email',
      'citcon fieldsets' => '',
      'citcon hide field' => TRUE,
    ),
  );

  return $fields;
}

/**
 * Returns a structured array defining the instances for this content type.
 *
 * This is factored into this function so it can be used in both
 * node_example_install() and node_example_uninstall().
 *
 * @return
 *  An associative array specifying the instances we wish to add to our new
 *  node type.
 */
function _citcon_email_installed_instances() {
  $t = get_t();

  // Initialize empty instances array
  $insts = array();


  //***********************
  // "Email" page instances
  //***********************
  // "Email Status" instances
  $insts['citcon_email_status'] = array(
    'field_name' => 'citcon_email_status',
    'label' => $t('Email Status'),
    'widget' => array(
      'type' => 'options_onoff',
    ),
    'default_value' => array(
      array(
        'value' => CITCON_CHECKBOX_OFF,
      ),
    ),
  );

  // "Target" instances
  $insts['citcon_email_to'] = array(
    'field_name' => 'citcon_email_to',
    'label' => $t('Email To'),
    'description' => $t("Add email addresses to the To field.<br />
NOTE: Add one email address per line, like this:<br />
<em>John Baker &lt;john@example.com&gt;</em><br />
or<br />
<em>jane@example.com</em>"),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );
  $insts['citcon_email_cc'] = array(
    'field_name' => 'citcon_email_cc',
    'label' => $t('Email Cc'),
    'description' => $t("Add email addresses to the Cc field."),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );
  $insts['citcon_email_bcc'] = array(
    'field_name' => 'citcon_email_bcc',
    'label' => $t('Email Bcc'),
    'description' => $t("Add email addresses to the Bcc field."),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );
  $insts['citcon_email_bcc_opt'] = array(
    'field_name' => 'citcon_email_bcc_opt',
    'label' => $t('Show Bcc Option'),
    'widget' => array(
      'type' => 'options_onoff',
    ),
    'default_value' => array(
      array(
        'value' => CITCON_CHECKBOX_OFF,
      ),
    ),
  );

  // "Content" instances
  $insts['citcon_email_subject_opt'] = array(
    'field_name' => 'citcon_email_subject_opt',
    'label' => $t('Edit Subject Option'),
    'widget' => array(
      'type' => 'options_onoff',
    ),
    'default_value' => array(
      array(
        'value' => CITCON_CHECKBOX_OFF,
      ),
    ),
  );
  $insts['citcon_email_subject'] = array(
    'field_name' => 'citcon_email_subject',
    'label' => $t('Subject Text'),
    'description' => $t('The email Subject field.'),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );
  $insts['citcon_email_body_opt'] = array(
    'field_name' => 'citcon_email_body_opt',
    'label' => $t('Edit Body Option'),
    'widget' => array(
      'type' => 'options_buttons',
    ),
    'default_value' => array(
      array(
        'value' => CITCON_EMAIL_BODY_LOCKED,
      ),
    ),
    'required' => TRUE,
  );
  $insts['citcon_email_body'] = array(
    'field_name' => 'citcon_email_body',
    'label' => $t('Body Text'),
    'description' => $t('The email Body field.'),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  );
  $insts['citcon_email_signature'] = array(
    'field_name' => 'citcon_email_signature',
    'label' => $t('Signature Text'),
    'description' => $t('This signature will be displayed at the very end of the email.<br />
You can customize the email signature using any of the replacement values listed below.'),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  );

  // "Options" instances
  $insts['citcon_email_bcc_me_field'] = array(
    'field_name' => 'citcon_email_bcc_me_field',
    'label' => $t('"Send me a copy" Field'),
    'description' => $t('Which field should we use to check for "Send myself a copy of this email".'),
    'widget' => array(
      'type' => 'options_select',
    ),
  );
  $insts['citcon_email_limiter'] = array(
    'field_name' => 'citcon_email_limiter',
    'label' => $t('Email Frequency Limiter'),
    'description' => $t('Allow 1 user (as identified by a unique email address) to send 1 email per X number of days.<br />
This is to prevent enthusiastic supporters of your campaign from sending 100 emails a day.'),
    'widget' => array(
      'type' => 'number',
    ),
    'default_value' => array(
      array(
        'value' => 5,
      ),
    ),
  );

  $insts['citcon_email_atf_title'] = array(
    'field_name' => 'citcon_email_atf_title',
    'label' => $t('Auto Target Group Title'),
    'description' => $t('If you enable an Auto Target plugin this will be the title for the Auto Target group.'),
    'widget' => array(
      'type' => 'text_textfield',
    ),
    'default_value' => array(
      array(
        'value' => 'Find your target',
      ),
    ),
  );
  $insts['citcon_email_atf_desc'] = array(
    'field_name' => 'citcon_email_atf_desc',
    'label' => $t('Auto Target Helper Text'),
    'description' => $t('If you enable an Auto Target plugin this text will be displayed in the Auto Target group.'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  );
  $insts['citcon_email_ef_title'] = array(
    'field_name' => 'citcon_email_ef_title',
    'label' => $t('Email Group Title'),
    'description' => $t('This will be the title for the Email group.'),
    'widget' => array(
      'type' => 'text_textfield',
    ),
    'default_value' => array(
      array(
        'value' => 'Send an Email',
      ),
    ),
  );
  $insts['citcon_email_ef_desc'] = array(
    'field_name' => 'citcon_email_ef_desc',
    'label' => $t('Email Group Helper Text'),
    'description' => $t('This text will be displayed in the Auto Target group. You may want to put something like tips that are displayed to your users while they are filling out the email form.'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  );

  // "Plugin" instances... these will always be hidden.
  $insts['citcon_email_plugin_target'] = array(
    'field_name' => 'citcon_email_plugin_target',
    'label' => $t('Email Targets Plugin'),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  );

  return $insts;
}
