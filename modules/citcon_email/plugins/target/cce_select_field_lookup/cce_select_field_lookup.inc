<?php

/**
 * @file
 * Plugin to define targets based on a drop-down list.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'target',
  'title' => t("Drop-down Lookup"),
  'requirements met' => 'cce_select_field_has_req',
  'description' => t('Define targets based on a drop-down list.'),
  'settings form' => 'cce_select_field_lookup_settings_form',
  'alter supporter form callback' => 'cce_select_field_lookup_supporter_form_alter',
  'ajax email callback' => 'cce_select_field_lookup_callback',
  'ajax email validate' => 'cce_select_field_lookup_validate',
);

/**
 * Returns a structured array defining the fields we need to attach to the supporter.
 *
 * @return
 *  An associative array specifying the fields we wish to add to our
 *  new node type.
 */
function _cce_select_field_lookup_installed_fields() {
  // During installation, the t() function is unavailable, so we use get_t()
  // to store the name of the translation function.
  $t = get_t();

  // Initialize empty fields array
  $fields = array();

  //***********************
  // "Select Field Data" page fields
  //***********************
  // "Emailed" fields
  $fields['citcon_email_field_select_to'] = array(
    'field_name' => 'citcon_email_field_select_to',
    'cardinality' => 1,
    'type' => 'text',
    'settings' => array(
      'citcon hide field' => TRUE,
    ),
  );

  return $fields;
}

/**
 * Returns a structured array defining the instances for this content type.
 *
 * This is factored into this function so it can be used in both
 * node_example_install() and node_example_uninstall().
 *
 * @return
 *  An associative array specifying the instances we wish to add to our new
 *  node type.
 */
function _cce_select_field_lookup_installed_instances() {
  $t = get_t();

  // Initialize empty instances array
  $insts = array();


  //***********************
  // "Select Field Data" page instances
  //***********************
  // "Emailed" instances
  $insts['citcon_email_field_select_to'] = array(
    'field_name' => 'citcon_email_field_select_to',
    'label' => $t('Drop down target'),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );

  return $insts;
}

/**
 * Install hook.
 * This is where we'll add our custom fields to the supporter type.
 */
function cce_select_field_lookup_install_on($supporter_type) {

  // Create all the fields we are adding to our supporter type.
  foreach (_cce_select_field_lookup_installed_fields() as $field) {
    $field['entity_types'] = array('citcon_supporter');
    $field['locked'] = TRUE;
    field_create_field($field);
  }

  // Create all the instances for our fields.
  foreach (_cce_select_field_lookup_installed_instances() as $instance) {
    $instance['entity_type'] = 'citcon_supporter';
    $instance['bundle'] = $supporter_type;
    field_create_instance($instance);
  }
}

/**
 * Call our own submit handler if we need to.
 */
function cce_select_field_lookup_set_supporter_fields(&$form_state, $emailed) {

  // Find out the current fields, and find out if we need to attach fields
  // for the plugin.
  $fields = field_info_instances('citcon_supporter', $form_state['values']['type']);
  $insts_keys = array_keys(_cce_select_field_lookup_installed_instances());

  // If we don't have the fields... install them.
  if (!citcon_array_keys_exists($insts_keys, $fields)) {
    cce_select_field_lookup_install_on($form_state['values']['type']);
  }

  // Set the form_state values for our custom fields.
  $langcode = LANGUAGE_NONE;
  $form_state['values']['citcon_email_field_select_to'][$langcode][0]['value'] = $emailed;
}

/**
 * Here's where we can alter the supporter form
 *
 * @param &$form
 *   Array - The form array, passed by reference so we can edit it.
 *
 * @param &$form_state
 *   Array - The form_state array, passed by reference so we can edit it.
 *
 * @param $values
 *   Array - The saved values for the plugin.
 */
function cce_select_field_lookup_supporter_form_alter(&$form, &$form_state, $values) {


  // Find out the current fields, and find out if we need to remove fields
  // for the plugin.
  $fields = field_info_instances('citcon_supporter', $form['type']['#value']);
  $insts_keys = array_keys(_cce_select_field_lookup_installed_instances());

  // If we have the fields... remove them.
  if (citcon_array_keys_exists($insts_keys, $fields)) {
    // Let's delete our custom fields from the form
    citcon_move_field($form, 'citcon_email_field_select_to');
  }

  // Create a fieldset, and move the Postal Code field there.
  $form['citcon_email_target'] += citcon_move_field($form, $values['drop_down_field']);

  // Add ajaxiness
  $form['citcon_email_target'][$values['drop_down_field']][$form['citcon_email_target'][$values['drop_down_field']]['#language']]['#ajax'] = array(
    'callback' => 'citcon_email_ajax_callback',
    'wrapper' => 'citcon-email-target',
    'method' => 'replace',
    'effect' => 'fade',
    '#citcon_plugin_load' => TRUE,
    '#citcon_plugin_type' => 'target',
    '#citcon_plugin_id' => 'cce_select_field_lookup',
  );

  // Add element validator
  $form['citcon_email_target'][$values['drop_down_field']][$form['citcon_email_target'][$values['drop_down_field']]['#language']]['#element_validate'][] = 'citcon_email_ajax_validate';

  // Add the validation handler to the form's Submit button
  $form['actions']['submit']['#citcon_plugin_load'] = TRUE;
  $form['actions']['submit']['#citcon_plugin_type'] = 'target';
  $form['actions']['submit']['#citcon_plugin_id'] = 'cce_select_field_lookup';
  $form['actions']['submit']['#validate'][] = 'citcon_email_ajax_validate';

  // If we found a result... let's post it.
  if (isset($form_state['citcon_email_target']['result'])) {
    $form['citcon_email_target']['result'] = array(
      '#type' => 'item',
      '#weight' => -20,
      '#title' => t('Result'),
      '#description' => $form_state['citcon_email_target']['result'],
    );
  }
}

/**
 * Callback for both ajax-enabled button.
 *
 * Selects and returns the fieldset with the names in it.
 */
function cce_select_field_lookup_callback($form, $form_state) {
  return $form['citcon_email_target'];
}

/**
 * Validation handler for the "Find" button.
 */
function cce_select_field_lookup_validate($form, &$form_state) {

  // Load the campaign node, get the plugin data, then the postal code value.
  $campaign_node = node_load($form_state['values']['nid']);
  $plugin_data = citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node);
  $result_msg = $plugin_data['result_msg'];
  $plugin_data = $plugin_data['cce_select_field_lookup'];
  $supporter = (object) $form_state['values'];
  $select_val = field_get_items('citcon_supporter', $supporter, $plugin_data['drop_down_field']);
  $select_val = $select_val[0]['value'];

  $target = NULL;
  if (!empty($plugin_data['select_fields'][$plugin_data['drop_down_field'] . '_options'][$select_val])) {
    $target = $plugin_data['select_fields'][$plugin_data['drop_down_field'] . '_options'][$select_val];
  }
  elseif (!empty($plugin_data['select_fields']['default'])) {
    $target = $plugin_data['select_fields']['default'];
  }

  // If we have a target... let's get to business.
  if (!empty($target)) {
    // Set the visible result field
    //$result_msg = citcon_get_campaign_info($campaign_node, '');
    $form_state['citcon_email_target']['result'] = t($result_msg, array('@target' => $target));

    // Set the data fields so we can save some of it with the supporter
    cce_select_field_lookup_set_supporter_fields($form_state, $target);

    // then the data getting sent.
    if (!in_array($target, $form_state['citcon_email'])) {
      $form_state['citcon_email']['to'][] = $target;
    }
  }
  // If we don't have a target... log the error... most likely this is due to not setting a default target.
  else {
    $form_state['citcon_email_target']['result'] = t("Sorry, something went wrong and we couldn't manage to find your target. We've made note of this incident and are looking into it.");

    watchdog('CitCon', "Drop down lookup couldn't find a target. Probably due to not having setup a default target.");
  }

  // If haven't really submitted the form, lets rebuild it.
  if (!$form_state['submitted']) {
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Settings form, which will attach to "Email" section.
 */
function cce_select_field_lookup_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  // Only get select lists for options
  $field_options = array(NULL => ' - none - ') + citcon_get_supporter_field_options($node, 'options_select');

  $form = array();
  $form['info'] = array(
    '#type' => 'item',
    '#description' => t('This will enable lookup based on a drop-down list functionality.'),
  );

  // Lookup field
  $form['drop_down_field'] = array(
    '#type' => 'select',
    '#options' => $field_options,
    '#title' => t('Drop down field'),
    '#description' => t('Select the drop down field from the available supporter type fields.'),
    '#default_value' => isset($defaults['drop_down_field']) ? $defaults['drop_down_field'] : NULL,
    '#ajax' => array(
      'callback' => 'cce_select_field_settings_callback',
      'wrapper' => 'citcon-email-select-fields',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  // Setup fieldset for ajax
  $form['select_fields'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="citcon-email-select-fields">',
    '#suffix' => '</div>',
  );
  // Find the drop down field, based on saved settings or form state
  $drop_field = NULL;
  // Is it from ajax?
  if (isset($form_state['values']['citcon_plugin_targets']['cce_select_field_lookup']['drop_down_field'])) {
    $drop_field = $form_state['values']['citcon_plugin_targets']['cce_select_field_lookup']['drop_down_field'];
  }
  // Is it from the saved setting?
  elseif (isset($defaults['drop_down_field'])) {
    $drop_field = $defaults['drop_down_field'];
  }
  // Do we have one at all?
  if (!empty($drop_field)) {
    $form['select_fields']['#description'] = t('');
    // Add the "Default" target field
    $form['select_fields']['default'] = array(
      '#type' => 'textfield',
      '#title' => t('Default target'),
      '#description' => t('This target will be used for any fields that are left blank.'),
      '#default_value' => isset($defaults['select_fields']['default']) ? $defaults['select_fields']['default'] : '',
    );
    $form['select_fields']['helper'] = array(
      '#markup' => t('<hr />For each drop down option, feel free to add an email target, as you would add an email to a TO field.'),
    );

    // Get the allowed values
    $field_info = field_info_field($drop_field);
    // Create the fields
    foreach ($field_info['settings']['allowed_values'] as $key => $label) {
      $form['select_fields'][$drop_field . '_options'][$key] = array(
        '#type' => 'textfield',
        '#title' => $label,
        '#default_value' => isset($defaults['select_fields'][$drop_field . '_options'][$key]) ? $defaults['select_fields'][$drop_field . '_options'][$key] : '',
      );
    }
  }

  return $form;
}

/**
 * Ajax callback for the settings page.
 */
function cce_select_field_settings_callback($form, $form_state) {
  return $form['target']['citcon_plugin_targets']['cce_select_field_lookup']['select_fields'];
}

/**
 * Bool function to determine if all the requirements are met and it's ready
 * to go.
 *
 * @param $values
 *   Array - Plugin values to check from.
 *
 * @return TRUE or FALSE.
 */
function cce_select_field_has_req($values) {
  // Need to add actual checks for this.
  return TRUE;
}
