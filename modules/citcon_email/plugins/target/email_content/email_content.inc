<?php

/**
 * @file
 * Plugin to allow custom email content depending on the target selected.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'target',
  'title' => t("Email Content Depends on Target"),
  'requirements met' => 'email_content_has_req',
  'description' => t('Define targets/subject/body based on a drop-down list.'),
  'settings form' => 'email_content_settings_form',
  'alter supporter form callback' => 'email_content_supporter_form_alter',
  'ajax email callback' => 'email_content_callback',
  'ajax email validate' => 'email_content_validate',
);

/**
 * Returns a structured array defining the fields we need to attach to the supporter.
 *
 * @return
 *  An associative array specifying the fields we wish to add to our
 *  new node type.
 */
//function _email_content_installed_fields() {
//  // During installation, the t() function is unavailable, so we use get_t()
//  // to store the name of the translation function.
//  $t = get_t();
//
//  // Initialize empty fields array
//  $fields = array();
//
//  //***********************
//  // "Select Field Data" page fields
//  //***********************
//  // "Emailed" fields
//  $fields['citcon_email_field_select_to'] = array(
//    'field_name' => 'citcon_email_field_select_to',
//    'cardinality' => 1,
//    'type' => 'text',
//    'settings' => array(
//      'citcon hide field' => TRUE,
//    ),
//  );
//
//  return $fields;
//}

/**
 * Returns a structured array defining the instances for this content type.
 *
 * This is factored into this function so it can be used in both
 * node_example_install() and node_example_uninstall().
 *
 * @return
 *  An associative array specifying the instances we wish to add to our new
 *  node type.
 */
//function _email_content_installed_instances() {
//  $t = get_t();
//
//  // Initialize empty instances array
//  $insts = array();
//
//
//  //***********************
//  // "Select Field Data" page instances
//  //***********************
//  // "Emailed" instances
//  $insts['citcon_email_field_select_to'] = array(
//    'field_name' => 'citcon_email_field_select_to',
//    'label' => $t('Drop down target'),
//    'widget' => array(
//      'type' => 'text_textfield',
//    ),
//  );
//
//  return $insts;
//}

/**
 * Install hook.
 * This is where we'll add our custom fields to the supporter type.
 */
//function email_content_install_on($supporter_type) {
//
//  // Create all the fields we are adding to our supporter type.
//  foreach (_email_content_installed_fields() as $field) {
//    $field['entity_types'] = array('citcon_supporter');
//    $field['locked'] = TRUE;
//    field_create_field($field);
//  }
//
//  // Create all the instances for our fields.
//  foreach (_email_content_installed_instances() as $instance) {
//    $instance['entity_type'] = 'citcon_supporter';
//    $instance['bundle'] = $supporter_type;
//    field_create_instance($instance);
//  }
//}

/**
 * Call our own submit handler if we need to.
 */
//function email_content_set_supporter_fields(&$form_state, $emailed) {
//
//  // Find out the current fields, and find out if we need to attach fields
//  // for the plugin.
//  $fields = field_info_instances('citcon_supporter', $form_state['values']['type']);
//  $insts_keys = array_keys(_email_content_installed_instances());
//
//  // If we don't have the fields... install them.
//  if (!citcon_array_keys_exists($insts_keys, $fields)) {
//    email_content_install_on($form_state['values']['type']);
//  }
//
//  // Set the form_state values for our custom fields.
//  $langcode = LANGUAGE_NONE;
//  $form_state['values']['citcon_email_field_select_to'][$langcode][0]['value'] = $emailed;
//
//}

/**
 * Here's where we can alter the supporter form
 *
 * @param &$form
 *   Array - The form array, passed by reference so we can edit it.
 *
 * @param &$form_state
 *   Array - The form_state array, passed by reference so we can edit it.
 *
 * @param $values
 *   Array - The saved values for the plugin.
 */
function email_content_supporter_form_alter(&$form, &$form_state, $values) {


  // Create a fieldset, and move the Select field there.
  $form['citcon_email_info'] += citcon_move_field($form, $values['drop_down_field']);

  // Find out the current fields, and find out if we need to remove fields
  // for the plugin.
  $fields = field_info_instances('citcon_supporter', $form['type']['#value']);
  $drop_down_field = $values['drop_down_field'];
  $langcode = $form['citcon_email_info'][$drop_down_field]['#language'];

  // Remove "Find Target" fieldset
  unset($form['citcon_email_target']);

  // Add ajaxiness
  $form['citcon_email_info'][$drop_down_field][$langcode]['#ajax'] = array(
    'callback' => 'citcon_email_ajax_callback',
    'wrapper' => 'citcon-email-info',
    'method' => 'replace',
    'effect' => 'fade',
    '#citcon_plugin_load' => TRUE,
    '#citcon_plugin_type' => 'target',
    '#citcon_plugin_id' => 'email_content',
  );

  // Add element validator
  $form['citcon_email_info'][$drop_down_field][$langcode]['#element_validate'][] = 'citcon_email_ajax_validate';

  // Change weight, to float to top.
  $form['citcon_email_info'][$drop_down_field]['#weight'] = -10;

  // Add the validation handler to the form's Submit button
  $form['actions']['submit']['#citcon_plugin_load'] = TRUE;
  $form['actions']['submit']['#citcon_plugin_type'] = 'target';
  $form['actions']['submit']['#citcon_plugin_id'] = 'email_content';
  $form['actions']['submit']['#validate'][] = 'citcon_email_ajax_validate';
  $form['actions']['submit']['#validate'][] = 'citcon_supporter_form_validate';

  // We need to set defaults... either via a default value, OR url $_GET var.
  static $GET_target;
  if (!isset($GET_target) && !isset($form_state['triggering_element'])) {
    if (isset($_GET['target'])) {
      // Setup default target based on $_GET['target'] url value.
      // Get the value and store it in a static var so that we never come back here
      $GET_target = $_GET['target'];
    }
    elseif (isset($form['citcon_email_info']['field_corp_target']['und']['#default_value'][0])
        && !empty($form['citcon_email_info']['field_corp_target']['und']['#default_value'][0])) {
      // Get the value from the default value.
      $GET_target = $form['citcon_email_info']['field_corp_target']['und']['#default_value'][0];
    }

    // Get the value and store it in a static var so that we never come back here
    //$GET_target = $_GET['target'];
    // This next bit of code is here to "trick" drupal into thinking we've
    // submitted via #ajax
    // =============
    // Set the limit_validation_errors array to only validate the $drop_down_field
    $form['actions']['submit']['#limit_validation_errors'] = array(
      array($drop_down_field),
    );
    // Set #submit, otherwise #limit_validation_errors won't work.
    $form['actions']['submit']['#submit'][] = 'email_content_validate';
    // Set a button in the buttons array so drupal will populate "triggering_element"
    $form_state['buttons'][] = $form['actions']['submit'] + array(
      '#ajax' => array(
        '#citcon_plugin_load' => TRUE,
        '#citcon_plugin_type' => 'target',
        '#citcon_plugin_id' => 'email_content',
      ),
    );
    // First time we submit this... not much happens, except for some drupal form processing
    // ...$form_state['values'] gets set, for example.
    drupal_build_form('citcon_supporter_form', $form_state);
    // Now we need to set the $drop_down_field equal to the $Get_target.
    $form_state['values'][$drop_down_field][$langcode][0] = $GET_target;
    // and set the input array equal to the values array.
    $form_state['input'] = $form_state['values'];
    // Unset this, otherwise form.inc 2020 goes to true and it thinks we're
    // trying to submit when we're not actually.
    unset($form_state['input']['submit']);
    // Submit again, this time with the form setup with all the right values,
    // ...which will rebuild the form, validating our $drop_down_field :)
    // ...which will set the proper form values and display them.
    drupal_build_form('citcon_supporter_form', $form_state);
  }
}

/**
 * Callback for both ajax-enabled button.
 *
 * Selects and returns the fieldset with the names in it.
 */
function email_content_callback($form, $form_state) {
  return $form['citcon_email_info'];
}

/**
 * Validation handler for the "Find" button.
 */
function email_content_validate($form, &$form_state) {
  // Load the campaign node, get the plugin data, then the postal code value.
  $campaign_node = node_load($form_state['values']['nid']);
  $plugin_data = citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node);
  $result_msg = $plugin_data['result_msg'];
  $plugin_data = $plugin_data['email_content'];
  $supporter = (object) $form_state['values'];
  $select_val = field_get_items('citcon_supporter', $supporter, $plugin_data['drop_down_field']);
  $select_val = $select_val[0]['value'];

  $target = array();
  // Get breakup values
  foreach (array('target', 'subject', 'body') as $var) {
    if (!empty($plugin_data['select_fields'][$plugin_data['drop_down_field'] . '_options'][$select_val][$var])) {
      $target[$var] = $plugin_data['select_fields'][$plugin_data['drop_down_field'] . '_options'][$select_val][$var];
    }
    elseif (!empty($plugin_data['select_fields']['default'][$var])) {
      $target[$var] = $plugin_data['select_fields']['default'][$var];
    }
  }

  // If we have a target... let's get to business.
  if (!empty($target)) {
    // Set the result variables
    $form_state['citcon_email_info']['breakup'] = $target;
    $form_state['citcon_email_target']['rebuild_display'] = TRUE;
    $form_state['citcon_email']['to'] = array($target['target']);
    $form_state['citcon_email']['subject'][0]['value'] = $target['subject'];
    $form_state['citcon_email']['subject'][0]['safe_value'] = $target['subject'];
    $form_state['citcon_email']['body'][0]['value'] = $target['body'];
    $form_state['citcon_email']['body'][0]['safe_value'] = $target['body'];
  }
  // If we don't have a target... log the error... most likely this is due to not setting a default target.
  else {
    $form_state['citcon_email_info']['error'] = t("Sorry, something went wrong and we couldn't manage to find your target. We've made note of this incident and are looking into it.");

    watchdog('CitCon', "Email Content lookup couldn't find a target. Probably due to not having setup a default target.");
  }

  // If we haven't really submitted the form, lets rebuild it.
  if (!$form_state['submitted']) {
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Settings form, which will attach to "Email" section.
 */
function email_content_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  // Only get select lists for options
  $field_options = array(NULL => ' - none - ') + citcon_get_supporter_field_options($node, 'options_select');

  $form = array();
  $form['info'] = array(
    '#type' => 'item',
    '#description' => t('This will enable lookup based on a drop-down list functionality.'),
  );

  // Lookup field
  $form['drop_down_field'] = array(
    '#type' => 'select',
    '#options' => $field_options,
    '#title' => t('Drop down field'),
    '#description' => t('Select the drop down field from the available supporter type fields.'),
    '#default_value' => isset($defaults['drop_down_field']) ? $defaults['drop_down_field'] : NULL,
    '#ajax' => array(
      'callback' => 'cce_select_field_settings_callback',
      'wrapper' => 'citcon-email-select-fields',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  // Setup fieldset for ajax
  $form['select_fields'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="citcon-email-select-fields">',
    '#suffix' => '</div>',
  );
  // Find the drop down field, based on saved settings or form state
  $drop_field = NULL;
  // Is it from ajax?
  if (isset($form_state['values']['citcon_plugin_targets']['email_content']['drop_down_field'])) {
    $drop_field = $form_state['values']['citcon_plugin_targets']['email_content']['drop_down_field'];
  }
  // Is it from the saved setting?
  elseif (isset($defaults['drop_down_field'])) {
    $drop_field = $defaults['drop_down_field'];
  }
  // Do we have one at all?
  if (!empty($drop_field)) {
    $form['select_fields']['#description'] = t('');
    // Add the "Default" target field
    $form['select_fields']['default'] = array(
      '#type' => 'fieldset',
      '#title' => t('Defaults'),
      '#description' => t('These values will be used for any fields that are left blank.'),
    );
    $form['select_fields']['default']['target'] = array(
      '#type' => 'textfield',
      '#title' => t('"To" Target'),
      '#description' => t('This target will be used for any fields that are left blank.'),
      '#default_value' => isset($defaults['select_fields']['default']['target']) ? $defaults['select_fields']['default']['target'] : '',
    );
    $form['select_fields']['default']['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#description' => t('This subject will be used for any fields that are left blank.'),
      '#default_value' => isset($defaults['select_fields']['default']['subject']) ? $defaults['select_fields']['default']['subject'] : '',
    );
    $form['select_fields']['default']['body'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#description' => t('This body will be used for any fields that are left blank.'),
      '#default_value' => isset($defaults['select_fields']['default']['body']) ? $defaults['select_fields']['default']['body'] : '',
    );
    $form['select_fields']['helper'] = array(
      '#markup' => t('<hr />For each drop down option, feel free to add an email target, as you would add an email to a TO field.'),
    );

    // Get the allowed values
    $field_info = field_info_field($drop_field);
    // Create the fields
    foreach ($field_info['settings']['allowed_values'] as $key => $label) {
      $form['select_fields'][$drop_field . '_options'][$key] = array(
        '#type' => 'fieldset',
        '#title' => $label,
      );
      $form['select_fields'][$drop_field . '_options'][$key]['url'] = array(
        '#type' => 'item',
        '#title' => t('URL'),
        '#description' => t('@node_path?target=@key', array('@node_path' => url('node/' . $form_state['node']->nid, array('absolute' => TRUE)), '@key' => urlencode($key))),
      );
      $form['select_fields'][$drop_field . '_options'][$key]['target'] = array(
        '#type' => 'textfield',
        '#title' => t('@field - Target', array('@field' => $label)),
        '#default_value' => isset($defaults['select_fields'][$drop_field . '_options'][$key]['target']) ? $defaults['select_fields'][$drop_field . '_options'][$key]['target'] : '',
      );
      $form['select_fields'][$drop_field . '_options'][$key]['subject'] = array(
        '#type' => 'textfield',
        '#title' => t('@field - Subject', array('@field' => $label)),
        '#default_value' => isset($defaults['select_fields'][$drop_field . '_options'][$key]['subject']) ? $defaults['select_fields'][$drop_field . '_options'][$key]['subject'] : '',
      );
      $form['select_fields'][$drop_field . '_options'][$key]['body'] = array(
        '#type' => 'textarea',
        '#title' => t('@field - Body', array('@field' => $label)),
        '#default_value' => isset($defaults['select_fields'][$drop_field . '_options'][$key]['body']) ? $defaults['select_fields'][$drop_field . '_options'][$key]['body'] : '',
      );
    }
  }

  return $form;
}

/**
 * Ajax callback for the settings page.
 */
function email_content_settings_callback($form, $form_state) {
  return $form['target']['citcon_plugin_targets']['email_content']['select_fields'];
}

/**
 * Bool function to determine if all the requirements are met and it's ready
 * to go.
 *
 * @param $values
 *   Array - Plugin values to check from.
 *
 * @return TRUE or FALSE.
 */
function email_content_has_req($values) {
  // Need to add actual checks for this.
  return TRUE;
}
