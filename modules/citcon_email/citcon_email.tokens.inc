<?php

/**
 * @file
 * Citizens Connected email tokens.
 */

/**
 * Implements hook_token_info().
 */
function citcon_email_token_info() {

  // Init token info array
  $info = array();

  /*   * *******************
   * "Dynamic" tokens
   * ******************* */

  // Share tokens, added to campaign tokens later.
  // These are all plugins, so we first define the type,
  // then the plugins add the tokens.
  if ($plugins = citcon_email_get_target_plugins()) {

    // Get token_info for plugins
    foreach ($plugins as $plugin) {
      if ($function = ctools_plugin_get_function($plugin, 'lookup token_info')) {
        // Setup the token type.
        $info['types']['cce-lookup-' . $plugin['name']] = array(
          'name' => t('Loookup Tokens'),
          'description' => t('Loookup Tokens for the ' . $plugin['title'] . ' plugin.'),
          'needs-data' => 'cce-result',
        );
        // Create blank array... ready to accept token info.
        $info['tokens']['cce-lookup-' . $plugin['name']] = array();

        // Create the fieldset, and attach form elements
        $info['tokens']['cce-lookup-' . $plugin['name']] += $function();
      }
    }
  }

  return $info;
}

/**
 * Implements hook_tokens()
 */
function citcon_email_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Lookup tokens
  // We'll only go into it if we've got plugins for it.
  if (strpos($type, 'cce-lookup-') === 0
      && $plugins = citcon_email_get_target_plugins()) {

    // For each token, go through each plugin and send the data.
    foreach ($tokens as $name => $original) {

      // Get result data
      $result_data = $data['cce-result'];

      // Get token_replace for plugins
      foreach ($plugins as $plugin) {
        if ($function = ctools_plugin_get_function($plugin, 'lookup token_replace')) {
          if ($replacement = $function($name, $result_data)) {
            // Set the replacement value.
            $replacements[$original] = $replacement;
          }
        }
      }
    }
  }

  return $replacements;
}
