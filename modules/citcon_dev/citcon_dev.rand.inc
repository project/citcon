<?php

/**
 * @file
 * Functions to generate randomness.
 */

/**
 * Generate a mt_random email address
 */
function citcon_dev_rand_email() {
  // Get the different parts
  $alpha = 'abcdefghijklmnopqrstuvwxyz';

  $len = mt_rand(4, 14);
  $user = '';
  for ($i = 0; $i < $len; $i++) {
    $user .= $alpha[mt_rand(0, strlen($alpha) - 1)];
  }

  $len = mt_rand(4, 14);
  $domain = '';
  for ($i = 0; $i < $len; $i++) {
    $domain .= $alpha[mt_rand(0, strlen($alpha) - 1)];
  }
  $exts = array('com', 'net', 'ca', 'org');
  $ext = $exts[array_rand($exts)];

  // build it and return it.
  return $user . '@' . $domain . '.' . $ext;
}

/**
 * Generate a mt_random name
 */
function citcon_dev_rand_name() {
  $names = array(
    'Alexis',
    'Ashley',
    'Beverly',
    'Evelyn',
    'Hilary',
    'Jocelyn',
    'Kelly',
    'Meredith',
    'Shirley',
    'Shannon',
    'Sharon,',
    'Vivian',
    'Addison',
    'Alexis',
    'Avery',
    'Ashley',
    'Taylor',
    'Evelyn',
    'Riley',
    'Aubrey',
    'Peyton',
    'Lauren',
    'Sydney',
    'Morgan',
    'Jocelyn',
    'Mackenzie',
    'Bailey',
    'Payton',
    'Jacob',
    'Heffley',
    'Divina',
    'Armbruster',
    'Brandy',
    'Althoff',
    'Alverson',
    'Fernanda',
    'Port',
    'Dori',
    'Berkowitz',
    'Clarine',
    'Hansell',
    'Julienne',
    'Vanepps',
    'Porsche',
    'Tse',
    'Camilla',
    'Ocegueda',
    'Karon',
    'Fallon',
    'Randy',
    'Yohn',
    'Ardell',
    'Ellisor',
    'Charlyn',
    'Burkhart',
    'Vergie',
    'Abram',
    'Rashida',
    'Senters',
    'Lennie',
    'Cousineau',
    'Inocencia',
    'Heckard',
    'Hedwig',
    'Paul',
    'Fletcher',
    'Woodbury',
    'Mel',
    'Rodriques',
    'Sheldon',
    'Pence',
    'Herbert',
    'Szeto',
    'Shawnna',
    'Biffle',
    'Sarah',
    'Boone',
    'Contessa',
    'Mcnelly',
    'Lara',
    'Deblois',
    'Loree',
    'Whaley',
    'Rosendo',
    'Leblanc',
    'Effie',
    'March',
    'Reuben',
    'Bryand',
    'Lakeisha',
    'Perreira',
    'Lashonda',
    'Gemmell',
    'Gavin',
    'Brim',
    'Clarence',
    'Mccreery',
    'Regena',
    'Isaacs',
    'Jodie',
    'Engelman',
    'Terry',
    'Maddy',
    'Britni',
    'Kinder',
    'Brittny',
    'Munford',
    'Flo',
    'Zuber',
    'Santana',
    'Fischbach',
    'Layla',
    'Jervis',
    'Jacinta',
    'Mulholland',
    'Quintin',
    'Berenbaum',
    'Merlene',
    'Cavazos',
    'Cherry',
    'Harling',
    'Fatima',
    'Vallee',
    'Danny',
    'Skeete',
    'Aurelio',
    'Maston',
  );

  // Pick a mt_random one.
  return $names[array_rand($names)];
}

/**
 * Generate some mt_random text.
 *
 * @param $settings
 *   Array - Keyed array containing settings for the random text.
 *
 * @return Random text.
 */
function citcon_dev_rand_text_field($settings) {
  $max_length = isset($settings['max_length']) ? $settings['max_length'] : 32;

  // Get the different parts
  $alpha = 'abcdefghijklmnopqrstuvwxyz';

  $len = mt_rand(4, 32);
  $text = '';
  for ($i = 0; $i < $len; $i++) {
    $text .= $alpha[mt_rand(0, strlen($alpha) - 1)];
  }
  $text[0] = strtoupper($text[0]);

  return substr($text, 0, $max_length);
}

/**
 * Generate a mt_random number.
 *
 * @param $settings
 *   Array - Keyed array containing settings for the random text. Although
 *   this doesn't seem to be active right now :(
 *
 * @return Random number between 0 and 100.
 */
function citcon_dev_rand_num_field($settings) {
  return mt_rand(0, 100);
}
