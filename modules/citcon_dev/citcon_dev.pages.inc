<?php

/**
 * @file
 * Pages used by the dev module.
 */

/**
 * Page callback for dev tools
 */
function citcon_dev_page() {
  $output = array();

  // Add the "Generate Supporters" form in a fieldset.
  $handle = t('Generate Supporters');
  $supporters_form = drupal_get_form('citcon_dev_generate_supporters_form', NULL, TRUE);
  $content = drupal_render($supporters_form);
  $output['csv_gen_sup_form_group'] = array(
    '#prefix' => '<div class="citcon-form-group">',
    '#markup' => theme('ctools_collapsible', array('handle' => $handle, 'content' => $content, /* 'collapsed' => TRUE */)),
    '#suffix' => '</div>',
  );

  return $output;
}
