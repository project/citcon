<?php

/**
 * @file
 * Citizens Connected development module. For devs only!
 * :P
 */

module_load_include('inc', 'citcon_dev', 'citcon_dev.rand');

/**
 * Implements hook_menu()
 */
function citcon_dev_menu() {
  $items = array();

  // If CitCon Admin pages exist, let's add to that.
  if (module_exists('citcon_admin')) {
    $items['admin/citcon/dev'] = array(
      'title' => t('CitCon Dev Tools'),
      'type' => MENU_LOCAL_TASK,
      'page callback' => 'citcon_dev_page',
      'access arguments' => array('access citcon dev tools'),
      'file' => 'citcon_dev.pages.inc',
      'weight' => 50,
    );
  }
  // Or create a new admin page
  else {
    $items['admin/citcon-dev'] = array(
      'title' => t('CitCon Dev Tools'),
      'page callback' => 'citcon_dev_page',
      'access arguments' => array('access citcon dev tools'),
      'file' => 'citcon_dev.pages.inc',
      'weight' => 50,
    );
  }

  // Add the petition path
  $items['node/%node/import-supporters'] = array(
    'title' => t('Import Supporters'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('citcon_dev_import_supporters_form', 1),
    'access arguments' => array('access citcon dev tools'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );

  return $items;
}


/**
 * Implements hook_permission().
 */
function citcon_dev_permission() {
  return array(
    'access citcon dev tools' => array(
      'title' => t('Access Citizens Connected Development Tools'),
    ),
  );
}

/**
 * Import supporters tool
 */
function citcon_dev_import_supporters_form($form, &$form_state, $node) {
  // Start with a file.
  $form['helper'] = array(
    '#type' => 'item',
    '#title' => t('Start with a CSV file.'),
    '#description' => t('Please choose a file, or upload one.'),
  );
  $form['csv'] = array(
    '#type' => 'file',
    //'#title' => t('Choose a CSV file'),
  );

  $dir = 'private://citcon_dev_import/csv';
  $files = array();
  if ($handle = @opendir($dir)) {
    while (FALSE !== ($file = readdir($handle))) {
      // Avoid . & .. files
      if ($file == '.' || $file == '..') {
        continue;
      }

      // Only bother with csv files
      $ext = strtolower(end(explode('.', $file)));
      if ($ext == 'csv') {
        $path = $dir ."/". $file;
        $files[] = array(
          'dir' => $dir,
          'filename' => $file,
          'ext' => $ext,
          'path' => $path,
          'size' => filesize($path),
          'time' => filectime($path),
        );
      }
    }
  }

  if (!empty($files)) {
    // Table Header
    $header = array(
      t('Filename'),
      t('Date'),
      t('Age'),
      t('Size'),
    );

    // Rows
    $rows = array();
    foreach ($files as $file) {
      $row = array();
      $row[] = check_plain($file['filename']);
      $row[] = format_date($file['time'], 'short');
      $row[] = format_interval(time() - $file['time'], 1);
      $row[] = format_size($file['size']);
      $rows[] = $row;
    }

    $form['csv_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }

  // campaign mapping?
  if (isset($form_state['citcon_dev_import']['campaigns'])) {
    // Set the form array tree
    $form['campaign_mapping'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Campaign Mapping'),
      '#description' => t('Map the incoming campaign to the existing campaign.'),
    );

    // Get all the campaigns
    $nids = citcon_get_all_campaigns();
    $options = array();
    foreach ($nids as $nid) {
      $node = node_load($nid);
      $options[$nid] = $node->title;
    }

    // For each of the incoming campaigns... create a mapping field
    foreach ($form_state['citcon_dev_import']['campaigns'] as $i => $campaign) {
      $form['campaign_mapping'][$campaign] = array(
        '#type' => 'select',
        '#prefix' => $campaign.' = ',
        '#options' => $options,
      );

      // If we've picked a campaign to map to... disable the ability to select another campaign.
      if (isset($form_state['citcon_dev_import']['fields'])) {
        $form['campaign_mapping'][$campaign]['#disabled'] = TRUE;
        $form['campaign_mapping'][$campaign]['#value'] = $form_state['values']['campaign_mapping'][$campaign];
      }
    }
  }

  // Let do some field mapping
  if (isset($form_state['citcon_dev_import']['fields'])) {
    // Set the form array tree
    $form['field_mapping'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Field Mapping'),
      '#description' => t('Map the incoming fields to the existing campaign fields.'),
    );

    // Create the field options.
    $options = array(
      NULL => t('None'),
      'created' => t('Created'),
    );
    foreach ($form_state['citcon_dev_import']['fields'] as $key => $field) {
      $options[$key] = $field['label'].' - '.$key;
    }

    // For each of the incoming campaigns... create a mapping field
    foreach ($form_state['citcon_dev_import']['keys'] as $key) {
      $form['field_mapping'][$key] = array(
        '#type' => 'select',
        '#prefix' => $key.' = ',
        '#options' => $options,
      );
    }
  }


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#name' => 'Next',
  );

  return $form;
}

/**
 * Validate handler
 */
function citcon_dev_import_supporters_form_validate($form, &$form_state) {
  // Increase the execution time
  //drupal_set_time_limit(120);

  // Step 1: parse the data and get basic mapping started
  if (!isset($form_state['citcon_dev_import']) || empty($form_state['citcon_dev_import'])) {
    if(!$file=file_save_upload('csv') ) {
      form_set_error('csv', t('CSV file unavailable!') );
    }
    //citcon_dev_import_step1($form_state);
  }
  // Step 2: Now that we have campaigns mapped... lets start to map fields
  else if (isset($form_state['values']['campaign_mapping']) && !isset($form_state['citcon_dev_import']['fields'])) {
    citcon_dev_import_step2($form_state);
  }
  // Step 3: Import!
  else {
    citcon_dev_import_step3($form_state);
  }
}

/**
 * Submit handler
 */
function citcon_dev_import_supporters_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Step 1 in importing supporters
 */
function citcon_dev_import_step1(&$form_state) {


dsm($form_state);
return;
    if( isset($form_values['font']) && user_access('date_pic manager') ) {
        $dir = drupal_get_path('module', 'date_pic') . '/fonts';

        if( file_check_directory( $dir ) ) {
            $file = file_save_upload('font', $dir, true);
            variable_set('date_pic_font', $dir . '/' . $file);
        } else {
            drupal_set_message( t('WARNING: Server DIR is not accessible. Consult with site admin!', 'status') );
        }
    }




return;


  // Get the CSV as an array of lines.
  $lines = explode("\n", $form_state['values']['csv_text']);
  // Get the first line, as it's the key.
  $keys = array_shift($lines);
  // Get each col
  $keys = substr($keys, 1);
  $keys = substr($keys, 0, strlen($keys) - 2);
  $form_state['citcon_dev_import']['keys'] = $cols = explode('","', $keys);

  // Create an index array that we can use later
  // We do this so it won't matter in which order the columns are in the csv.
  $indexes = array();
  foreach($cols as $i => $col) {
    // Remove quotes, spaces, and capitals.
    $col = str_replace('"', '', $col);
    $col = str_replace(' ', '_', $col);
    $col = strtolower($col);

    // Then attach the key and position in the indexes array
    $indexes[$col] = $i;
  }

  // Find all the unique campaigns in the list.
  $campaigns = array();
  // And while we're at it, create the data array
  $supporter_data = array();
  foreach ($lines as $line) {
    // Get the columns
    if (!empty($line)) {
      $line = substr($line, 1);
      $line = substr($line, 0, strlen($line) - 2);
      $cols = explode('","', $line);

      // Add the data to our supporter data array.
      $supporter_data[] = $cols;

      // If we don't already have the campaign in our array... add it.
      if (!in_array($cols[$indexes['campaign_title']], $campaigns)) {
        $campaigns[] = $cols[$indexes['campaign_title']];
      }
    }
  }

  // Attach it to the form_state so we can do stuff with it later.
  $form_state['citcon_dev_import']['campaigns'] = $campaigns;
  $form_state['citcon_dev_import']['indexes'] = $indexes;

  // The supporter data could be huge... so, lets store it in the DB cache.
  $cid = substr(md5(mt_rand()), 0, 5).'-supporter_import';
  cache_set($cid, $supporter_data);
  $form_state['citcon_dev_import']['csv_data_cid'] = $cid;

get_mem_use();
}

/**
 * Step 2 in importing supporters - Getting the field mapping
 */
function citcon_dev_import_step2(&$form_state) {
get_mem_use();
  $fields = array();

  // Get all the different fields we use for the campaigns
  foreach($form_state['values']['campaign_mapping'] as $nid) {
    // Get the campaign and it's info
    $node = node_load($nid);
    $campaign_info = citcon_get_campaign_info($node);

    // Find the supporter type
    $supporter_type = $campaign_info['citcon_fields_supporter_type'][0]['value'];
    // Then the field instances for that type.
    $supporter_fields = field_info_instances('citcon_supporter', $supporter_type);

    // Then merge it to the fields array
    $fields = array_merge($fields, $supporter_fields);
  }

  // Then attach it to the form_state so we can use it later.
  $form_state['citcon_dev_import']['fields'] = $fields;
}

/**
 * Step 3 in importing supporters
 */
function citcon_dev_import_step3(&$form_state) {
  // Then attach it to the form_state so we can use it later.
  $field_mappings = $form_state['values']['field_mapping'];
  $records = $form_state['citcon_dev_import']['csv_data'];
  $indexes = $form_state['citcon_dev_import']['indexes'];

  foreach($records as $record) {
    // Find which campaign this belongs to.
    $campaign = $record[$indexes['campaign_title']];
    $nid = $form_state['values']['campaign_mapping'][$campaign];
    // and load the values
    $node = node_load($nid);
    $campaign_values = citcon_get_campaign_info($node);

    // Create a blank supporter
    $supporter = array(
      'is_new' => TRUE,
      'sid' => NULL,
      'nid' => $nid,
      'type' => $campaign_values['citcon_fields_supporter_type'][0]['value'],
    );

    // Then we apply each of the fields that were mapped
    foreach ($form_state['values']['field_mapping'] as $key => $field_id) {
      // if it's not mapped... don't worry about it.
      if (empty($field_id)) {
        continue;
      }

      // Find the "machine name" of the key
      $key = str_replace('"', '', $key);
      $key = str_replace(' ', '_', $key);
      $key = strtolower($key);

      // Handle "special" fields
      if (strpos($key, 'email') !== FALSE) {
        $supporter[$field_id]['und'][0]['email'] = $record[$indexes[$key]];
      }
      else if (strpos($key, 'created') !== FALSE) {
        $supporter['created'] = strtotime($record[$indexes[$key]]);
      }
      else {
        $supporter[$field_id]['und'][0]['value'] = $record[$indexes[$key]];
      }
    }

    // Create the supporter
    $supporter = entity_create('citcon_supporter', $supporter);
    citcon_supporter_save($supporter);
  }

  drupal_set_message('Supporters imported');
}

/**
 * Generate supporters form
 */
function citcon_dev_generate_supporters_form($form, &$form_state) {

  // Add the "Number of supporters" option
  $form['num_of_entries'] = array(
    '#type' => 'select',
    '#title' => t('Number of supporter entires to generate'),
    '#options' => array(
      5 => 5,
      10 => 10,
      50 => 50,
      100 => 100,
      500 => 500,
    ),
    '#default_value' => isset($form_state['values']['num_of_entries']) ? $form_state['values']['num_of_entries'] : 5,
  );
  $form['per_of_repeaters'] = array(
    '#type' => 'select',
    '#title' => t('Percentage of entries that are repeat supporters'),
    '#options' => array(
      0 => '0%',
      '.05' => '5%',
      '.10' => '10%',
      '.25' => '25%',
      '.50' => '50%',
      '.75' => '75%',
      '.95' => '95%',
    ),
    '#default_value' => isset($form_state['values']['per_of_repeaters']) ? $form_state['values']['per_of_repeaters'] : '.25',
  );
  $form['style_of_repeaters'] = array(
    '#type' => 'radios',
    '#title' => t('How are repeat supporters choosen?'),
    '#options' => array(
      'rand' => t('Completely Random'),
      'curve' => t('Mimic a "normal" curve'),
    ),
    '#default_value' => isset($form_state['values']['style_of_repeaters']) ? $form_state['values']['style_of_repeaters'] : 'curve',
  );

  // Add our actions
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['generate'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Supporters'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Submit callback to generate supporters
 */
function citcon_dev_generate_supporters_form_submit($form, &$form_state) {

  // Get all the different campaign nids
  $nids = citcon_get_all_campaigns();

  // Figure out how many supporters and repeats we'll need
  $percent_repeats = $form_state['values']['per_of_repeaters'];
  $total_entries = $form_state['values']['num_of_entries'];
  $total_repeats = (integer)($total_entries * $percent_repeats);
  $total_supporters = $total_entries - $total_repeats;
  $supporters = citcon_dev_generate_supporters($nids, $total_supporters);

  // Save the supporters to the DB.
  foreach ($supporters as $supporter) {
    citcon_supporter_save($supporter);
  }

  // Then add the remaining entries using the existing supporters
  // But lets change the date
  for ($i = 0; $i < $total_repeats; $i++) {
    // Pick a random supporter
    $supporter = $supporters[array_rand($supporters)];

    // Generate a new one
    $new_supporter = citcon_dev_generate_supporter($nids[array_rand($nids)]);

    // Transfer over the email field value
    $new_supporter->{$new_supporter->email_field} = $supporter->{$supporter->email_field};

    // Lazily transfer any data
    foreach ((array)$supporter as $field => $value) {
      if (strpos($field, 'field') === 0) {
        $new_supporter->{$field} = $value;
      }
    }
    citcon_supporter_save($new_supporter);
  }

  drupal_set_message(t('@entries entries have been added. Of those, @uni are unique supporters and @repeat entries were repeats.', array('@entries' => $total_entries, '@uni' => $total_supporters, '@repeat' => $total_repeats)));
}

/**
 * Generate $num number of supporters
 *
 * @param $nids
 *   Array - Node IDs to pick from when generating supporters
 *
 * @param $num
 *   Integer - The number of supporters to generate
 *
 * @return Array of supporter objects
 */
function citcon_dev_generate_supporters($nids, $num) {
  $supporters = array();

  for ($i = 0; $i < $num; $i++) {
    $supporters[] = citcon_dev_generate_supporter($nids[array_rand($nids)]);
  }

  return $supporters;
}

/**
 * Generates a supporter based on the nid of the campaign
 *
 * @param $nid
 *   Integer - Campaign node ID to use when generating supporters
 *
 * @return A single supporter object
 */
function citcon_dev_generate_supporter($nid) {
  // Load the campaign, and get the info fields we'll need
  $node = node_load($nid);
  $supporter_type = citcon_get_campaign_info($node, 'citcon_fields_supporter_type');
  $supporter_type = isset($supporter_type[0]['value']) ? $supporter_type[0]['value'] : NULL;
  $email_field = citcon_get_supporter_email_field($node);

  // Initialize a blank supporter values array
  $supporter_values = array(
    'is_new' => TRUE,
    'sid' => NULL,
    'created' => time() - rand(10, (60*60*24*7*52)),
    'type' => $supporter_type,
    'nid' => $nid,
    'email_field' => $email_field,
  );

  // If the given nid has no supporter type, do nothing.
  if (empty($supporter_type)) {
    return FALSE;
  }

  // Get all the field instances.
  $instances = field_info_instances('citcon_supporter', $supporter_type);

  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);

    // Check if it's the email_field, then generate an email.
    if (!empty($email_field) && $email_field == $instance['field_name']) {
      $value = array('email' => citcon_dev_rand_email());
    }
    // Check if it's a name text field, then add a name.
    else if (strpos($instance['field_name'], 'name') !== FALSE && $instance['widget']['module'] == 'text') {
      $value = array('value' => citcon_dev_rand_name());
    }
    // Some number field? Add a number.
    else if ($instance['widget']['module'] == 'number') {
      $value = array('value' => citcon_dev_rand_num_field($field['settings']));
    }
    // Some text field? Add some text.
    else if ($instance['widget']['module'] == 'text') {
      $value = array('value' => citcon_dev_rand_text_field($field['settings']));
    }
    // List? Pick one.
    else if ($instance['widget']['module'] == 'options') {
      $field = field_info_field($instance['field_name']);
      $values = $field['settings']['allowed_values'];
      $value = array('value' => array_rand($values));
    }

    $supporter_values[$instance['field_name']]['und'][0] = $value;
  }

  $supporter = entity_create('citcon_supporter', $supporter_values);

  return $supporter;
}
