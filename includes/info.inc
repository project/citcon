<?php

/**
 * @file
 * Information/statistics grabbing functions.
 */

/**
 * Get the total number of unique supporters, either for a specific campaign or all of them.
 *
 * Also, get results specific to a particular campaign, or the group that it belongs to.
 *
 * @param $type
 *   String - The type to search for, possible values include:
 *    - all - All entries.
 *    - unique - Unique entries.
 *    - repeat - Repeat entries. (all - unique)
 *    - repeat-unique - Unique repeat entries.
 *
 * @param $nid
 *   Integer - Optional - Node ID of the campaign results being viewed.
 *   Or if NULL, will return results for all campaigns.
 *
 * @return Integer count of total supporters.
 */
function citcon_get_total_supporters($type = 'all', $nid = NULL) {

  // Find all supporters
  $query = db_select('citcon_supporter_stats', 'css');

  // Are we looking for a particular campaign? Add conditions
  if (is_numeric($nid)) {
    $query->join('citcon_supporter', 'cs', 'css.sid = cs.sid');
    $query->condition('cs.nid', $nid, '=');
  }

  switch ($type) {
    case 'all' :
      return $query->countQuery()->execute()->fetchField();
      break;
    case 'unique' :
      $query->groupBy('email');
      return $query->countQuery()->execute()->fetchField();
      break;
    case 'repeat' :
      $all = citcon_get_total_supporters('all');
      $unique = citcon_get_total_supporters('unique');
      return $all - $unique;
      break;
    case 'repeat-unique' :
      // @todo: optimize this so it's not counting PHP objects
      $query->groupBy('email');
      $query->addExpression('COUNT(*)', 'total');
      $query->having('total >= 2');
      $result = $query->execute();
      $records = array();
      foreach ($result as $record) {
        $records[] = $record;
      }
      return count($records);
      break;
  }
}

/**
 * Get the total number of unique supporters, either for a specific campaign or all of them.
 *
 * Also, get results specific to a particular campaign, or the group that it belongs to.
 *
 * @param $limit
 *   Integer - Number of results to return.
 *
 * @param $nid
 *   Integer - Optional - Node ID of a particular campaign.
 *   Or if NULL, will return results for all campaigns.
 *
 * @return Array of keyed arrays. Each element containing a bunch of info.
 * For more details, check out the $records array that gets returned below.
 */
function citcon_get_top_supporters($limit = 5, $nid = NULL) {
  // Find all supporters
  $query = db_select('citcon_supporter_stats', 'css');

  // Are we looking for a particular campaign? Add conditions
  if (is_numeric($nid)) {
    $query->join('citcon_supporter', 'cs', 'css.sid = cs.sid');
    $query->condition('cs.nid', $nid, '=');
  }

  // Group them and add conditions
  $query->fields('css', array('sid', 'email'));
  $query->groupBy('email');
  $query->addExpression('COUNT(*)', 'total');
  $query->having('total >= 2');

  // Set the order and limit
  $query->orderBy('total', 'DESC');
  $query->range(0, $limit);

  $results = $query->execute();
  $records = array();
  foreach ($results as $record) {
    // Load all the supporter records based on email
    $supporter = citcon_supporter_load($record->email);
    // Get all the different campaigns
    $nids = array();
    foreach ($supporter['sids'] as $entry) {
      if (!in_array($entry->nid, $nids)) {
        $nids[] = $entry->nid;
      }
    }

    // Setup the display name after getting supporter values.
    $info = citcon_get_supporter_info($record->email);
    $display_name = t('@first @last <@email>', array(
      '@first' => isset($info['field_citcon_first_name']) ? check_plain($info['field_citcon_first_name'][0]['value']) : '',
      '@last' => isset($info['field_citcon_last_name']) ? check_plain($info['field_citcon_last_name'][0]['value']) : '',
      '@email' => check_plain($record->email),
    ));

    // Setup the record with easy to access data.
    $records[] = array(
      'display_name' => $display_name,
      'email' => check_plain($record->email),
      'data' => $supporter['sids'],
      'first' => array_shift($supporter['sids']),
      'last' => array_pop($supporter['sids']),
      'campaigns' => entity_load('node', $nids),
      'combined_values' => $supporter['combined_values'],
    );
  }
  return $records;
}

/**
 * Get all supporter entires ordered by date... DESC
 *
 * @param $nid
 *   Integer - Optional default NULL - Node ID of a particular campaign.
 *   Or if NULL, will return results for all campaigns.
 *
 * @param $pager
 *   Integer - Optional default FALSE - Number of records to dislpay per page.
 *   If a number greater than 0 is provided, pager functionality will be enabled.
 *
 * @param $element
 *   Integer - Optional default 0 - The element ID for the pager.
 *
 * @param $sortable
 *   Bool - Optional default FALSE - If true, tableSort functionality will be added,
 *   sortable by date OR email...
 *
 * @return An array of supporter IDs, sorted by date.
 */
function citcon_get_all_supporters($nid = NULL, $pager = FALSE, $element = 0, $sortable = FALSE) {
  $supporters = &drupal_static(__FUNCTION__);

  // if we're looking for all the supporters and it's not set... Get them!
  if (empty($nid)) {
    if (!isset($supporters['all'])) {
      $query = new EntityFieldQuery();

      // Find all supporters
      $result = $query->entityCondition('entity_type', 'citcon_supporter')
          ->propertyOrderBy('created', 'DESC')
          ->execute();

      $supporters['all'] = !empty($result) ? array_keys($result['citcon_supporter']) : array();
    }

    return $supporters['all'];
  }
  // if we're looking for supporters of a particular campaign
  // and it's not set... Get them!
  elseif (is_numeric($nid)) {
    if (!isset($supporters[$nid]) || !empty($pager)) {
      $query = new EntityFieldQuery();

      // Find all supporters
      $query->entityCondition('entity_type', 'citcon_supporter')
          ->propertyCondition('nid', $nid)
          ->propertyOrderBy('created', 'DESC');

      // Add pager if we need to.
      if (is_numeric($pager) && $pager > 0) {
        $query->pager($pager, $element);
      }

      $result = $query->execute();
      $supporters[$nid] = !empty($result) ? array_keys($result['citcon_supporter']) : array();
    }

    return $supporters[$nid];
  }

  return array();
}

/**
 * Count supporters for a particular campaign.  This function is
 * optimized with Drupal caching, as it's usually run on every
 * campaign landing page to show how many people have signed so
 * far.   Entries will be kept in the Drupal cache for at least
 * CITCON_SUPPORTER_COUNT_CACHE_TIME seconds before being
 * queried again.
 *
 * @param $nid
 *   Integer - Required - Node ID of a particular campaign.
 *
 * @param $tnid
 *  	 Integer - Optional - Translated Node ID (tnid) from campaign
 *  				(or 0 to not count translations)
 *
 * @return The count of supporters for this campaign,
 * 	 or 0 if there are no supporters found.
 */

define('CITCON_SUPPORTER_COUNT_CACHE_TIME',5);  // 5 second cache of count values

function citcon_get_campaign_supporters_count($nid, $tnid = 0) {
  $supporters_count_static_cache = &drupal_static(__FUNCTION__);
  // First look in this function's Drupal Static cache.  Maybe
  // we already calculated this campaign's supporters on this
  // page load!
  if (!isset($supporters_count_static_cache[$nid])) {
    // If not in the static cache, try and fetch the count for this nid
    // from the Drupal cache, if a valid entry exists there (ie. it was
    // calculated within the last 5 seconds):
    $result = NULL;
    if ($cached = cache_get('citcon_supporter_count_campaign_' . $nid)) {
      if ($cached->expire >= REQUEST_TIME) {
        $result = $cached->data;
      }
    }

    // If no cached copy available in the Drupal cache, fetch it from the Database
    // with a count query, then save to the cache.
    if (!$result) {
      // We haven't found it anywhere in the cache or it's expired,
      // so actually query the MySQL database and count supporters:

      // If no $tnid was supplied, don't worry about translations.
      // If a tnid argument was present, then translations are
      // turned on and we need to gather all possible translation nids
      if ($tnid) {
        // find all nids to count.  This is any campaign
        // with a tnid that matches the supplied tnid.

        // for parent campaigns, this returns them and all their children.
        // For child translations, this returns them, their siblings, and their parent
        $parent_query = new EntityFieldQuery();
        $parent_query->entityCondition('entity_type', 'node')
          ->propertyCondition('type', 'citcon_campaign')
          ->propertyCondition('tnid', $tnid);
        $result = $parent_query->execute();
        $nids = array();
        foreach ($result['node'] as $node) {
          $nids[] = $node->nid;
        }
      } else {
        // just count supporters to this campaign's nid:
        $nids = array($nid);
      }

      // Now count all citcon_supporter records with those campaign nids
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'citcon_supporter')
        ->propertyCondition('nid', $nids, 'IN');
      $result = $query->count()->execute();
      cache_set('citcon_supporter_count_campaign_' . $nid, $result, 'cache', REQUEST_TIME + CITCON_SUPPORTER_COUNT_CACHE_TIME);
    }
    $supporters_count_static_cache[$nid] = $result;
  }
  return $supporters_count_static_cache[$nid];
}

/**
 * Get all campaigns which are translations of other campaigns.
 *
 * @return Array of node IDs.
 */
function citcon_get_all_campaign_translations() {
  $tnids = &drupal_static(__FUNCTION__);

  // First call? Get all the campaigns!
  if (!isset($tnids)) {
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'citcon_campaign')
    ->propertyCondition('tnid', 0, '<>')
    ->execute();
    $nids = !empty($results) ? array_keys($results['node']) : array();

    // Then go through them all and create the array.
    $tnids = array();
    foreach($nids as $nid) {
      $node = node_load($nid);
      if ($node->tnid != $node->nid) {
        $tnids[$node->tnid][] = $node->nid;
      }
    }
  }

  return $tnids;
}

/**
 * Get all campaigns.
 *
 * @return Array of node IDs.
 */
function citcon_get_all_campaigns() {
  $nids = &drupal_static(__FUNCTION__);

  // First call? Get all the campaigns!
  if (!isset($nids)) {
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'node')
        ->propertyCondition('type', 'citcon_campaign')
        ->execute();
    $nids = !empty($results) ? array_keys($results['node']) : array();
  }

  return $nids;
}

/**
 * Get all "Active" campaigns.
 *
 * @return Array of node IDs.
 */
function citcon_get_all_active_campaigns() {
  $nids = &drupal_static(__FUNCTION__);

  // First time? Get all the campaigns then check for "active" status.
  if (!isset($nids)) {
    $all_nids = citcon_get_all_campaigns();
    $nids = array();
    foreach ($all_nids as $nid) {
      $node = node_load($nid);
      $status = citcon_get_campaign_info($node, 'citcon_campaign_status');
      if ($status[0]['value'] == CITCON_CAMPAIGN_ACTIVE) {
        $nids[] = $nid;
      }
    }
  }

  return $nids;
}

/**
 * Get all "Inactive" campaigns.
 *
 * @return Array of node IDs.
 */
function citcon_get_all_inactive_campaigns() {
  $nids = &drupal_static(__FUNCTION__);

  // First time? Get all the campaigns then check for "active" status.
  if (!isset($nids)) {
    $all_nids = citcon_get_all_campaigns();
    $nids = array();
    foreach ($all_nids as $nid) {
      $node = node_load($nid);
      $status = citcon_get_campaign_info($node, 'citcon_campaign_status');
      if ($status[0]['value'] == CITCON_CAMPAIGN_INACTIVE) {
        $nids[] = $nid;
      }
    }
  }

  return $nids;
}

/**
 * Helper function to quickly pull an email addy from an sid.
 *
 * @param $sid
 *   Int - The Supporter ID to lookup
 *
 * @return The email addy or FALSE if none found.
 */
function citcon_get_supporter_email($sid) {
  // Find all supporters
  $result = db_select('citcon_supporter_stats', 'css')
      ->fields('css', array('email'))
      ->condition('sid', $sid, '=')
      ->execute()
      ->fetchField();

  return $result;
}

/**
 * Get all "Test Mode" campaigns.
 *
 * @return Array of node IDs.
 */
function citcon_get_all_test_campaigns() {
  $nids = &drupal_static(__FUNCTION__);

  // First time? Get all the campaigns then check for "active" status.
  if (!isset($nids)) {
    $all_nids = citcon_get_all_campaigns();
    $nids = array();
    foreach ($all_nids as $nid) {
      $node = node_load($nid);
      $status = citcon_get_campaign_info($node, 'citcon_campaign_status');
      if ($status[0]['value'] == CITCON_CAMPAIGN_TEST) {
        $nids[] = $nid;
      }
    }
  }

  return $nids;
}

/**
 * Helper function to pull all the data values of a campaign.
 *
 * @param $campaign
 *   Object - Node object to extract the data from.
 *
 * @param $field
 *   String - The field we're looking for.
 *
 * @return Returns an array of data
 */
function citcon_get_campaign_info($campaign, $field = NULL) {
  $campaign = citcon_campaign_load($campaign);

  if (!empty($field)) {
    // Get the value.
    $result = field_get_items('node', $campaign, $field);
  }
  else {
    $result = array();
    $field_names = array_keys((array) $campaign);
    foreach ($field_names as $field_name) {
      if (strpos($field_name, 'citcon') === 0) {
        $result[$field_name] = citcon_get_campaign_info($campaign, $field_name);
      }
    }
  }

  return $result;
}

/**
 * Helper function to pull aggregated supporter data.
 *
 * This function requires an id (INT OR EMAIL) of a saved supporter to work.
 *
 * @param $supporter
 *   Integer or String or Object- Supporter Object or supporter ID or email
 *   addy of the supporter, from which to load the object to extract the data from.
 *
 * @param $field
 *   String - The field we're looking for.
 *
 * @param $aggr
 *   Bool - If you give a numeric value for the supporter... do you
 *   also want all the aggregated data?
 *
 * @return Returns an array of data.
 *
 * @todo This function is "hard coded" with "und" as the language for the values.
 */
function citcon_get_supporter_info($supporter, $field = NULL, $aggr = FALSE) {

  // TODO: This function is "hard coded" with "und" as the language for the values.
  // This needs to change.
  // If we've gotten an ID... we need the email, so lets get that.
  if (is_numeric($supporter) && $aggr) {
    $supporter = (array) citcon_supporter_load($supporter);
    static $one;

    //if (!isset($one)) {
    //  $one = TRUE;
    //  dsm($supporter);
    //  die;
    //}
  }
  elseif (is_numeric($supporter)) {
    $email = citcon_get_supporter_email($supporter);

    // Using the email, lets load a supporter
    $supporter = citcon_supporter_load($email);
  }
  elseif (is_string($supporter)) {
    $email = $supporter;

    // Using the email, lets load a supporter
    $supporter = citcon_supporter_load($email);
  }

  $result = array();

  // If we want everything from a particular record... give it up!
  if (empty($field) && $aggr) {
    // Foreach field
    foreach ($supporter as $field_name => $value) {
      // Check for a field
      if (strpos($field_name, 'citcon') === 0 || strpos($field_name, 'field') === 0) {
        // check for a value
        if (isset($value['und'])) {
          // Set it.
          $result[$field_name] = $value['und'];
        }
      }
    }
  }
  else if (empty($field) && !empty($supporter['fields'])) {
    // Foreach field
    foreach ($supporter['fields'] as $field_name) {
      // Check for a field
      if (strpos($field_name, 'citcon') === 0 || strpos($field_name, 'field') === 0) {
        // check for a value
        if (isset($supporter['combined_values']->{$field_name}['und'])) {
          // Set it.
          $result[$field_name] = $supporter['combined_values']->{$field_name}['und'];
        }
      }
    }
  }
  else {
    // Check for a specific value.
    if (isset($supporter['combined_values']->{$field}['und'])) {
      // Set it.
      $result = $supporter['combined_values']->{$field}['und'];
    }
  }

  return $result;
}

/**
 * Find a particular field id based on mappings.
 *
 * @param $field
 *   String - The type of field we're looking for.
 *
 * @param $node
 *   Object - Optional - The campaign node we're looking into. Or, we'll
 *   try to load one from the menu.
 *
 * @return A field ID string.
 */
function citcon_get_supporter_field($field, $node = NULL) {
  // Get the node from the menu if we weren't given one.
  if (!isset($node)) {
    $node = menu_get_object();
  }

  // Load the campaign values.
  $campaign_values = citcon_get_campaign_info($node);

  // Let's start by assuming nothing is selected.
  $result = NULL;
  switch ($field) {
    case 'email':
      if (isset($campaign_values['citcon_supporter_email_field'])) {
        $result = $campaign_values['citcon_supporter_email_field'][0]['value'];
      }
      break;
    case 'petition':
      if (isset($campaign_values['citcon_petition_opt_field'])) {
        $result = $campaign_values['citcon_petition_opt_field'][0]['value'];
      }
      break;
    case 'text':
      //$result = $campaign_values['citcon_email_bcc_me_field'][0]['value'];
      break;
    case 'bcc_me':
      if (isset($campaign_values['citcon_email_bcc_me_field'])) {
        $result = $campaign_values['citcon_email_bcc_me_field'][0]['value'];
      }
      break;
  }

  return $result;
}

/**
 * Find the email field id.
 *
 * @param $node
 *   Object - Optional - The campaign node we're looking into. Or, we'll
 *   try to load one from the menu.
 *
 * @return A field ID string.
 */
function citcon_get_supporter_email_field($node = NULL) {
  // Check the ol' campaign settings.
  $email_field = citcon_get_supporter_field('email', $node);

  if (empty($email_field)) {
    // If we got nothing back... we need to find something.
    // Let's find any possible fields
    $fields = citcon_get_supporter_email_field_options($node);

    if (count($fields) == 1) {
      // If there's only one possiblity... then let's use that.
      $field_keys = array_keys($fields);
      $email_field = array_shift($field_keys);
    }
    elseif (count($fields) > 1) {
      // If there's more than one... let's see if we have our default...
      foreach ($fields as $field_id => $field_label) {
        if ($field_id == '') {
          $email_field = $field_id;
        }
      }
    }
  }

  return $email_field;
}

/**
 * Find the supporter fields for a particular campaign.
 *
 * @param $campaign
 *   Integer or Object - Either a node ID or a loaded object.
 *
 * @param $field_types
 *   String or Array - Optional - Limit the results to these field types.
 *
 * @return The supporter fields in an array.
 */
function citcon_get_supporter_field_options($campaign, $field_types = NULL) {
  // Load campaign
  $campaign = citcon_campaign_load($campaign);

  // Create array if none
  if (is_string($field_types)) {
    $field_types = array($field_types);
  }

  $options = array();

  // Find our supporter type
  if ($bundle_name = citcon_get_campaign_info($campaign, 'citcon_fields_supporter_type')) {
    // Find the fields
    $fields = field_info_instances('citcon_supporter', $bundle_name[0]['value']);

    // Create the options array
    foreach ($fields as $field) {
      // If we're looking for a particular field type... filter 'em.
      if (isset($field_types)) {
        if (in_array($field['widget']['type'], $field_types)) {
          $options[$field['field_name']] = t($field['label']);
        }
      }
      // Or don't.
      else {
        $options[$field['field_name']] = t($field['label']);
      }
    }
  }

  return $options;
}

/**
 * Allowed values function for the supporter email field
 *
 * @return An array of allowed values.
 */
function citcon_get_supporter_email_field_options($node = NULL) {
  // Get the node from the menu if we weren't given one.
  if (!isset($node)) {
    $node = menu_get_object();
  }

  if (isset($node->nid)) {
    return citcon_get_supporter_field_options($node, 'email_textfield');
  }
  //else {
  //  return NULL;
  //}
}

/**
 * Allowed values function for the supporter email field
 *
 * @return An array of allowed values.
 */
function citcon_get_checkbox_field_options() {
  // Get the node from the menu if we weren't given one.
  if (!isset($node)) {
    $node = menu_get_object();
  }

  if (isset($node->nid)) {
    return citcon_get_supporter_field_options($node, 'options_onoff');
  }
  //else {
  //  return NULL;
  //}
}

/**
 * Get loaded CitCon supporter objects.
 * Builds an EntityFieldQuery, then loads the entities.
 *
 * @param $options
 *   Array - Array containing keys for filters
 *
 * @param $pager
 *   Integer - Optional default FALSE - Number of records to dislpay per page.
 *   If a number greater than 0 is provided, pager functionality will be enabled.
 *
 * @return An array of supporter objects.
 */
function citcon_get_supporters($options = NULL, $pager = FALSE) {
  // If we're getting a "top" list... then get it
  if (isset($options['top_sups']) && !empty($options['top_sups'])) {
    return citcon_get_top_supporters($options['num_of_sups'], $options['nid']);
  }

  $query = new EntityFieldQuery();
  // Find all supporters
  $query->entityCondition('entity_type', 'citcon_supporter');

  // Limit the number of records to get
  if (isset($options['num_of_sups']) && !empty($options['num_of_sups'])) {
    $query->range(0, $options['num_of_sups']);
  }

  // Limit to a particular campaign
  if (isset($options['nid']) && !empty($options['nid'])) {
    $query->propertyCondition('nid', $options['nid']);
  }
  // Or Filter type
  elseif (isset($options['supporter_type']) && !empty($options['supporter_type']) && empty($options['nid'])) {
    $query->propertyCondition('type', $options['supporter_type']);
  }

  // Filter fields
  if (isset($options['field_filters'])) {
    foreach ($options['field_filters'] as $field_id => $info) {
      $query->fieldCondition($field_id, $info['column'], $info['value']);
    }
  }

  // Filter dates
  if (isset($options['date_filters']['from']) || isset($options['date_filters']['to'])) {
    // Get defaults
    $time = array();
    // From the UNIX epoch
    $time[] = isset($options['date_filters']['from']) ? strtotime($options['date_filters']['from']) : 0;
    // To the end of time... or close to it ;)
    $time[] = isset($options['date_filters']['to']) ? strtotime($options['date_filters']['to']) : 9999999999;

    // Add condition
    $query->propertyCondition('created', $time, 'BETWEEN');
  }

  // Order by date
  $query->propertyOrderBy('created', 'DESC');

  // Add pager if we need to.
  if (is_numeric($pager) && $pager > 0) {
    $query->pager($pager);
  }

  // And return the loaded entities
  $results = $query->execute();
  if (!empty($results['citcon_supporter'])) {
    return entity_load('citcon_supporter', array_keys($results['citcon_supporter']));
  }
  else {
    return array();
  }
}

/**
 * Find all the content types which are CitCon campaign types
 *
 * @return Array of campaign type machine_names
 */
function citcon_get_all_campaign_types() {
  $types = node_type_get_types();

  foreach ($types as $name => $type) {
    if (!citcon_is_campaign_type($name)) {
      unset($types[$name]);
    }
  }

  return $types;
}

/**
 * Returns whether the given content type is a Citizens Connected campaign type.
 *
 * @return
 *   Boolean value.
 */
function citcon_is_campaign_type($type) {

  // We'll use this in the future, when you can enable any content type to be a campaign.
  return variable_get('citcon_is_campaign_type_use_' . $type, 0) == CITCON_CAMPAIGN_TYPE_ENABLED;
}

/**
 * Helper function to determine when was the last submit
 * from a supporter.
 *
 * @param $email
 *   String - Email addy to lookup.
 *
 * @param $nid
 *   Int - Node ID to check against.
 *
 * @return Unix timestamp of last time submitted against this node. Or,
 *   FALSE if there is no previous entry.
 */
function citcon_supporter_last_submit($email, $nid) {
  // Load campaign field.
  $campaign_node = node_load($nid);
  // Get the email field.
  $email_field = citcon_get_supporter_email_field($campaign_node);

  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'citcon_supporter')
    ->propertyCondition('nid', $nid)
    ->fieldCondition($email_field, 'email', $email)
    ->propertyOrderBy('created', 'DESC')
    ->range(0,1)
    ->execute();

  // If we've got no supporter, we've got no reason to continue.
  if (empty($entities['citcon_supporter'])) {
    return FALSE;
  }

  $sids = array_keys($entities['citcon_supporter']);
  $supporter = citcon_supporter_load($sids[0]);

  if (!empty($supporter->created)) {
    return $supporter->created;
  }
  else {
    return FALSE;
  }
}
