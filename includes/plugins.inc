<?php

/**
 * @file
 * Plugin functions and definitions for ctools.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function citcon_ctools_plugin_directory($module, $plugin) {
  if ($module == 'citcon' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_ctools_plugin_type().
 *
 * ...is this seriously undocumented in the guilde to create plugins?
 *
 */
function citcon_ctools_plugin_type() {
  return array(
    'settings' => array(
    ),
    'petition' => array(
    ),
    'sharing' => array(
    ),
    'data_process' => array(
    ),
  );
}

/**
 * Fetch metadata for a particular plugin.
 *
 * @param $type
 *   String - The plug in type.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular target plugin.
 */
function citcon_get_plugin($type, $id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', $type, $id);
}

/**
 * Helper function to load the plugin data
 *
 * @param $type
 *   String - The plugin type to load.
 *
 * @param $node
 *   Object - The campaign node to get the data from.
 *
 * @param $id
 *   String - Optional - The plug in id.
 *
 * @return An array containing the unserialized data.
 */
function citcon_get_plugin_data($type, $node, $id = NULL) {
  $value = citcon_get_campaign_info($node, $type);
  $data = unserialize($value[0]['value']);
  if (empty($id)) {
    return $data;
  }
  else {
    return $data[$id];
  }
}

/**
 * Fetch metadata for a particular share plugin.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular share plugin.
 */
function citcon_get_share_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'sharing', $id);
}

/**
 * Fetch metadata for all share plugins.
 *
 * @return
 *   An array of arrays with information about all available share plugins.
 */
function citcon_get_share_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'sharing');
}

/**
 * Fetch metadata for a particular settings plugin.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular settings plugin.
 */
function citcon_get_settings_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'settings', $id);
}

/**
 * Fetch metadata for all settings plugins.
 *
 * @return
 *   An array of arrays with information about all available settings plugins.
 */
function citcon_get_settings_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'settings');
}

/**
 * Fetch metadata for a particular petition plugin.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular petition plugin.
 */
function citcon_get_petition_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'petition', $id);
}

/**
 * Fetch metadata for all petition plugins.
 *
 * @return
 *   An array of arrays with information about all available petition plugins.
 */
function citcon_get_petition_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'petition');
}

/**
 * Fetch metadata for a particular data_process plugin.
 *
 * @param $id
 *   String - The plug in id.
 *
 * @return
 *   An array with information about the particular data_process plugin.
 */
function citcon_get_data_process_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'data_process', $id);
}

/**
 * Fetch metadata for all data_process plugins.
 *
 * @return
 *   An array of arrays with information about all available data_process plugins.
 */
function citcon_get_data_process_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('citcon', 'data_process');
}

/**
 * Get all the plugins.
 *
 * All of them!
 *
 * @return
 *   An array of arrays with information about all available data_process plugins.
 */
function citcon_get_all_plugins() {
  ctools_include('plugins');

  $settings = citcon_get_settings_plugins();
  $petition = citcon_get_petition_plugins();
  $sharing = citcon_get_share_plugins();
  $data_process = citcon_get_data_process_plugins();
  $target = module_exists('citcon_email') ? citcon_email_get_target_plugins() : array();

  return $settings + $petition + $sharing + $data_process + $target;
}

/**
 * Ajax callback for all plugins
 */
function citcon_ajax_callback($form, $form_state) {
  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = (empty($button) && isset($form_state['triggering_element']['#ajax'])) ? $form_state['triggering_element']['#ajax'] : $button;

  // If the plugin is requesting to be loaded via a button or a triggering element... load it.
  if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax callback')) {
      return $function($form, $form_state);
    }
  }
}

/**
 * Ajax submit for all plugins
 */
function citcon_ajax_validate($form, &$form_state) {
  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = (empty($button) && isset($form_state['triggering_element']['#ajax'])) ? $form_state['triggering_element']['#ajax'] : $button;

  // If the plugin is requesting to be loaded via a button or a triggering element... load it.
  if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax validate')) {
      $function($form, $form_state);
    }
  }
}

/**
 * Ajax submit for all plugins
 */
function citcon_ajax_submit($form, &$form_state) {
  $button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : NULL;
  $trigger = (empty($button) && isset($form_state['triggering_element']['#ajax'])) ? $form_state['triggering_element']['#ajax'] : $button;

  // If the plugin is requesting to be loaded via a button or a triggering element... load it.
  if (isset($trigger['#citcon_plugin_load']) && $trigger['#citcon_plugin_load']) {
    $plugin = citcon_get_plugin($trigger['#citcon_plugin_type'], $trigger['#citcon_plugin_id']);

    if ($function = ctools_plugin_get_function($plugin, 'ajax submit')) {
      $function($form, $form_state);
    }
  }
}

/**
 * Generic form generator that plugins can use... in case a plugin wants to
 * generate a form. An example of this is the "Email share" plugin.
 *
 * @param $form
 *   Array - The form array.
 *
 * @param $form_state
 *   Array - The form_state array.
 *
 * @param $form_info
 *   Array - A keyed array that contains all the info needed to load the form
 *
 * @return $form
 *   Get back a form array
 */
function citcon_plugin_get_form($form, &$form_state, $form_info = NULL) {
  // Init form with form info
  $form = array();
  $form['form_info'] = array(
    '#type' => 'value',
    '#value' => $form_info,
  );

  // load/run the form callback if it exists.
  if ($plugin = citcon_get_plugin($form_info['type'], $form_info['id'])) {
    if (function_exists($form_info['callback'])) {
      $form_info['callback']($form, $form_state, $form_info['node']);
    }
  }

  return $form;
}
