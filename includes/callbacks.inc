<?php

/**
 * @file
 * Callbacks of all kinds. AJAX, hook_menu, FAPI...
 */

/**
 * Entity uri callback.
 */
function citcon_supporter_uri($supporter) {
  return array(
    'path' => 'citcon-supporter/' . $supporter->sid,
  );
}

/**
 * Ajax callback... called it!
 */
function citcon_ajax_destination_callback($form, $form_state) {
  return $form['ccfs_destination']['citcon-ajax-wrapper'];
}

/**
 * Access callback for thank you page.
 *
 * Mostly, we'll need to check the session data.
 *
 * Also... this is located in this file, because this file is included from citcon.mod
 * ...which needs to happen with access callbacks.
 */
function citcon_thankyou_page_access($node) {
  drupal_session_initialize();
  // if the sid session var is set... we should be here.
  if (isset($_SESSION['citcon_sid']) && !empty($_SESSION['citcon_sid'])) {
    return TRUE;
  }
  // if we're new, let's visit the campaign page.
  else {
    drupal_goto('node/' . $node->nid);
    return FALSE;
  }
}

/**
 * Title callback for thank you page
 *
 * @param $node
 *   Object - This is where we'll find the campaign settings... more
 *   importantly, it'll be where we find the thank you page title.
 *
 * @return The thank you page title... duh.
 */
function citcon_thankyou_page_title($node) {
  // Get the title
  $title = citcon_get_campaign_info($node, 'citcon_thank_dest_title');

  // Get our supporter from session
  $supporter = citcon_get_supporter_from_session();

  // Replace tokens
  $title = token_replace($title[0]['value'], array('cc-s' => $supporter, 'cc-c' => $node));

  return $title;
}

/**
 * Title callback for a supporter page
 *
 * @param $supporter
 *   Object - This is where we'll find the campaign settings... more
 *   importantly, it'll be where we find the thank you page title.
 *
 * @return The thank you page title... duh.
 */
function citcon_supporter_page_title($supporter) {
  // Get the full scoop on this supporter
  $full_supporter = citcon_supporter_load($supporter->sid, TRUE);

  return $full_supporter['display_name'];
}

/**
 * Title callback for petition page
 *
 * @param $node
 *   Object - This is where we'll find the campaign settings... more
 *   importantly, it'll be where we find the petition page title.
 *
 * @return The petition page title... duh.
 */
function citcon_petition_page_title($node) {
  // Get the title
  $title = citcon_get_campaign_info($node, 'citcon_pet_dest_title');

  // Replace tokens
  $title = token_replace($title[0]['value'], array('cc-c' => $node));

  return $title;
}

