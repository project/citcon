<?php

/**
 * @file
 * Citizens Connected admin pages/forms.
 */

/**
 * Campaign edit form... different, based on what page we're displaying.
 *
 * @param $form
 *   Array - form array
 *
 * @param &$form_state
 *   Array - form_state array passed by association
 *
 * @param $admin_group
 *   String - The citcon campaign administration group to display
 *
 * @param $node
 *   Object - Loaded node object
 *
 * @return Returns the loaded form array...which is just a modified version
 * of the node edit form.
 */
function citcon_campaign_form($form, &$form_state, $node, $admin_group) {

  // Retrieve the node form
  $form_id = $node->type . '_node_form';
  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_retrieve_form($form_id, $form_state);
  //
  //// Remove the base form id so that node_form_alters don't fire
  //unset($form_state['build_info']['base_form_id']);
  //// Also, this isn't really a node edit form... so we unset this.
  //// Otherwise, some form_alter hooks may check for this.
  //unset($form['#node_edit_form']);
  // Hide standard fields with css
  $form['additional_settings']['#prefix'] = '<div class="citcon-css-hide">';
  $form['additional_settings']['#suffix'] = '</div>';

  // Add our own flags for form altering
  $form['#citcon_node_edit_form'] = TRUE;
  $form['#citcon_node_edit_group'] = empty($admin_group) ? 'basic' : $admin_group;

  // Start modifying the form!
  // Isolate the admin group and remove node form fields
  _citcon_isolate_form_fields($form, $form_state, $form['#citcon_node_edit_group']);

  // Render fieldsets
  _citcon_attach_fieldsets($form, $form_state);

  // Attach our validate & submit handlers
  $form['#validate'][] = 'citcon_campaign_form_validate';
  $form['#submit'][] = 'citcon_campaign_form_submit';

  return $form;
}

/**
 * Validate handler for citcon_campaign_form().
 *
 * @see citcon_campaign_form()
 */
function citcon_campaign_form_validate($form, &$form_state) {
  // Remove node_form_submit from submit handlers...
  if ($form_state['submitted']) {
    foreach ($form_state['submit_handlers'] as $i => $callback) {
      if ($callback == 'node_form_submit') {
        // let's switch it with our sub handler instead.
        unset($form_state['submit_handlers']);
        $form_state['submit_handlers'][$i] = 'citcon_campaign_form_submit';

        // Remove citcon_campaign_form_submit from submit handlers...
        // We just added it tot he form_state submit handlers
        foreach ($form['#submit'] as $i => $callback) {
          if ($callback == 'citcon_campaign_form_submit') {
            unset($form['#submit'][$i]);
          }
        }
      }
    }
  }

  // Run node validate functions
  module_load_include('inc', 'node', 'node.pages');
  node_form_validate($form, $form_state);

  // Serialize values so we can save them.
  if (isset($form_state['values']['citcon_plugin_petition']) && $form['#citcon_node_edit_group'] == 'petition') {
    $field = citcon_move_field($form, 'citcon_plugin_petition');
    $langcode = $field['citcon_plugin_petition']['#language'];
    unset($form_state['values']['citcon_plugin_petition'][$langcode]);
    // Then serialize the data and set it to the field.
    $form_state['values']['citcon_plugin_petition'][$langcode][0]['value'] = serialize($form_state['values']['citcon_plugin_petition']);
  }

  if (isset($form_state['values']['citcon_plugin_settings']) && $form['#citcon_node_edit_group'] == 'settings') {
    $field = citcon_move_field($form, 'citcon_plugin_settings');
    $langcode = $field['citcon_plugin_settings']['#language'];
    unset($form_state['values']['citcon_plugin_settings'][$langcode]);
    // Then serialize the data and set it to the field.
    $form_state['values']['citcon_plugin_settings'][$langcode][0]['value'] = serialize($form_state['values']['citcon_plugin_settings']);
  }

  if (isset($form_state['values']['citcon_plugin_sharing']) && $form['#citcon_node_edit_group'] == 'settings') {
    $field = citcon_move_field($form, 'citcon_plugin_sharing');
    $langcode = $field['citcon_plugin_sharing']['#language'];
    unset($form_state['values']['citcon_plugin_sharing'][$langcode]);
    // Then serialize the data and set it to the field.
    $form_state['values']['citcon_plugin_sharing'][$langcode][0]['value'] = serialize($form_state['values']['citcon_plugin_sharing']);
  }

  // Data processing plugin
  if (isset($form_state['values']['citcon_plugin_data_process']) && $form['#citcon_node_edit_group'] == 'after') {
    $field = citcon_move_field($form, 'citcon_plugin_data_process');
    $langcode = $field['citcon_plugin_data_process']['#language'];
    unset($form_state['values']['citcon_plugin_data_process'][$langcode]);
    // Then serialize the data and set it to the field.
    $form_state['values']['citcon_plugin_data_process'][$langcode][0]['value'] = serialize($form_state['values']['citcon_plugin_data_process']);
  }

  // Check that our thank you from address is a valid address
  if ($form['#citcon_node_edit_group'] == 'after' && (!isset($form_state['values']['citcon_thank_email_from']) || !valid_email_address($form_state['values']['citcon_thank_email_from']))) {
    form_set_error('citcon_thank_email_from', t('Please enter a valid email address for the Thank You - From field.'));
  }

}

/**
 * Submit handler for citcon_campaign_form().
 *
 * It's a copy of node_form_submit... minus the redirect.
 *
 * @see citcon_campaign_form()
 */
function citcon_campaign_form_submit($form, &$form_state) {

  // Flatten array so that locale module can find the fields
  // Also, let's move the old node values over the form_state array.
  foreach ($form_state['citcon']['fields'] as $field_name) {
    $form += citcon_move_field($form, $field_name);
  }

  // Getting a recursion error, so we'll unset this submit callback
  foreach ($form['#submit'] as $i => $callback) {
    if ($callback == __FUNCTION__) {
      unset($form['#submit'][$i]);
    }
  }

  $node = node_form_submit_build_node($form, $form_state);

  $insert = empty($node->nid);
  node_save($node);

  // If there are shared settings
  // And we have a translation
  // Find all translations and spread the settings
  if (isset($node->tnid) && !empty($node->tnid)
      && isset($form_state['citcon']['shared_settings']) && !empty($form_state['citcon']['shared_settings'])
      && module_exists('citcon_locale')) {
    $nids = citcon_locale_get_tnids($node->tnid);
    foreach ($nids as $nid) {
      //if this is the original node, do nothing.
      if ($nid == $node->nid) {
        continue;
      }

      // Move each of the shared settings over
      $tnode = node_load($nid);
      foreach ($form_state['citcon']['shared_settings'] as $shared_key) {
        $tnode->{$shared_key} = $node->{$shared_key};
      }

      node_save($tnode);
    }
  }

  $node_link = l(t('view'), 'node/' . $node->nid);
  $watchdog_args = array(
    '@type' => $node->type,
    '%title' => $node->title,
  );
  $t_args = array(
    '@type' => node_type_get_name($node),
    '%title' => $node->title,
  );

  if ($insert) {
    watchdog('content', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has been created.', $t_args));
  }
  else {
    watchdog('content', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  if ($node->nid) {
    $form_state['values']['nid'] = $node->nid;
    $form_state['nid'] = $node->nid;
    // This is the redirect we comment out.
    //$form_state['redirect'] = 'node/' . $node->nid;
  }
  else {
    // In the unlikely case something went wrong on save, the node will be
    // rebuilt and node form redisplayed the same way as in preview.
    drupal_set_message(t('The post could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }

  // Set the thank you alias if we need to.
  if ($form['#citcon_node_edit_group'] == 'after') {

    // Get the alias value from the node
    $thank_alias = citcon_get_campaign_info($node, 'citcon_thank_dest_path');
    $thank_alias = $thank_alias[0]['value'];

    // We try to load the path... regardless, all we care about is the pid.
    $thank_path = path_load('node/' . $node->nid . '/citcon-thank-you-page');

    if (!empty($thank_alias)) {
      // We set these vars...
      $node_path = (isset($node->path['alias']) && !empty($node->path['alias'])) ? $node->path['alias'] : 'node/' . $node->nid;
      $thank_path['source'] = 'node/' . $node->nid . '/citcon-thank-you-page';
      $thank_path['language'] = $node->path['language'];
      $thank_path['alias'] = $node_path . '/' . $thank_alias;
      // ...but leave the pid untouched so we won't create a duplicate.
      path_save($thank_path);
    }
  }

  // Set the petition alias if we need to.
  if ($form['#citcon_node_edit_group'] == 'petition') {

    // Get the alias value from the node
    $petition_alias = citcon_get_campaign_info($node, 'citcon_pet_dest_path');
    $petition_alias = $petition_alias[0]['value'];

    // We try to load the path... regardless, all we care about is getting a pid.
    $petition_path = path_load('node/' . $node->nid . '/citcon-petition-page');

    if (!empty($petition_alias)) {
      // We set these vars...
      $node_path = (isset($node->path['alias']) && !empty($node->path['alias'])) ? $node->path['alias'] : 'node/' . $node->nid;
      $petition_path['source'] = 'node/' . $node->nid . '/citcon-petition-page';
      $petition_path['language'] = $node->path['language'];
      $petition_path['alias'] = $node_path . '/' . $petition_alias;
      // ...but leave the pid untouched so we won't create a duplicate.
      path_save($petition_path);
    }
  }

  // Save the thank you from email address.
  if (isset($form_state['values']['citcon_thank_email_from'])) {
    variable_set('citcon-node-'.$form_state['values']['nid'].'-thank_you_from', $form_state['values']['citcon_thank_email_from']);
  }

  // Clear the page and block caches.
  cache_clear_all();
}

/**
 * Displays the supporter type admin overview page.
 */
function citcon_overview_supporter_types() {
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => '4'));
  $rows = array();

  $types = citcon_supporter_type_load_all();

  // If there are no types, set a blank array.
  if (!$types) {
    $types = array();
  }

  foreach ($types as $key => $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $row = array(theme('citcon_supporter_admin_overview', array('label' => $type->label, 'type' => $type)));
    // Set the edit column.
    $row[] = array('data' => l(t('edit'), 'admin/structure/citcon_supporter_types/manage/' . $type_url_str));

    // Manage fields.
    $row[] = array('data' => l(t('manage fields'), 'admin/structure/citcon_supporter_types/manage/' . $type_url_str . '/fields'));

    // Display fields.
    $row[] = array('data' => l(t('manage display'), 'admin/structure/citcon_supporter_types/manage/' . $type_url_str . '/display'));

    // Set the delete column.
    if ($type->custom) {
      $row[] = array('data' => l(t('delete'), 'admin/structure/citcon_supporter_types/manage/' . $type_url_str . '/delete'));
    }
    else {
      $row[] = array('data' => '');
    }

    $rows[] = $row;
  }

  $build['supporter_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No supporter types available. <a href="@link">Add supporter content type</a>.', array('@link' => url('admin/structure/citcon_supporter_types/add'))),
  );


  return $build;
}

/**
 * Title callback for a supporter type.
 *
 * @param $supporter_type
 *   (optional) The machine name of the supporter type when
 *   editing an existing supporter type.
 *
 * @return The label of the supporter type.
 */
function citcon_supporter_type_page_title($supporter_type) {
  return $supporter_type->label;
}

/**
 * Form constructor for the supporter type editing form.
 *
 * @param $form
 *   Array - The form array.
 *
 * @param $form_state
 *   Array - The form_state array.
 *
 * @param $supporter_type
 *   String/Object - The machine name of the supporter type when
 *   editing an existing supporter type. Or the supporter type object
 *   depending on what the operation is.
 *
 * @param $op
 *   String - optional - The operation being performed.
 *
 * @return Basic form for editing or creating a supporter type.
 */
function citcon_supporter_type_form($form, &$form_state, $supporter_type, $op = 'edit') {
  if ($op == 'clone') {
    // We're cloning... change a a few things.
    $supporter_type->label .= ' (cloned)';
    $supporter_type->type .= '_clone';
  }
  elseif (!isset($supporter_type->type)) {
    // This is a new type. Lets get default values.
    $supporter_type = citcon_supporter_type_set_defaults();
  }

  // Make the type object available to implementations of hook_form_alter.
  $form['#citcon_supporter_type'] = $supporter_type;

  $form['label'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $supporter_type->label,
    '#description' => t('The name of this supporter type. This text will be displayed on administration pages only. <br /><em>It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. <strong>This name must be unique.</strong></em>'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => $supporter_type->type,
    '#maxlength' => 32,
    '#disabled' => !isset($supporter_type->is_new),
    '#machine_name' => array(
      'exists' => 'citcon_supporter_type_load',
      'source' => array('label'),
    ),
    '#required' => TRUE,
    '#description' => t('A unique machine-readable name for this supporter type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['custom'] = array(
    '#type' => 'value',
    '#value' => $supporter_type->custom,
  );
  $form['hidden'] = array(
    '#type' => 'value',
    '#value' => $supporter_type->hidden,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save supporter type'),
    '#weight' => 40,
  );

  if ($supporter_type->custom) {
    if (!empty($supporter_type->type)) {
      $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete content type'),
        '#weight' => 45,
      );
    }
  }

  return $form;
}

/**
 * Form submission handler for citcon_supporter_type_form().
 *
 * @see citcon_supporter_type_form_validate()
 */
function citcon_supporter_type_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  $type = citcon_supporter_type_set_defaults();

  $type->type = trim($form_state['values']['type']);
  $type->label = trim($form_state['values']['label']);
  $type->custom = $form_state['values']['custom'];
  $type->hidden = $form_state['values']['hidden'];

  if (isset($form['#citcon_supporter_type']->module)) {
    $type->module = $form['#citcon_supporter_type']->module;
  }

  if ($op == t('Delete content type')) {
    $form_state['redirect'] = 'admin/structure/citcon_supporter_types/manage/' . str_replace('_', '-', $type->type) . '/delete';
    return;
  }

  // Saving the content type after saving the variables allows modules to act
  // on those variables via hook_node_type_insert().
  $status = citcon_supporter_type_save($type);

  //node_types_rebuild();
  menu_rebuild();
  $t_args = array('%name' => $type->label);

  if ($status == CITCON_SUPPORTER_TYPE_SAVED_UPDATED) {
    drupal_set_message(t('The supporter type %name has been updated.', $t_args));
  }
  elseif ($status == CITCON_SUPPORTER_TYPE_SAVED_NEW) {
    //node_add_body_field($type);
    drupal_set_message(t('The supporter type %name has been added.', $t_args));
    watchdog('node', 'Added supporter type %name.', $t_args, WATCHDOG_NOTICE, l(t('view'), 'admin/structure/citcon_supporter_types'));
  }

  $form_state['redirect'] = 'admin/structure/citcon_supporter_types';
  return;
}

/**
 * Menu callback; delete a single content type.
 */
function citcon_supporter_type_delete_confirm($form, &$form_state, $type) {
  $form['type'] = array('#type' => 'value', '#value' => $type->type);
  $form['label'] = array('#type' => 'value', '#value' => $type->label);

  $message = t('Are you sure you want to delete the content type %type?', array('%type' => $type->label));
  $caption = '';

  $num_supporters = db_query("SELECT COUNT(*) FROM {citcon_supporter} WHERE type = :type", array(':type' => $type->type))->fetchField();
  if ($num_supporters) {
    $caption .= '<p>' . format_plural($num_supporters, '%type is used by 1 piece of content on your site. If you remove this content type, you will not be able to edit the %type content and it may not display correctly.', '%type is used by @count pieces of content on your site. If you remove %type, you will not be able to edit the %type content and it may not display correctly.', array('%type' => $type->label)) . '</p>';
  }

  $caption .= '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $message, 'admin/structure/citcon_supporter_types', $caption, t('Delete'));
}

/**
 * Process content type delete confirm submissions.
 */
function citcon_supporter_type_delete_confirm_submit($form, &$form_state) {
  citcon_supporter_type_delete($form_state['values']['type']);

  $t_args = array('%name' => $form_state['values']['label']);
  drupal_set_message(t('The content type %name has been deleted.', $t_args));
  watchdog('menu', 'Deleted content type %name.', $t_args, WATCHDOG_NOTICE);

  //node_types_rebuild();
  menu_rebuild();

  $form_state['redirect'] = 'admin/structure/citcon_supporter_types';
  return;
}

/**
 * Results page, like a dashboard for the campaign manager
 */
function citcon_results_page() {
  $output = array();

  // Create the "quick view" group
  $output['quick'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quick View'),
    '#collapsible' => TRUE,
  );

  // Get the data for the quick view table
  $quick_data = citcon_get_quickview_data();
  $output['quick']['table'] = array(
    '#theme' => 'table',
    '#rows' => $quick_data['rows'],
    '#header' => $quick_data['header'],
    '#collapsible' => TRUE,
  );

  if (module_exists('citcon_csv')) {
    // Create the "quick view" group
    $output['csv'] = array(
      '#type' => 'fieldset',
      '#title' => t('CSV Download'),
      '#collapsible' => TRUE,
    );
    $output['csv']['form'] = drupal_get_form('citcon_csv_form');
  }

  return $output;
}

/**
 * Helper function to get data for the quickview table, or for any other use
 * that may arise.
 *
 * @param $nid
 *   Integer - Optional - Node ID of the campaign results being viewed.
 *   Or if NULL, will return results for all campaigns.
 *
 * @return Keyed array, with rows and headers info for easy table display.
 */
function citcon_get_quickview_data($nid = NULL) {

  $data = array();

  // Get base data
  $data['data'] = array(
    'total unique supporters' => citcon_get_total_supporters($nid, TRUE),
    'total grouped supporters' => citcon_get_total_supporters($nid),
      //'all languages' => citcon_get_all_languages($nid),
  );

  // Create header for table
  $data['header'] = array();

  // Organize into rows
  $data['rows'][] = array(
    t('Total supporters from all the campaigns:'),
    $data['data']['total unique supporters'],
  );

  //dsm($data);

  return $data;
}

