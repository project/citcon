<?php

/**
 * @file
 * Main Citizens Connected pages.
 */

/**
 * Menu callback for thank you page
 *
 * @param $node
 *   Object - This is the campaign.... where we'll find the thank
 *   you page content
 *
 * @return Return a renderable array of content.
 */
function citcon_thankyou_page($node) {
  // Add the original page to the breadcrumb trail.
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l($node->title, 'node/' . $node->nid);
  drupal_set_breadcrumb($breadcrumb);

  // Get the supporter
  $supporter = citcon_get_supporter_from_session();

  // And the body text
  $body = citcon_get_campaign_info($node, 'citcon_thank_dest_text');

  // If we didn't get a supporter, don't bother trying to replace the tokens.
  if (empty($supporter)) {
    $body = check_markup($body[0]['value'], $body[0]['format']);
    $body = token_replace($body, array('cc-c' => $node));

    return $body;
  }
  else {
    $body = check_markup($body[0]['value'], $body[0]['format']);
    $body = token_replace($body, array('cc-s' => $supporter, 'cc-c' => $node));

    return $body;
  }
}

/**
 * Menu callback for supporter view page
 *
 * @param $supporter
 *   Object - The supporter.
 *
 * @return Return a renderable array of content.
 */
function citcon_supporter_page_view($supporter) {

  // For markup consistency with other pages, use citcon_supporter_view_multiple() rather than citcon_supporter_view_multiple().
  return citcon_supporter_view_multiple(array($supporter->sid => $supporter), 'full');
}

/**
 * Menu callback for supporter edit page
 *
 * @param $supporter
 *   Object - The supporter.
 *
 * @return Return a renderable array of content.
 */
function citcon_supporter_page_edit($supporter) {
  return drupal_get_form('citcon_supporter_form', $supporter);
}

/**
 * Menu callback -- ask for confirmation of node deletion
 */
function citcon_supporter_delete_confirm($form, &$form_state, $supporter) {
  $form['#citcon_supporter'] = $supporter;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['sid'] = array('#type' => 'value', '#value' => $supporter->sid);
  return confirm_form($form, t('Are you sure you want to delete the supporter?'), 'citcon-supporter/' . $supporter->sid, t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Execute node deletion
 */
function citcon_supporter_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    // Get the full story on this supporter
    $supporter = citcon_supporter_load($form_state['values']['sid']);
    $campaign = node_load($supporter->nid);

    // Remove stat
    db_delete('citcon_supporter_stats')
        ->condition('sid', $supporter->sid, '=')
        ->execute();

    // Remove entity
    entity_delete('citcon_supporter', $form_state['values']['sid']);

    watchdog('citcon_supporter', 'Supporter #@id, who supported <em>@title</em> has been deleted.', array('@id' => $supporter->sid, '@title' => $campaign->title));
    drupal_set_message(t('Supporter #@id has been deleted.', array('@id' => $supporter->sid)));
  }

  $form_state['redirect'] = url('node/' . $campaign->nid);
}

/**
 * Menu callback for petition page
 *
 * @param $node
 *   Object - This is the campaign.... where we'll find the thank
 *   you page content
 *
 * @return Return a renderable array of content.
 */
function citcon_petition_page($node) {
  // Add the original page to the breadcrumb trail.
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l($node->title, 'node/' . $node->nid);
  drupal_set_breadcrumb($breadcrumb);

  // Get the body text
  $body = citcon_get_campaign_info($node, 'citcon_pet_dest_text');

  $body = check_markup($body[0]['value'], $body[0]['format']);
  $body = token_replace($body, array('cc-c' => $node));

  return $body;
}
