<?php

/**
 * @file
 * Citizens Connected tokens.
 */

/**
 * Implements hook_token_info().
 */
function citcon_token_info() {

  // Init token info array
  $info = array();


  /*   * *******************
   * Static tokens
   * ******************* */
  // Petition tokens, added to campaign tokens later.
  $info['types']['cc-petition'] = array(
    'name' => t('Campaign Petition tokens'),
    'description' => t('Tokens for displaying a petition list of your campaign supporters.'),
    'needs-data' => 'cc-c',
  );
  $info['tokens']['cc-petition']['url'] = array(
    'name' => t('Petition Page URL'),
    'description' => t("Token to display the petition page URL."),
  );
  $info['tokens']['cc-petition']['title'] = array(
    'name' => t('Petition Page Title'),
    'description' => t("Token to display the petition page title."),
  );
  $info['tokens']['cc-petition']['primary-list'] = array(
    'name' => t('Primary Petition List'),
    'description' => t("Token to display the primary petition list."),
  );
  $info['tokens']['cc-petition']['primary-pager'] = array(
    'name' => t('Primary Petition Pager'),
    'description' => t("Token to display the primary petition pager."),
  );
  $info['tokens']['cc-petition']['secondary-list'] = array(
    'name' => t('Secondary Petition List'),
    'description' => t("Token to display the secondary petition list."),
  );
  $info['tokens']['cc-petition']['secondary-pager'] = array(
    'name' => t('Secondary Petition Pager'),
    'description' => t("Token to display the secondary petition pager."),
  );


  /*   * *******************
   * "Dynamic" tokens
   * ******************* */

  // Share tokens, added to campaign tokens later.
  // These are all plugins, so we first define the type,
  // then the plugins add the tokens.
  if ($plugins = citcon_get_share_plugins()) {
    $info['types']['cc-share'] = array(
      'name' => t('Social Media Sharing tokens'),
      'description' => t('Tokens for sharing your campaign on social media.'),
      'needs-data' => 'cc-c',
    );

    // Create blank array... ready to accept token info.
    $info['tokens']['cc-share'] = array();

    // Get token_info for plugins
    foreach ($plugins as $plugin) {
      if ($function = ctools_plugin_get_function($plugin, 'sharing token_info')) {
        // Create the fieldset, and attach form elements
        $info['tokens']['cc-share'] += $function();
      }
    }
  }



  // Tokens for individual supporters... by supporter type.
  // With instance and field info added.
  $types = citcon_supporter_get_types();
  foreach (array_keys($types) as $type_id) {
    $insts = field_info_instances('citcon_supporter', $type_id);
    foreach (array_keys($insts) as $inst_id) {
      $insts[$inst_id]['field_info'] = field_info_field($inst_id);
    }
    $types[$type_id]->instances = $insts;
  }

  foreach ($types as $type) {
    // Create a token type particularly for each supporter type
    // For each supporter type, lets display their fields.
    $info['types']['cc-st-' . $type->type] = array(
      'name' => t('CitCon "@type" fields', array('@type' => $type->label)),
      'description' => t('Tokens related a particular supporter type.'),
      'needs-data' => 'cc-s',
    );

    foreach ($type->instances as $inst_id => $inst) {
      // Find which field tokens to add based on cardinality.
      if ($inst['field_info']['cardinality'] == 1) {
        $columns = array_keys($inst['field_info']['columns']);
        $hash = substr(md5(implode('', $columns)), 0, 9);
        $val_type = 'cc-tt-svf-' . $hash;
        //$info['types'][$val_type] = array(
        //  'name' => t('CC TT - SVF (@hash)', array('@hash' => $hash)),
        //  'description' => t('CitCon Token Tools - Single Value Field for @columns', array('@columns' => implode(', ', $columns))),
        //  'needs-data' => 'values-array',
        //);
        //$info['tokens'][$val_type]['cardinality'] = array(
        //  'name' => t('Cardinality'),
        //  'description' => t("The cardinality value of the field."),
        //);
        //foreach ($columns as $column) {
        //  $info['tokens'][$val_type][$column] = array(
        //    'name' => $column,
        //    'description' => t('The "@col" value.', array("@col" => $column)),
        //  );
        //}
      }
      elseif ($inst['field_info']['cardinality'] == 2) {
        $val_type = 'cc-tt-dvf';
      }
      else {
        $val_type = 'cc-tt-mvf';
      }

      // Add a token for each field
      $info['tokens']['cc-st-' . $type->type][$inst_id] = array(
        'name' => $inst['label'],
        'description' => t('The "@type" field.', array('@type' => $inst['label'])),
        'type' => $val_type,
      );
    }

    // Date tokens
    if (!empty($info['tokens']['cc-st-' . $type->type])) {
      $info['tokens']['cc-st-' . $type->type]['date'] = array(
        'name' => 'Date',
        'description' => t('The date the supporter signed.'),
        'type' => 'date',
      );
    }

    // Supporters tokens.
    // Lets give first, last, etc, options.
    $info['types']['cc-ss-' . $type->type] = array(
      'name' => t('CitCon Supporters (@type)', array('@type' => $type->label)),
      'description' => t('Tokens related to "@type" supporter type', array('@type' => $type->label)),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-ss-' . $type->type]['count'] = array(
      'name' => t('Count'),
      'description' => t("The campaign supporter count."),
    );
    $info['tokens']['cc-ss-' . $type->type]['goal'] = array(
      'name' => t('Goal'),
      'description' => t("The campaign goal."),
    );
    $info['tokens']['cc-ss-' . $type->type]['first'] = array(
      'name' => t('First'),
      'description' => t("The first campaign supporter."),
      'type' => 'cc-st-' . $type->type,
    );
    $info['tokens']['cc-ss-' . $type->type]['last'] = array(
      'name' => t('Last'),
      'description' => t("The last campaign supporter."),
      'type' => 'cc-st-' . $type->type,
    );
    $info['tokens']['cc-ss-' . $type->type]['pos'] = array(
      'name' => t('Position'),
      'description' => t("A campaign supporter at a particular position. Starting with 1 as the last person. 0 does nothing, and negative numbers go the other way. So, -1 would be the first person."),
      'type' => 'cc-st-' . $type->type,
      'dynamic' => TRUE,
    );

    // HTML Campaign tokens, with supporter data for each supporter type
    $info['types']['cc-c-' . $type->type] = array(
      'name' => t('Campaign Tokens (@type)', array('@type' => $type->label)),
      'description' => t('Tokens related to the campaign being viewed. With "@type" supporter data.', array('@type' => $type->label)),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type]['supporter-form'] = array(
      'name' => t('Form'),
      'description' => t("Supporter Form."),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type]['title'] = array(
      'name' => t('Title'),
      'description' => t("Campaign title."),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type]['url'] = array(
      'name' => t('URL'),
      'description' => t("Campaign URL."),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type]['petition'] = array(
      'name' => t('Petition Lists'),
      'description' => t("Petition lists for this campaign."),
      'type' => 'cc-petition',
    );
    $info['tokens']['cc-c-' . $type->type]['share'] = array(
      'name' => t('Sharing'),
      'description' => t("Social Media Sharing tokens for this campaign."),
      'type' => 'cc-share',
    );
    $info['tokens']['cc-c-' . $type->type]['supporters'] = array(
      'name' => t('Supporters'),
      'description' => t("Supporter tokens for this campaign."),
      'type' => 'cc-ss-' . $type->type,
    );
    if (module_exists('citcon_graph')) {
      $info['tokens']['cc-c-' . $type->type]['graphs'] = array(
        'name' => t('Graphs'),
        'description' => t("Graph tokens for this campaign"),
        'type' => 'cc-g',
      );
    }

    // NO-HTML Campaign tokens, with supporter data for each supporter type
    $info['types']['cc-c-' . $type->type . '-nohtml'] = array(
      'name' => t('Campaign Tokens (@type)', array('@type' => $type->label)),
      'description' => t('Tokens related to the campaign being viewed. With "@type" supporter data.', array('@type' => $type->label)),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type . '-nohtml']['title'] = array(
      'name' => t('Title'),
      'description' => t("Campaign title."),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type . '-nohtml']['url'] = array(
      'name' => t('URL'),
      'description' => t("Campaign URL."),
      'needs-data' => 'cc-c',
    );
    $info['tokens']['cc-c-' . $type->type . '-nohtml']['petition'] = array(
      'name' => t('Petition Lists'),
      'description' => t("Petition lists for this campaign."),
      'type' => 'cc-petition',
    );
    $info['tokens']['cc-c-' . $type->type . '-nohtml']['supporters'] = array(
      'name' => t('Supporters'),
      'description' => t("Supporter tokens for this campaign."),
      'type' => 'cc-ss-' . $type->type,
    );
  }

  return $info;
}

/**
 * Implements hook_tokens()
 */
function citcon_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Campaign tokens
  if (strpos($type, 'cc-c-') === 0) {
    // Check that we have a campaign node... or do nothing
    if (!isset($data['cc-c']->nid)) {
      return;
    }

    $node = $data['cc-c'];
    $campaign_values = citcon_get_campaign_info($node);
    foreach ($tokens as $name => $original) {
      $op = explode(':', $name);

      // First level of available operations
      if (isset($op[0])) { 
        switch ($op[0]) {
          case 'supporter-form' :
            // Get the supporter type machine name.
            $supporter_type = citcon_get_campaign_info($node, 'citcon_fields_supporter_type');
  
            // if none... replace with nothing.
            if (empty($supporter_type) || empty($supporter_type[0]['value'])) {
              $replacements[$original] = '';
              continue;
            }
  
            // Set basic values for a supporter entity.
            $values = array(
              'nid' => $node->nid,
              'type' => $supporter_type[0]['value'],
              'language' => $node->language,
            );
  
            // Create a new blank supporter using the values.
            $supporter = entity_create('citcon_supporter', $values);
  
            // Then create an edit form for them.
            $entity_form = entity_form('citcon_supporter', $supporter);
            $replacements[$original] = drupal_render($entity_form);
            break;
          case 'title' :
            $replacements[$original] = check_plain($node->title);
            break;
          case 'url' :
            $replacements[$original] = url('node/' . $node->nid, array('absolute' => TRUE));
            break;
          case 'graphs' :
            // Chained token relationships.
            if ($graph_tokens = token_find_with_prefix($tokens, 'graphs')) {
              $replacements += token_generate('cc-g', $graph_tokens, array('cc-c' => $data['cc-c']), $options);
            }
            break;
          case 'share' :
            // Chained token relationships.
            if ($share_tokens = token_find_with_prefix($tokens, 'share')) {
              $replacements += token_generate('cc-share', $share_tokens, array('cc-c' => $data['cc-c']), $options);
            }
            break;
          case 'petition' :
            // Chained token relationships.
            if ($petition_tokens = token_find_with_prefix($tokens, 'petition')) {
              $replacements += token_generate('cc-petition', $petition_tokens, array('cc-c' => $data['cc-c']), $options);
            }
            break;
          case 'supporters' :
            // Get all the supporters for this campaign.
            $supporter = FALSE;
            $supporters = NULL;   // Do not load all the supporters unless necessary
  
            // Initialize a supporter ID var to FALSE
            $sid = FALSE;
  
            // Second level of operations
            if (isset($op[1])) {
              switch ($op[1]) {
                case 'count' :
                default :
                  // Count the supporters efficiently (including a parent/sibling translation)
                  // using citcon_get_campaign_supporters_count() function.
                  $nid = $data['cc-c']->nid;
                  $tnid = isset($data['cc-c']->tnid) ? $data['cc-c']->tnid : 0;
                  $replacements[$original] = citcon_get_campaign_supporters_count($nid, $tnid) + check_plain($campaign_values['citcon_goal_offset'][0]['value']);
                  break;
                case 'goal' :
                  $replacements[$original] = check_plain($campaign_values['citcon_goal'][0]['value']);
                  break;
                case 'first' :
                  $supporters = citcon_get_all_supporters($data['cc-c']->nid); // @todo: optimize this to not get all supporters
                  if (count($supporters) > 0) {
                    // Get the first value... which EntityFieldQuery will return as 1
                    $sid = $supporters[count($supporters) - 1];
                    $supporter = entity_load('citcon_supporter', array($sid));
                    $supporter = $supporter[$sid];
                    $sup_token_key = $op[2] . (isset($op[3]) ? ':' . $op[3] : '');
                  }
                  break;
                case 'last' :
                  $supporters = citcon_get_all_supporters($data['cc-c']->nid); // @todo: optimize this to not get all supporters
                  if (count($supporters) > 0) {
                    // Get the last value... which will be equal to the count
                    $sid = $supporters[0];
                    $supporter = entity_load('citcon_supporter', array($sid));
                    $supporter = $supporter[$sid];
                    $sup_token_key = $op[2] . (isset($op[3]) ? ':' . $op[3] : '');
                  }
                  break;
                case 'pos' :
                  $supporters = citcon_get_all_supporters($data['cc-c']->nid); // @todo: optimize this to not get all supporters
                  // Get a specific value...
                  $pos = $op[2];
                  if ($pos > 0) {
                    $sid = $supporters[$pos - 1];
                    $supporter = entity_load('citcon_supporter', array($sid));
                    $supporter = $supporter[$sid];
                  }
                  elseif ($pos < 0) {
                    $sid = $supporters[count($supporters) - ($pos * -1)];
                    $supporter = entity_load('citcon_supporter', array($sid));
                    $supporter = $supporter[$sid];
                  }
                  $sup_token_key = $op[3] . (isset($op[4]) ? ':' . $op[4] : '');
                  break;
              } // endswitch ($op[1])
            } // endif isset($op[1])
            // If we've got a supporter to load... let's do it.
            if ($supporter) {
              $replacements += token_generate('cc-st-' . $campaign_values['citcon_fields_supporter_type'][0]['value'], array($sup_token_key => $original), array('cc-s' => $supporter), $options);
            }
  
            break;
        } // endswitch ($op[0])
      } // endif isset($op[0]) - token has valid 3rd level
    }
  }

  // Supporter field tokens
  if (strpos($type, 'cc-st-') === 0) {
    // Get the supporter data ready.
    $supporter = $data['cc-s'];

    foreach ($tokens as $name => $original) {
      // Replace date tokens
      if ($date_tokens = token_find_with_prefix($tokens, 'date')) {
        $replacements += token_generate('date', $date_tokens, array('date' => $supporter->created), $options);
      }

      // Get field info, and columns
      $field_info = field_info_field($name);

      // If we don't have any field info... skip the token.
      if (empty($field_info)) {
        continue;
      }

      // Find out which columns are in here.
      $columns = array_keys($field_info['columns']);

      // Try to find a 'value' column
      if (in_array('value', $columns)) {
        $result = field_get_items('citcon_supporter', $supporter, $name);
        $replacements[$original] = filter_xss($result[0]['value']);
      }
      // Try to find a 'email' column
      elseif (in_array('email', $columns)) {
        $result = field_get_items('citcon_supporter', $supporter, $name);
        $replacements[$original] = check_plain($result[0]['email']);
      }
    }
  }

  // Share tokens
  // We'll only go into it if we've got plugins for it.
  if ($type == 'cc-share'
      && $plugins = citcon_get_share_plugins()) {

    // For each token, go through each plugin and send the data.
    foreach ($tokens as $name => $original) {

      // Get node data, and saved plugin data
      $node = $data['cc-c'];
      $plugin_data = citcon_get_plugin_data('citcon_plugin_sharing', $node);

      // Get token_replace for plugins
      foreach ($plugins as $plugin) {
        if ($function = ctools_plugin_get_function($plugin, 'sharing token_replace')) {
          if ($replace = $function($name, $plugin_data[$plugin['name']], $node)) {
            // Set the replacement value.
            $replacements[$original] = $replace;
          }
        }
      }
    }
  }

  // Petition tokens
  if ($type == 'cc-petition') {
    // Load campaign values and supporters for this campaign
    $node = $data['cc-c'];
    $campaign_values = citcon_get_campaign_info($node);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'url':
          $replacements[$original] = check_plain(url('node/' . $node->nid, array('absolute' => TRUE)) . '/' . $campaign_values['citcon_pet_dest_path'][0]['value']);
          break;
        case 'title':
          $replacements[$original] = check_plain($campaign_values['citcon_pet_dest_title'][0]['value']);
          break;
        case 'primary-list':
          // Set element var
          $element = 1;

          // Get all the values we need
          $rows = $campaign_values['citcon_petition_pri_rows'][0]['value'];
          $text = check_markup($campaign_values['citcon_petition_pri_text'][0]['value'], $campaign_values['citcon_petition_pri_text'][0]['format']);

          // Get paginated list of supporters
          $supporter_count = citcon_get_campaign_supporters_count($node->nid);
          pager_default_initialize($supporter_count, $rows, $element);
          $supporters = citcon_get_all_supporters($node->nid, $rows, $element);

          // Generate the return text
          $return = '';
          for ($i = 0; $i < $rows; $i++) {
            // If there are no more supporters, we're done.
            if ((count($supporters) - ($i + 1)) < 0) {
              $replacements[$original] = $return;
              continue;
            }
            // Get the supporter, at the appropriate position
            $sid = $supporters[$i];
            $supporter = entity_load('citcon_supporter', array($sid));
            $supporter = $supporter[$sid];

            $return .= token_replace($text, array('cc-s' => $supporter));
          }

          $replacements[$original] = $return;
          break;
        case 'primary-pager':
          // Set element var
          $element = 1;

          $replacements[$original] = theme('pager', array('element' => $element));
          break;
        case 'secondary-list':
          // Set element var
          $element = 2;

          // Get all the values we need
          $rows = $campaign_values['citcon_petition_sec_rows'][0]['value'];
          $text = check_markup($campaign_values['citcon_petition_sec_text'][0]['value'], $campaign_values['citcon_petition_sec_text'][0]['format']);

          // Get paginated list of supporters
          $supporter_count = citcon_get_campaign_supporters_count($node->nid);
          pager_default_initialize($supporter_count, $rows, $element);
          $supporters = citcon_get_all_supporters($node->nid, $rows, $element);

          // Generate the return text
          $return = '';
          for ($i = 0; $i < $rows; $i++) {
            // If there are no more supporters, we're done.
            if ((count($supporters) - ($i + 1)) < 0) {
              $replacements[$original] = $return;
              continue;
            }
            // Get the supporter, at the appropriate position
            $sid = $supporters[$i];
            $supporter = entity_load('citcon_supporter', array($sid));
            $supporter = $supporter[$sid];

            $return .= token_replace($text, array('cc-s' => $supporter));
          }

          $replacements[$original] = $return;
          break;
        case 'secondary-pager':
          // Set element var
          $element = 2;

          $replacements[$original] = theme('pager', array('element' => $element));
          break;
      }
    }
  }

  return $replacements;
}
