<?php
/**
 * @file
 * Plugin for embedding campaign results in an iframe.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'petition',
  'title' => t("iFrame Page"),
  'description' => t('Allows you to define an iframe petition page, so you can view the results of your campaign in an iframe on some other page.'),
  'settings form' => 'iframe_page_settings_form',
  'menu' => 'iframe_page_menu',
  'node view' => 'iframe_page_node_view',
);

/**
 * Settings form, which will attach to "Settings][Plugins" section.
 */
function iframe_page_settings_form($form, $form_state, $defaults) {

  // display token tree
  $drop_down = '';
  $node = $form['#node'];
  $supporter_type = citcon_get_campaign_info($node, 'citcon_fields_supporter_type');
  if ($supporter_type) {
    // Get the title and content of token tree drop down.
    $handle = t('Tokens Supported') . ' <span class="citcon-campaign-tokens citcon-form-element-flag" title="This field supports Campaign Tokens">L-T</span>';
    $tree = array(
      '#theme' => 'token_tree',
      '#token_types' => array('cc-c-' . $supporter_type[0]['value']),
      '#recursion_limit' => 5,
      '#global_types' => FALSE,
    );
    // Then render it.
    $drop_down = '<div class="citcon-token-trees">';
    $drop_down .= theme('ctools_collapsible', array('handle' => $handle, 'content' => drupal_render($tree), 'collapsed' => TRUE));
    $drop_down .= '</div>';
  }

  // Create the form
  $form = array();
  $form['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Page body'),
    '#description' => t("HTML to use for the page.") . $drop_down,
    '#default_value' => isset($defaults['body']['value']) ? $defaults['body']['value'] : NULL,
    '#format' => isset($defaults['body']['format']) ? $defaults['body']['format'] : NULL,
  );
  $form['css_url'] = array(
    '#type' => 'textfield',
    '#title' => t('External CSS URL'),
    '#description' => t("The URL of an external CSS file.<br />Be sure to include http:// at the beginning.<br /> Note: This will be the ONLY CSS file in use."),
    '#default_value' => isset($defaults['css_url']) ? $defaults['css_url'] : NULL,
  );
  $form['iframe_code'] = array(
    '#type' => 'textarea',
    '#title' => t('iframe code'),
    '#description' => t("This is the embed code."),
    '#value' => '<iframe src="' . url('node/' . $node->nid . '/iframe', array('absolute' => TRUE)) . '"></iframe>',
  );

  return $form;
}

/**
 * This is where we'll be adding in our CSS
 */
function iframe_page_node_view($defaults) {
  //dsm($defaults);
  $css_url = $defaults['css_url'];

  $options = array(
    'type' => 'external',
    'every_page' => FALSE,
    'group' => CSS_THEME,
    'weight' => 99,
  );
  $data = $defaults['css_url'];

  drupal_add_css($data, $options);
}

/**
 * implements hook_menu...but not really
 *
 * @param $plugin
 *   Array - Metadata about the plugin, used mostly for filepathing
 *
 * @return Menu items array
 */
function iframe_page_menu($plugin) {
  $items = array();

  $items['node/%node/iframe'] = array(
    'page callback' => 'iframe_iframe_page',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file path' => $plugin['path'],
    'file' => $plugin['file'],
  );

  return $items;
}

function iframe_iframe_page($node) {
  $defaults = citcon_get_plugin_data('citcon_plugin_petition', $node);
  $defaults = $defaults['iframe_page'];
//dsm($defaults);
//return'hey';
  $css_url = $defaults['css_url'];
  $body = check_markup(token_replace($defaults['body']['value'], array('cc-c' => $node)), $defaults['body']['format']);

  // I know this is bad form, but it's easy.
  // The alternative is setting up a plugin hook for hook_theme
  // then blah blah... I'm outputting simple HTML for a stripped down version of this HTML
  ?>

  <!DOCTYPE html>
  <html>
    <head>
      <title><?php print $node->title ?></title>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print $css_url ?>" />  </head>
    <body>
  <?php print $body ?>
    </body>
  </html>

  <?php
}

