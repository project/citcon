<?php

/**
 * @file
 * Plugin to allow external CSS per campaign.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'settings',
  'title' => t("External CSS"),
  'description' => t('Allows you to define external CSS per campaign.'),
  'settings form' => 'external_css_settings_form',
  'node view' => 'external_css_node_view',
);

/**
 * Settings form, which will attach to "Settings][Plugins" section.
 */
function external_css_settings_form($form, $form_state, $defaults) {

  $form = array();
  $form['css_url'] = array(
    '#type' => 'textfield',
    '#title' => t('External CSS URL'),
    '#description' => t("What's the URL of the external CSS file?<br />Be sure to include http:// at the beginning.<br /> Note: This CSS file will be placed last, which means this stylesheet will have final say on conflicting styles."),
    '#default_value' => isset($defaults['css_url']) ? $defaults['css_url'] : NULL,
  );

  return $form;
}

/**
 * This is where we'll be adding in our CSS
 */
function external_css_node_view($defaults) {
  //dsm($defaults);
  $css_url = $defaults['css_url'];

  $options = array(
    'type' => 'external',
    'every_page' => FALSE,
    'group' => CSS_THEME,
    'weight' => 99,
  );
  $data = $defaults['css_url'];

  drupal_add_css($data, $options);
}
