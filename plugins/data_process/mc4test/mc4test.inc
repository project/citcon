<?php
/**
 * @file
 * Plugin to provide Mailchimp integration options.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'data_process',
  'title' => t("MailChimp for Testing"),
  'description' => t('Options to integrate with MailChimp.'),
  // Functions
  'hook_cron' => 'mc4test_cron',
  'settings form' => 'mc4test_settings_form',
  'citcon_config settings' => 'mc4test_citcon_config_settings_form',
);

/**
 * Settings form, which will attach to "Settings][Sharing" section.
 */
function mc4test_settings_form($form, $form_state, $defaults) {
  $return = array();
  $plugin_name = 'mc4test';

  // If we're missing default values... let get the global defaults
  if (empty($defaults)) {
    $defaults = variable_get('citcon_config_' . $plugin_name . '_settings', array());
  }

  // Add frequency settings
  $return['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-Wide Frequency Settings'),
  );
  $return['api']['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Api Key'),
    '#description' => t("'Nuff said."),
    '#default_value' => isset($defaults['api']['api_key']) ? $defaults['api']['api_key'] : '',
  );

  // Get list_options
  $return['list_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Options'),
      ) + mc4test_list_options(isset($defaults['list_options']) ? $defaults['list_options'] : array());

  // List Specific settings
  if (isset($defaults['api']['api_key']) && !empty($defaults['api']['api_key'])
      && isset($defaults['list_options']['mc_list_id']) && !empty($defaults['list_options']['mc_list_id'])) {

    // Find out if we're trying to refresh the list.
    $refresh_groups = FALSE;
    $refresh_fields = FALSE;
    if (isset($form_state['clicked_button'])) {
      switch ($form_state['clicked_button']['#name']) {
        case 'refresh_groups':
          $refresh_groups = TRUE;
          drupal_set_message(t('Refreshing MailChimp groups list.'));
          break;
        case 'refresh_fields' :
          $refresh_fields = TRUE;
          drupal_set_message(t('Refreshing MailChimp fields.'));
          break;
      }
    }

    // Get campaign_group_options
    $return['group_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Group Options'),
        )
        + mc4test_campaign_group_options($defaults, $refresh_groups)
        + mc4test_online_action_group_options($defaults, $refresh_groups);

    // Refresh groups button
    $return['group_options']['refresh_groups'] = array(
      '#type' => 'button',
      '#name' => 'refresh_groups',
      '#value' => 'Refresh Group List',
      '#weight' => 99,
    );

    // Get field mapping form
    $return['field_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Field Mapping'),
        ) + mc4test_field_mapping($defaults, $refresh_fields);

    // Refresh fields button
    $return['field_mapping']['refresh_fields'] = array(
      '#type' => 'button',
      '#name' => 'refresh_fields',
      '#value' => 'Refresh MailChimp Fields',
      '#weight' => 99,
    );
  }
  // Display message about more options being available.
  else {
    $return['more_options_avail'] = array(
      '#type' => 'item',
      '#title' => t('More Options Available'),
      '#description' => t('Please fill out the API key and List ID options to see more Mail Chimp options.'),
    );
  }

  return $return;
}

/**
 * Settings form for citcon_config page
 */
function mc4test_citcon_config_settings_form($form, $form_state, $defaults) {
  // If we've gotten to this point... the plugin is installed and enabled.
  // So, lets make sure we have the proper fields in the DB.
  if (!db_field_exists('citcon_supporter', 'mc4test_uploaded')) {
    $spec = array(
      'description' => 'MailChimp upload status of supporters.',
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    );

    // Add the field
    db_add_field('citcon_supporter', 'mc4test_uploaded', $spec);
  }

  // Testing groups
  //$groups = mc4test_get_groups($defaults['api']['api_key'], $defaults['list_options']['mc_list_id']);
  //dsm($groups);
  // Testing cron
  //mc4test_sync();
  // Start our form array
  $return = array();

  // Add frequency settings
  $return['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-Wide Frequency Settings'),
  );
  $return['api']['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Api Key'),
    '#description' => t("'Nuff said."),
    '#default_value' => isset($defaults['api']['api_key']) ? $defaults['api']['api_key'] : '',
  );

  // Add frequency settings
  $return['freq'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-Wide Frequency Settings'),
  );
  $return['freq']['batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch Size'),
    '#description' => t('When X number of supporter records are ready to be sent to Salesforce... they will :)'),
    '#default_value' => isset($defaults['freq']['batch_size']) ? $defaults['freq']['batch_size'] : 500,
  );
  $return['freq']['auto_sync_rate'] = array(
    '#type' => 'select',
    '#options' => array(
      NULL => t('Never'),
      43200 => t('6 hours'),
      86400 => t('1 day'),
      259200 => t('3 days'),
      604800 => t('1 week'),
    ),
    '#title' => t('Auto-Sync Interval'),
    '#description' => t('How often should we auto-sync with Salesforce?'),
    '#default_value' => isset($defaults['freq']['auto_sync_rate']) ? $defaults['freq']['auto_sync_rate'] : 86400,
  );

  // Get list_options
  $return['list_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default List Options'),
      ) + mc4test_list_options(isset($defaults['list_options']) ? $defaults['list_options'] : array());

  // List Specific settings
  if (isset($defaults['api']['api_key']) && !empty($defaults['api']['api_key'])
      && isset($defaults['list_options']['mc_list_id']) && !empty($defaults['list_options']['mc_list_id'])) {

    // Find out if we're trying to refresh the list.
    $refresh_groups = FALSE;
    $refresh_fields = FALSE;
    if (isset($form_state['clicked_button'])) {
      switch ($form_state['clicked_button']['#name']) {
        case 'refresh_groups':
          $refresh_groups = TRUE;
          drupal_set_message(t('Refreshing MailChimp groups list.'));
          break;
        case 'refresh_fields' :
          $refresh_fields = TRUE;
          drupal_set_message(t('Refreshing MailChimp fields.'));
          break;
      }
    }

    // Get campaign_group_options
    $return['group_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Group Options'),
        )
        + mc4test_campaign_group_options($defaults, $refresh_groups)
        + mc4test_online_action_group_options($defaults, $refresh_groups);

    // Refresh groups button
    $return['group_options']['refresh_groups'] = array(
      '#type' => 'button',
      '#name' => 'refresh_groups',
      '#value' => 'Refresh Group List',
      '#weight' => 99,
    );

    // Get field mapping form
    $return['field_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Field Mapping'),
        ) + mc4test_field_mapping($defaults, $refresh_fields);

    // Refresh fields button
    $return['field_mapping']['refresh_fields'] = array(
      '#type' => 'button',
      '#name' => 'refresh_fields',
      '#value' => 'Refresh MailChimp Fields',
      '#weight' => 99,
    );
  }
  // Display message about more options being available.
  else {
    $return['more_options_avail'] = array(
      '#type' => 'item',
      '#title' => t('More Options Available'),
      '#description' => t('Please fill out the API key and List ID options to see more Mail Chimp options.'),
    );
  }

  return $return;
}

/**
 * Callback to return DSF specific form options
 *
 * @return Partial form array.
 */
function mc4test_list_options($defaults) {
  $form = array();

  $form['mc_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MailChimp List ID'),
    '#default_value' => isset($defaults['mc_list_id']) ? $defaults['mc_list_id'] : '',
  );
  //$form['mc_lead_source'] = array(
  //  '#type' => 'textfield',
  //  '#title' => t('Salesforce Lead Source'),
  //  '#default_value' => isset($defaults['mc_lead_source']) ? $defaults['mc_lead_source'] : '',
  //);
  //$form['mc_campaign_title'] = array(
  //  '#type' => 'textfield',
  //  '#title' => t('Salesforce Campaign Title'),
  //  '#default_value' => isset($defaults['mc_campaign_title']) ? $defaults['mc_campaign_title'] : '',
  //);

  return $form;
}

/**
 * Callback to return DSF specific form options
 *
 * @param $defaults
 *  Array - Our default values for this part of the form.
 *
 * @param $refresh_groups
 *   Bool - Whether or not to reset the cache on the fields list.
 *
 * @return Partial form array.
 */
function mc4test_campaign_group_options($defaults, $refresh_groups = FALSE) {
  // Init vars
  $form = array();
  $api_key = $defaults['api']['api_key'];
  $list_id = $defaults['list_options']['mc_list_id'];
  $options = mc4test_get_campaign_groups($api_key, $list_id, $refresh_groups);

  // For each of the SF fields... present the options.
  $form['campain_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which campaign group should we attach supporters to?'),
    '#options' => array_combine($options, $options),
    '#default_value' => isset($defaults['group_options']['campain_options']) ? $defaults['group_options']['campain_options'] : array(),
  );

  return $form;
}

/**
 * Callback to return DSF specific form options
 *
 * @param $defaults
 *  Array - Our default values for this part of the form.
 *
 * @param $refresh_groups
 *   Bool - Whether or not to reset the cache on the fields list.
 *
 * @return Partial form array.
 */
function mc4test_online_action_group_options($defaults, $refresh_groups = FALSE) {
  // Init vars
  $form = array();
  $api_key = $defaults['api']['api_key'];
  $list_id = $defaults['list_options']['mc_list_id'];
  $options = mc4test_get_online_action_groups($api_key, $list_id, $refresh_groups);

  // For each of the SF fields... present the options.
  $form['online_actions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which online action group should we attach supporters to?'),
    '#options' => array_combine($options, $options),
    '#default_value' => isset($defaults['group_options']['online_actions']) ? $defaults['group_options']['online_actions'] : array(),
  );

  return $form;
}

/**
 * Callback to return DSF mapping form
 *
 * @param $defaults
 *  Array - Our default values for this part of the form.
 *
 * @param $refresh_fields
 *   Bool - Whether or not to reset the cache on the fields list.
 *
 * @return Partial form array.
 */
function mc4test_field_mapping($defaults, $refresh_fields = FALSE) {
  // Init vars
  $form = array();
  $options = array(NULL => 'None');
  $api_key = $defaults['api']['api_key'];
  $list_id = $defaults['list_options']['mc_list_id'];

  // Get field options
  $types = field_info_instances('citcon_supporter');
  foreach ($types as $type) {
    foreach ($type as $id => $field) {
      $options['Supporter Fields'][$id] = $field['label'] . ' - ' . $id;
    }
  }

  // Get meta data options
  foreach (mc4test_get_meta_options() as $id => $label) {
    $options['Meta Data'][$id] = $label . ' - ' . $id;
  }

  // Get settings data options
  foreach (mc4test_list_options(array()) as $id => $form_item) {
    $options['Campaign Settings Data'][$id] = $form_item['#title'] . ' - ' . $id;
  }

  // For each of the SF fields... present the options.
  foreach (mc4test_get_mc_fields($api_key, $list_id, $refresh_fields) as $mc_field_id => $mc_field_name) {
    $form[$mc_field_id] = array(
      '#type' => 'select',
      '#field_prefix' => $mc_field_name,
      '#options' => $options,
      '#default_value' => isset($defaults['field_mapping'][$mc_field_id]) ? $defaults['field_mapping'][$mc_field_id] : '',
    );
  }

  return $form;
}

/**
 * Callback to get the meta data we might need for the SF fields.
 *
 * @return An array of options
 */
function mc4test_get_meta_options() {
  return array(
    'supporter_id' => 'Supporter ID',
    'supporter_created' => 'Supporter Created Date',
    'node_id' => 'Node ID',
    'node_created' => 'Campaign Created Date',
    'node_language' => 'Campaign Language',
  );
}

/**
 * Callback to get the MC fields we need data for.
 *
 * @param $api_key
 *   String - The MC API key.
 *
 * @param $list_id
 *   String - The MC list ID.
 *
 * @param $reset
 *   Bool - Optional - Reset the cache or not.
 *
 * @return An array of options.
 */
function mc4test_get_mc_fields($api_key, $list_id, $reset = FALSE) {
  return mc4test_get_fields($api_key, $list_id, $reset);
}

/**
 * Callback to get the EDC MC Campaign groups we need data for.
 *
 * @param $api_key
 *   String - The MC API key.
 *
 * @param $list_id
 *   String - The MC list ID.
 *
 * @param $reset
 *   Bool - Optional - Reset the cache or not.
 *
 * @return An array of options.
 */
function mc4test_get_campaign_groups($api_key, $list_id, $reset = FALSE) {
  return mc4test_get_groups($api_key, $list_id, 'Campaign', $reset);
}

/**
 * Callback to get the EDC MC Online Action groups we need data for.
 *
 * @param $api_key
 *   String - The MC API key.
 *
 * @param $list_id
 *   String - The MC list ID.
 *
 * @param $reset
 *   Bool - Optional - Reset the cache or not.
 *
 * @return An array of options.
 */
function mc4test_get_online_action_groups($api_key, $list_id, $reset = FALSE) {
  return mc4test_get_groups($api_key, $list_id, 'Online Actions', $reset);
}

/**
 * MC API call to find all groups in a list.
 *
 * @param $api_key
 *   String - The MC API key.
 *
 * @param $list_id
 *   String - The MC list ID.
 *
 * @param $group_id
 *   String - Optional - The particular group to look for.
 *
 * @param $reset
 *   Bool - Optional - Reset the cache or not.
 *
 * @return An array of options.
 */
function mc4test_get_groups($api_key, $list_id, $group_id = NULL, $reset = FALSE) {
  $groups = cache_get('citcon-mc_groups-' . $list_id);

  // If there's nothing in the cache... call the API and store the result.
  if (!isset($groups->data) || $reset) {
    // Include the module, and set the time limit to something larger.
    module_load_include('php', 'citcon', 'plugins/data_process/mc4test/MCAPI.class');
    drupal_set_time_limit(120);

    // Init MCAPI
    $api = new MCAPI($api_key);

    // Send our data...
    $return_val = $api->listInterestGroupings($list_id);

    // Check for API error
    if ($api->errorCode) {
      // Throw an error and log it.
      drupal_set_message(t("Sorry, Citizens Connected couldn't update the list of groups from MailChimp. We've logged the error and will look into it."), 'error');
      watchdog('citcon', 'MC find list groups failed... errorCode = @errorCode | errorMessage = @errorMessage', array('@errorCode' => $api->errorCode, '@errorMessage' => $api->errorMessage));
    }
    // ... otherwise, store it in the cache.
    else {
      // Build a usable $groups array.
      $groups = array();
      foreach ($return_val as $group) {
        foreach ($group['groups'] as $item) {
          $groups[$group['name']][$item['name']] = $item['name'];
        }
      }
      cache_set('citcon-mc_groups-' . $list_id, $groups);
    }
  }
  // Or if there's cache data... get that.
  else {
    $groups = $groups->data;
  }

  if (isset($group_id)) {
    return $groups[$group_id];
  }
  else {
    return $groups;
  }
}

/**
 * MC API call to find all fields in a list.
 *
 * @param $api_key
 *   String - The MC API key.
 *
 * @param $list_id
 *   String - The MC list ID.
 *
 * @param $reset
 *   Bool - Optional - Reset the cache or not.
 *
 * @return An array of options.
 */
function mc4test_get_fields($api_key, $list_id, $reset = FALSE) {
  $fields = cache_get('citcon-mc_fields-' . $list_id);

  // If there's nothing in the cache... call the API and store the result.
  if (!isset($fields->data) || $reset) {
    // Include the module, and set the time limit to something larger.
    module_load_include('php', 'citcon', 'plugins/data_process/mc4test/MCAPI.class');
    drupal_set_time_limit(120);

    // Init MCAPI
    $api = new MCAPI($api_key);

    // Send our data...
    $return_val = $api->listMergeVars($list_id);

    // Check for API error
    if ($api->errorCode) {
      // Throw an error and log it.
      drupal_set_message(t("Sorry, Citizens Connected couldn't update the list of fields from MailChimp. We've logged the error and will look into it."), 'error');
      watchdog('citcon', 'MC find list fields failed... errorCode = @errorCode | errorMessage = @errorMessage', array('@errorCode' => $api->errorCode, '@errorMessage' => $api->errorMessage));
    }
    // ... otherwise, store it in the cache.
    else {
      // Build a usable $groups array.
      $fields = array();
      foreach ($return_val as $field) {
        $fields[$field['tag']] = $field['name'] . ' - ' . $field['tag'];
      }
      cache_set('citcon-mc_fields-' . $list_id, $fields);
    }
  }
  // Or if there's cache data... get that.
  else {
    $fields = $fields->data;
  }

  return $fields;
}

/**
 * Cron tasks... which will be to figure out if we should run a sync.
 */
function mc4test_cron() {
  // Get our settings
  $settings = variable_get('citcon_config_mc4test_settings', FALSE);

  // If we have no settings... do nothing.
  if (empty($settings)) {
    return;
  }

  // Get config vars, and current values... then sync
  $batch_time = isset($settings['freq']['auto_sync_rate']) ? $settings['freq']['auto_sync_rate'] : NULL;
  $batch_size = isset($settings['freq']['batch_size']) ? $settings['freq']['batch_size'] : 500;
  $cur_time = time() - (isset($settings['sync']['last']) ? $settings['sync']['last'] : 500);
  $cur_size = mc4test_count_batch();

  if ($batch_size < $cur_size || $batch_time > $cur_time) {
    mc4test_sync($batch_size);
    watchdog('citcon', 'Cron is uploading to MC with mc4test... batch time = @batch_time | batch size = @batch_size | cur time = @cur_time | cur size = @cur_size', array('@batch_time' => $batch_time, '@batch_size' => $batch_size, '@cur_time' => $cur_time, '@cur_size' => $cur_size));
  }
}

/**
 * Get to Bizzznasss... upload/sync Supporter data
 *
 * @param $batch_size
 *   Int - Number of people sync.
 */
function mc4test_sync($batch_size = 500) {
  // Include the module, and set the time limit to something larger.
  module_load_include('php', 'citcon', 'plugins/data_process/mc4test/MCAPI.class');
  drupal_set_time_limit(120);

  // Gather our data
  $sids = array();
  $data = mc4test_get_batch_array($batch_size, $sids);

  // If we have no data, do nothing.
  if (empty($data)) {
    return;
  }

  // Get our settings
  $settings = variable_get('citcon_config_mc4test_settings', array());
  $api_key = $settings['api']['api_key'];
  $list_id = $settings['list_options']['mc_list_id'];
  $optin = FALSE; //no, don't send optin emails
  $up_exist = TRUE; // yes, update currently subscribed users
  $replace_int = FALSE; // no, add interest, don't replace
  // Init MCAPI
  $api = new MCAPI($api_key);

  // Send our data...
  $vals = $api->listBatchSubscribe($list_id, $data, $optin, $up_exist, $replace_int);

  if ($api->errorCode) {
    watchdog('citcon', 'MC upload failed... errorCode = @errorCode | errorMessage = @errorMessage', array('@errorCode' => $api->errorCode, '@errorMessage' => $api->errorMessage));
  }
  else {
    mc4test_set_batch($sids);
  }

  return;
}

/**
 * Get the CSV string.
 *
 * @param $batch_Size
 *   Int - Number of supporters to upload
 *
 * @param $sids
 *   Array - Send empty... we'll populate it with Supporter IDs that we're getting
 *
 * @return The CSV string!
 */
function mc4test_get_batch_array($batch_size, &$sids) {
  if (!db_field_exists('citcon_supporter', 'mc4test_uploaded')) {
    return NULL;
  }

  // init rows
  $rows = array();

  // Get the Supporters which haven't been uploaded yet.
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'citcon_supporter')
      ->propertyCondition('mc4test_uploaded', 0)
      ->range(0, $batch_size)
      ->execute();

  // Get supporter objects
  $supporters = !empty($results) ? entity_load('citcon_supporter', array_keys($results['citcon_supporter'])) : array();

  // Get Site-Wide default data
  $default_settings = variable_get('citcon_config_mc4test_settings', array());

  // These are fields which require a bit more special care to get the value :)
  $meta_fields = array_keys(mc4test_get_meta_options() + mc4test_list_options(array()));

  foreach ($supporters as $sid => $supporter) {
    $sids[] = $sid;

    // Find out if the campaign has it's own settings
    $campaign = node_load($supporter->nid);
    $campaign_settings = citcon_get_plugin_data('citcon_plugin_data_process', $campaign);
    $supporter_data = citcon_get_supporter_info($sid, NULL, TRUE);

    // Figure out which settings to use.
    if (empty($campaign_settings)) {
      $settings = $default_settings;
    }
    else {
      $settings = $campaign_settings['mc4test'];
    }

    // Get required variables.
    $api_key = $settings['api']['api_key'];
    $list_id = $settings['list_options']['mc_list_id'];

    // Skip if we don't have what we need.
    if (empty($api_key) || empty($list_id)) {
      continue;
    }

    // init row
    $row = array();
    foreach (mc4test_get_mc_fields($api_key, $list_id) as $mc_field => $mc_field_label) {
      $supporter_field = $settings['field_mapping'][$mc_field];

      // If it's empty... set it to nothing.
      if (empty($supporter_field)) {
        $value = '';
      }
      // If we're looking for a meta field... do whatever special thing we need.
      elseif (in_array($supporter_field, $meta_fields)) {
        // If it's in the settings... get it!
        if (strpos($supporter_field, 'mc_') === 0) {
          $value = $settings['list_options'][$supporter_field];
        }
        // Otherwise, do that crazy voodoo.
        else {
          switch ($supporter_field) {
            case 'supporter_id':
              $value = $supporter->sid;
              break;
            case 'supporter_created':
              $value = $supporter->created;
              break;
            case 'node_id':
              $value = $campaign->nid;
              break;
            case 'node_created':
              $value = $campaign->created;
              break;
            case 'node_language':
              $value = $campaign->language;
              break;
          }
        }
      }
      // Otherwise, get the value as per usual.
      elseif (isset($supporter_data[$supporter_field])) {
        if (isset($supporter_data[$supporter_field][0]['value'])) {
          // If we found a value... set it
          $value = $supporter_data[$supporter_field][0]['value'];
        }
        elseif (isset($supporter_data[$supporter_field][0]['email'])) {
          // If we found an email... set it
          $value = $supporter_data[$supporter_field][0]['email'];
        }
      }

      // Insert the value into CSV
      $row[$mc_field] = $value;
    }

    if (isset($settings['group_options']['campaign_options']) && is_array($settings['group_options']['campaign_options'])) {
      // Campaign Groups
      $campaign_groups = array();
      foreach ($settings['group_options']['campaign_options'] as $group) {
        if (!empty($group)) {
          $campaign_groups[] = str_replace(',', '\,', $group);
        }
      }
      if (!empty($campaign_groups)) {
        // Insert the value into CSV
        $row['GROUPINGS'][] = array(
          'name' => 'Campaign',
          'groups' => implode(',', array_filter($campaign_groups)),
        );
      }
    }

    // Online Actions Groups
    if (isset($settings['group_options']['online_actions']) && is_array($settings['group_options']['online_actions'])) {
      $online_action_groups = array();
      foreach ($settings['group_options']['online_actions'] as $group) {
        if (!empty($group)) {
          $online_action_groups[] = str_replace(',', '\,', $group);
        }
      }
      if (!empty($online_action_groups)) {
        // Insert the value into CSV
        $row['GROUPINGS'][] = array(
          'name' => 'Online Actions',
          'groups' => implode(',', array_filter($online_action_groups)),
        );
      }
    }

    // Add the row to the rows array
    $rows[] = $row;
  }
  return $rows;
}

/**
 * Count the number of Supporters which haven't been uploaded to SF
 *
 * @return The number of people which haven't been uploaded.
 */
function mc4test_count_batch() {
  $query = new EntityFieldQuery();
  return $query->entityCondition('entity_type', 'citcon_supporter')
          ->propertyCondition('mc4test_uploaded', 0)
          ->count()
          ->execute();
}

/**
 * Flag the supporters as having been sync-ed
 *
 * @param $sids
 *   Array - Supporter IDs to set.
 */
function mc4test_set_batch($sids = array()) {
  // UPDATE {citcon_supporter} SET mc4test_uploaded=1 WHERE sid IN sids
  db_update('citcon_supporter')
      ->fields(array(
        'mc4test_uploaded' => 1,
      ))
      ->condition('sid', $sids)
      ->execute();
}
