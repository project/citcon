<?php

/**
 * @file
 * Plugin to provide simple Facebook sharing functionality.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'sharing',
  'title' => t("Facebook"),
  'description' => t('Options to share this on facebook.'),
  'settings form' => 'facebook_simple_settings_form',
  'sharing token_info' => 'facebook_simple_sharing_token_info',
  'sharing token_replace' => 'facebook_simple_sharing_token_replace',
);

/**
 * Settings form, which will attach to "Settings][Sharing" section.
 */
function facebook_simple_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  $campaign_values = citcon_get_campaign_info($node);
  $supporter_type = $campaign_values['citcon_fields_supporter_type'][0]['value'];

  $form = array();
  $form['facebook_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom facebook code'),
    '#description' => t('When you use the facebook token, this code will be embedded in the page. Leave it blank to use the default we have for facebook. Which is a "Like" button.'),
    '#default_value' => isset($defaults['facebook_code']) ? $defaults['facebook_code'] : NULL,
  );

  return $form;
}

/**
 * A kind of hook_token_info
 */
function facebook_simple_sharing_token_info() {
  return array(
    'facebook' => array(
      'name' => t('Facebook'),
      'description' => t("Share on Facebook token."),
    ),
  );
}

/**
 * A kind of hook_token_replace
 */
function facebook_simple_sharing_token_replace($name, $data, $node) {
  // If it's not our token... get outta here, quick.
  if ($name != 'facebook') {
    return FALSE;
  }

  // Look for custom FB code... if not, use our default.
  if (isset($data['facebook_code']) && !empty($data['facebook_code'])) {
    $code = $data['facebook_code'];
  }
  else {
    $code = '<iframe src="//www.facebook.com/plugins/like.php?href&amp;send=false&amp;layout=button_count&amp;width=250&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:21px;" allowTransparency="true"></iframe>';
  }

  return $code;
}
