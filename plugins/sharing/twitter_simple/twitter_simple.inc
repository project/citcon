<?php

/**
 * @file
 * Plugin to provide simple Twitter sharing functionality.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'sharing',
  'title' => t("Twitter"),
  'description' => t('Options to share this on twitter.'),
  'settings form' => 'twitter_simple_settings_form',
  'sharing token_info' => 'twitter_simple_sharing_token_info',
  'sharing token_replace' => 'twitter_simple_sharing_token_replace',
);

/**
 * Settings form, which will attach to "Settings][Sharing" section.
 */
function twitter_simple_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  $campaign_values = citcon_get_campaign_info($node);
  $supporter_type = $campaign_values['citcon_fields_supporter_type'][0]['value'];

  $form = array();
  $form['twitter_msg'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom default message'),
    '#description' => t('When you use the twitter token, this will be the default message. Leave it blank to use the twitter default.'),
    '#default_value' => isset($defaults['twitter_msg']) ? $defaults['twitter_msg'] : NULL,
  );
  $form['campaign_tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('cc-c-' . $supporter_type . '-nohtml'),
    '#global_types' => FALSE,
  );

  return $form;
}

/**
 * A kind of hook_token_info
 */
function twitter_simple_sharing_token_info() {
  return array(
    'twitter' => array(
      'name' => t('Twitter'),
      'description' => t("Share on Twitter token."),
    ),
  );
}

/**
 * A kind of hook_token_replace
 */
function twitter_simple_sharing_token_replace($name, $data, $node) {
  // If it's not our token... get outta here, quick.
  if ($name != 'twitter') {
    return FALSE;
  }

  // Set an empty message, then try to find one.
  $msg = NULL;
  if (isset($data['twitter_msg']) && !empty($data['twitter_msg'])) {
    $msg = token_replace($data['twitter_msg'], array('cc-c' => $node));
  }

  // Get the embed code ready, with the custom message... or no message.
  $code = '<a href="https://twitter.com/share" class="twitter-share-button"' . (!empty($msg) ? ' data-text="' . $msg . '"' : '') . '>Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';

  return $code;
}
