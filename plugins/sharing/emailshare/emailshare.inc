<?php
/**
 * @file
 * Plugin to provide options for sharing the campaign through email.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'sharing',
  'title' => t("Email"),
  'description' => t('Options to share this through email.'),
  'settings form' => 'emailshare_settings_form',
  'sharing token_info' => 'emailshare_sharing_token_info',
  'sharing token_replace' => 'emailshare_sharing_token_replace',
  'ajax callback' => 'emailshare_ajax_callback',
  'ajax validate' => 'emailshare_ajax_validate',
);

/**
 * Settings form, which will attach to "Settings][Sharing" section.
 */
function emailshare_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  $campaign_values = citcon_get_campaign_info($node);
  $supporter_type = $campaign_values['citcon_fields_supporter_type'][0]['value'];

  $form = array();
  $form['email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend "Subject"'),
    '#description' => t('The subject of the Email a friend feature.'),
    '#default_value' => isset($defaults['email_subject']) ? $defaults['email_subject'] : NULL,
  );
  $form['email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Email a friend "Body"'),
    '#description' => t('The body of the Email a friend feature.'),
    '#default_value' => isset($defaults['email_body']) ? $defaults['email_body'] : NULL,
  );
  $form['campaign_tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('cc-c-' . $supporter_type . '-nohtml'),
    '#global_types' => FALSE,
  );
  $form['email_group_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend form title'),
    '#description' => t('What should read at the top of the form?'),
    '#default_value' => isset($defaults['email_group_title']) ? $defaults['email_group_title'] : t('Enter email addresses'),
  );
  $form['email_group_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend form description'),
    '#description' => t('What should read near the top of the form?'),
    '#default_value' => isset($defaults['email_group_description']) ? $defaults['email_group_description'] : t('One per field.'),
  );
  $form['email_add_another'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend "Add Another" button'),
    '#description' => t('What should we put on the "Add Another" button for this? "Add Another"?'),
    '#default_value' => isset($defaults['email_add_another']) ? $defaults['email_add_another'] : t('+ Add Another'),
  );
  $form['email_submit'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend "Send" button'),
    '#description' => t('What should we put on the Submit button for this? "Send"?'),
    '#default_value' => isset($defaults['email_submit']) ? $defaults['email_submit'] : t('Send'),
  );
  $form['email_thanks'] = array(
    '#type' => 'textfield',
    '#title' => t('Email a friend thank you text'),
    '#description' => t('After they click "Send"... what should we say?'),
    '#default_value' => isset($defaults['email_thanks']) ? $defaults['email_thanks'] : t('Thank you so much for supporting this cause.'),
  );

  return $form;
}

/**
 * A kind of hook_token_info
 */
function emailshare_sharing_token_info() {
  return array(
    'email-form' => array(
      'name' => t('Email Form'),
      'description' => t('"Email a friend" form token.'),
    ),
    'email-subject' => array(
      'name' => t('Email Subject'),
      'description' => t('The subject of the "Email a friend" email.'),
    ),
    'email-body' => array(
      'name' => t('Email Body'),
      'description' => t('The body of the "Email a friend" email.'),
    ),
  );
}

/**
 * A kind of hook_token_replace
 */
function emailshare_sharing_token_replace($name, $data, $node) {
  switch ($name) {
    case 'email-subject':
      return token_replace($data['email_subject'], array('cc-c' => $node));
      break;
    case 'email-body':
      return token_replace(check_markup($data['email_body']), array('cc-c' => $node));
      break;
    case 'email-form':
      $form_info = array(
        'type' => 'sharing',
        'id' => 'emailshare',
        'node' => $node,
        'callback' => 'emailshare_share_form',
      );
      $plugin_get_form = drupal_get_form('citcon_plugin_get_form', $form_info);
      return drupal_render($plugin_get_form);
      break;
  }

  // return false if we found nothing
  return FALSE;
}

/**
 * Form function for sharing
 */
function emailshare_share_form(&$form, &$form_state, $node) {
  // Get plugin settings
  $plugin_data = citcon_get_plugin_data('citcon_plugin_sharing', $node, 'emailshare');

  // if we've already sent it... send back the thank you message
  if (isset($form_state['emailshare_sent']) && $form_state['emailshare_sent']) {
    $form['email_a_friend'] = array(
      '#type' => 'fieldset',
      '#title' => isset($plugin_data['email_group_title']) ? $plugin_data['email_group_title'] : t('Enter email addresses'),
      '#description' => isset($plugin_data['email_thanks']) ? $plugin_data['email_thanks'] : t('Thank you so much for supporting this cause.'),
      '#prefix' => '<div id="citcon-emailshare">',
      '#suffix' => '</div>',
    );

    // And that's it!
    return;
  }

  // Store our subject and body
  $form['subject'] = array(
    '#type' => 'value',
    '#value' => $plugin_data['email_subject'],
  );
  $form['body'] = array(
    '#type' => 'value',
    '#value' => $plugin_data['email_body'],
  );

  // Find out how many email fields we need to be rendering here.
  $num_of_supporters = isset($form_state['clicked_button']['#name']) && $form_state['clicked_button']['#name'] == 'add_another' ? $form_state['values']['num_of_supporters'] + 1 : 1;
  // Add our $num_of_supporters data
  $form['num_of_supporters'] = array(
    '#type' => 'value',
    '#value' => $num_of_supporters,
  );

  // Then we add a "find" button.
  $form['email_a_friend'] = array(
    '#type' => 'fieldset',
    '#title' => isset($plugin_data['email_group_title']) ? $plugin_data['email_group_title'] : t('Enter email addresses'),
    '#description' => isset($plugin_data['email_group_description']) ? $plugin_data['email_group_description'] : t('One email per field, please and thank you.'),
    '#prefix' => '<div id="citcon-emailshare">',
    '#suffix' => '</div>',
  );

  // Set the tree for our addys
  $form['email_a_friend']['email_addys']['#tree'] = TRUE;
  for ($i = 0; $i < $num_of_supporters; $i++) {
    $form['email_a_friend']['email_addys'][$i] = array(
      '#type' => 'textfield',
      '#size' => 30,
    );
  }

  // Add our ajax buttons
  $form['email_a_friend']['add_another'] = array(
    '#type' => 'button',
    '#value' => isset($plugin_data['email_add_another']) ? $plugin_data['email_add_another'] : t('+ Add Another'),
    '#name' => 'add_another',
    '#weight' => 10,
    '#executes_submit_callback' => FALSE,
    '#citcon_plugin_load' => TRUE,
    '#citcon_plugin_type' => 'sharing',
    '#citcon_plugin_id' => 'emailshare',
    '#submit' => array('citcon_ajax_submit'),
    '#validate' => array('citcon_ajax_validate'),
    '#ajax' => array(
      'callback' => 'citcon_ajax_callback',
      'wrapper' => 'citcon-emailshare',
      'effect' => 'fade',
      'keypress' => TRUE,
      'progress' => array(
        'message' => NULL,
      ),
    ),
  );
  $form['email_a_friend']['send'] = array(
    '#type' => 'button',
    '#value' => isset($plugin_data['email_submit']) ? $plugin_data['email_submit'] : t('Send'),
    '#name' => 'send',
    '#weight' => 20,
    '#executes_submit_callback' => FALSE,
    '#citcon_plugin_load' => TRUE,
    '#citcon_plugin_type' => 'sharing',
    '#citcon_plugin_id' => 'emailshare',
    '#submit' => array('citcon_ajax_submit'),
    '#validate' => array('citcon_ajax_validate'),
    '#ajax' => array(
      'callback' => 'citcon_ajax_callback',
      'wrapper' => 'citcon-emailshare',
      'effect' => 'fade',
      'keypress' => TRUE,
      'progress' => array(
        'message' => NULL,
      ),
    ),
  );
}

/**
 * Ajax callback for the form
 */
function emailshare_ajax_callback($form, $form_state) {
  return $form['email_a_friend'];
}

/**
 * Ajax validate for the form
 */
function emailshare_ajax_validate($form, &$form_state) {
  // if we're just adding another or not sending... don't validate yet.
  if ($form_state['clicked_button']['#name'] != 'send') {
    return;
  }

  // Otherwise, find invalid addresses.
  $errors = FALSE;
  foreach (array_filter($form_state['values']['email_addys']) as $i => $addy) {
    if (!valid_email_address($addy)) {
      $errors = TRUE;
      form_set_error('email_addys][' . $i, t('Please enter a valid email address'));
    }
  }

  // Then send the message... if we found no errors
  if (empty($errors)) {
    global $language;

    // Get the campaign values
    $node = $form_state['values']['form_info']['node'];
    $campaign_values = citcon_get_campaign_info($node);

    // Get the "Test To" email addresses in case we need them.
    $test_to = FALSE;
    if (isset($campaign_values['citcon_test_emails'])
        && !empty($campaign_values['citcon_test_emails'])) {
      $test_to = citcon_get_test_email_addresses($campaign_values);
    }

    // Set the email params
    $params = array(
      'bcc' => implode(', ', array_filter($form_state['values']['email_addys'])),
      'subject' => token_replace($form_state['values']['subject'], array('cc-c' => $node)),
      'body' => token_replace($form_state['values']['body'], array('cc-c' => $node)),
    );

    // Set the From addy if we have one.
    $from = NULL;
    if ($supporter = citcon_get_supporter_from_session()) {
      if (isset($supporter->field_citcon_email[LANGUAGE_NONE][0]['email'])) {
        $from = $supporter->field_citcon_email[LANGUAGE_NONE][0]['email'];
      }
    }

    // Based on the "Status" of the campaign, do different things.
    $campaign_status = $campaign_values['citcon_campaign_status'][0]['value'];
    switch ($campaign_status) {
      case CITCON_CAMPAIGN_ACTIVE:
        // Set the TO field blank, because we're sending one email
        // to different people, listed in the BCC field.
        $email = drupal_mail('citcon', 'general', '', $language, $params, $from);
        break;
      case CITCON_CAMPAIGN_TEST:
        $test_mode = $campaign_values['citcon_test_opt'][0]['value'];

        // Based on the test mode, let's display info a particular way.
        switch ($test_mode) {
          case CITCON_TEST_DISPLAY:
            $debug_info = "\n\r";
            $debug_info .= '=========================';
            $debug_info .= "\n\r";
            $debug_info .= '"Email a friend" Email Debug Info';
            $debug_info .= "\n\r";
            $debug_info .= '=========================';
            $debug_info .= "\n\r";
            $debug_info .= citcon_get_email_debug_info(NULL, $params, $from);

            drupal_set_message(nl2br($debug_info));
            break;
          case CITCON_TEST_EMAIL_DEBUG:
            $debug_info = "\n\r";
            $debug_info .= '=========================';
            $debug_info .= "\n\r";
            $debug_info .= '"Email a friend" Email Debug Info';
            $debug_info .= "\n\r";
            $debug_info .= '=========================';
            $debug_info .= "\n\r";
            $debug_info .= citcon_get_email_debug_info(NULL, $params, $from);

            $params['body'] .= $debug_info;
          // Don't break here, because we're sending something...
          case CITCON_TEST_EMAIL:
            if ($test_to) {
              unset($thanks_email['params']['cc']);
              unset($thanks_email['params']['bcc']);
              $email = drupal_mail('citcon', 'general', $test_to, $language, $params, $from);
            }
            break;
        }
    }

    $form_state['emailshare_sent'] = TRUE;
  }
}
