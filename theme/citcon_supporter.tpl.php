<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) "display name" of the supporter.
 * - $content: An array of supporter items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $supporter_url: Direct url of the current node.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions.
 *   - citcon-supporter--[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *
 * Other variables:
 * - $supporter: Full node object. Contains data that may not be safe.
 * - $type: Supporter type, i.e. story, page, blog, etc.
 * - $sid: Supporter ID.
 * - $nid: Node ID of the campaign.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 */
?>
<div id="citcon-supporter-<?php print $sid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $supporter_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>

  <div class="submitted">
    <?php print $date; ?>
  </div>

  <?php print render($content['links']); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
    // We hide the links now because we can rendered them before.
    hide($content['links']);
    print render($content);
    ?>
  </div>

</div>
