<?php
/**
 * @file
 * Provides the default theme hooks for the admin overview page.
 */

/**
 * Returns HTML for a supporter type description for the
 * supporter type admin overview page.
 *
 * @param $variables
 *   An associative array containing:
 *   - label: The human-readable name of the supporter type.
 *   - type: An object containing the 'type' (machine name) and 'description' of
 *     the supporter type.
 *
 * @return Returns HTML output.
 */
function theme_citcon_supporter_admin_overview($variables) {
  $label = $variables['label'];
  $type = $variables['type'];

  $output = check_plain($label);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $type->type)) . '</small>';
  return $output;
}
